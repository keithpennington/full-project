<?php

/** Gravity Forms Constants */
define( 'GF_LICENSE_KEY', 'abf9b2327846829533c193f8dc4d2c4d' );
define( 'GF_THEME_IMPORT_FILE', '../parent.theme/inc/gf-forms.json' );

add_action( 'after_setup_theme', function() {

  include_once( get_stylesheet_directory() . "/classes/Loader.php" );
  
}, 1 );

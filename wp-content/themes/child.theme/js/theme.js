
var theme; 

jQuery(document).ready(function($) {

  theme = {

    analytics: {
      type: "gtag"
    },

    styles: {
      remove: (el) => {
        var h = $(el).outerHeight();
        return {
          margin: "-"+(h/2)+"px  auto",
          opacity: "0",
          transform: "rotate3d(1,0,-.2,90deg) scale(.6)"
        };
      }
    },

    lightbox: {
      imageSelector: false,
      gallerySelector: false,
      slideSelector: false
    },

    header: {
      id: "#main-nav",
      height: function() {
        return $(this.id).outerHeight();
      },
      isFixed: function() {
        return $(this.id).hasClass("is-fixed-top") ? true : false;
      },
      position: {
        changeOn: ["mobile", "tablet"],
        toggleClass: "is-fixed-top",
        bodyToggleClass: "has-navbar-fixed-top"
      }
    },

    footer: {
      id: "#site-footer",
      height: function() {
        return $(this.id).outerHeight();
      }
    },

    breakpoints: [
      {
        name:  "mobile",
        limits: [ 0, 767 ]
      },
      {
        name: "tablet",
        limits: [ 768, 1024 ]
      },
      {
        name: "desktop",
        limits: [ 1025, 1215 ]
      },
      {
        name: "widescreen",
        limits: [ 1216, 1407 ]
      },
      {
        name: "fullhd",
        limits: [ 1408, 3000 ]
      }
    ],

    scrolledThreshold: 80,

    devSiteExp: new RegExp(/(localhost)/),

    init: function() {

        console.log("Initializing theme JS...");
        console.log("Is dev: " + this.isDev());
        console.log("Is logged in: " + this.isLoggedIn());

        this.ieFix();

        if( $(".blocks-gallery-item").length > 0 )
          this.artworkFullscreenSlider(".blocks-gallery-item");

    },

    ieFix: () => {

      if( navigator.userAgent.search("MSIE") >= 0 ) {
        // Set dashoffset to 0px since animation will not work
        $("#pen").css("stroke-dashoffset", "0px !important");
      }

    },

    docHeight: () => {
      return $(document).height();
    },

    isHome: () => {
      return $(".front-page").length > 0 ? true : false;
    },

    isDev: function() {
      var matches = location.href.match(this.devSiteExp);
      return matches !== null ? true : false;
    },

    isLoggedIn: () => {
      return $("body.logged-in").length > 0 ? true : false;
    }

  };
  
  try {

    theme.init();

  } catch (ex) {

    console.log("Error: ", ex);
    
  }

});

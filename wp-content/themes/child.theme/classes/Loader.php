<?php

namespace Theme\Child;

use Theme\Parent\{ Loader as Core, Utility };

class Loader extends Core {

  protected $_ThemeSupport = [
    [
      'name' => "align-wide"
    ],
    [
      'name' => "html5"
    ],
    [
      'name' => "menus"
    ],
    [
      'name' => "post-thumbnails"
    ],
    [
      'name' => "widgets"
    ],
    [
      'name' => "editor-color-palette",
      'args' => [
        [
          'name'    => "Primary",
          'slug'    => "primary",
          'color'   => "#F4C206"
        ],
        [
          'name'    => "Secondary",
          'slug'    => "secondary",
          'color'   => "#F6E4A0"
        ],
        [
          'name'    => "Tertiary",
          'slug'    => "tertiary",
          'color'   => "#5B4837"
        ],
        [
          'name'    => "Body",
          'slug'    => "body",
          'color'   => "#707070"
        ],
        [
          'name'    => "Light",
          'slug'    => "light",
          'color'   => "#FFF4E9"
        ],
        [
          'name'    => "Dark",
          'slug'    => "dark",
          'color'   => "#585858"
        ],
        [
          'name'    => "Link",
          'slug'    => "link",
          'color'   => "#8773B3"
        ],
        [
          'name'    => "Info",
          'slug'    => "info",
          'color'   => "#FFB52E"
        ],
        [
          'name'    => "Warning",
          'slug'    => "warning",
          'color'   => "#F47E00"
        ],
        [
          'name'    => "Danger",
          'slug'    => "danger",
          'color'   => "#B10C18"
        ]
      ]
    ],
    [
      'name'    => "woocommerce"
    ],
    [
      'name'    => "wc-product-gallery-zoom"
    ],
    [
      'name'    => "wc-product-gallery-lightbox"
    ],
    [
      'name'    => "wc-product-gallery-slider"
    ],
    [
      'name'    => "custom-spacing"
    ]

  ];
  protected $_Menus = [
    "Main",
    "Footer",
    "Resources",
    // "Social"
  ];
  protected $_PostTypes = [
    "Service",
    "Coupon",
    "Message",
    "Review",
    "Slide",
    "Staff",
    "Job",
    "MegaMenu",
    "Location"
  ];
  protected $_Shortcodes = [
    "Address",
    "Feed",
    "Modal",
    "Sitemap",
    "Slider",
  ];
  protected $_SettingsPages = [
    "Business",
    "Social",
    "ThemeReference"
  ];
  protected $_Widgets = [
    'deactivate' => [
      "WP_Widget_Calendar",
      "WP_Widget_Search",
      "WP_Widget_Categories",
      "WP_Widget_Recent_Posts",
      "WP_Widget_Recent_Comments",
      "WP_Widget_RSS",
      "WP_Widget_Tag_Cloud",
      "WP_Widget_Meta",
      "WP_Widget_Archives",
      "WP_Widget_Pages"
    ],
    'activate' => [

    ]
  ];

  protected $_sidebars = [
    [
      'name'          => "Blog",
      'id'            => "blog",
      'description'   => "Sidebar to use on the blog pages.",
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => "</div>"
    ],
    [
      'name'          => "Footer",
      'id'            => "footer",
      'description'   => "Footer widget area",
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => "</div>"
    ],
    [
      'name'          => "Navbar",
      'id'            => "navbar",
      'description'   => "Displays above the navbar links",
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => "</div>"
    ]
  ];

  protected $_scripts = [
    'front' => [
      [
        'name'          => "jquery",
        'dir'           => "template",
        'file'          => "/vendor/jquery-3.3.1/jquery-3.3.1.min.js",
        'dependencies'  => null,
        'ver'           => false,
        'footer'        => false
      ],
      [
        'name'          => "theme",
        'dir'           => "stylesheet",
        'file'          => "/js/theme.min.js",
        'dependencies'  => [ "jquery" ],
        'ver'           => true,
        'footer'        => true
      ]
    ],
    'admin' => [
      [
        'name'          => "bootstrap",
        'dir'           => "template",
        'file'          => "/vendor/bootstrap-4.2.1/js/bootstrap.bundle.min.js",
        'dependencies'  => [ "jquery" ],
        'ver'           => false,
        'footer'        => true
      ]
    ]
  ];
  protected $_styles = [
    'front' => [
      [
        'name'  => "fontawesome",
        'dir'   => "template",
        'file'  => "/vendor/fontawesome-free-5.7.1/css/all.min.css"
      ],
      [
        'name'  => "fonts",
        'dir'   => "stylesheet",
        'file'  => "/css/fonts.css",
        'ver'   => true
      ],
      [
        'name'  => "theme",
        'dir'   => "stylesheet",
        'file'  => "/css/theme.min.css",
        'ver'   => true
      ]
    ],
    'admin' => [
      [
        'name'  => "bootstrap",
        'dir'   => "template",
        'file'  => "/vendor/bootstrap-4.2.1/css/bootstrap.min.css"
      ],
      [
        'name'  => "fontawesome",
        'dir'   => "template",
        'file'  => "/vendor/fontawesome-free-5.7.1/css/all.min.css"
      ],
      [
        'name'  => "admin-custom",
        'dir'   => "stylesheet",
        'file'  => "/css/admin.min.css",
        'ver'   => true
      ]
    ]
  ];
  protected $_stylesheetFramework = "Bulma";

  protected function _stylesheetSetup() {

    // ACTIONS
    add_action( 'pre_footer', [ $this, "postFooter" ] );
    add_action( 'wp_head', [ $this, 'locAddAjaxurl' ] );

    // FILTERS
    add_filter( 'get_site_icon_url', [ $this, "favicon" ], 10, 3 );
    add_filter( 'body_class', [ $this, 'bodyClasses' ], 10, 2 );
    add_filter( 'footer_copyright', [ $this, "copyright" ] );
    add_filter( 'paginate_links', [ $this, 'removeQueryVars' ] );  
    add_filter( 'query_vars', [ $this, 'addLocVars' ] );

    // CONTENT FRAMEWORK
    if( ! is_admin() ) 
      define( "CONTENTFRAMEWORK", $this->_stylesheetFramework );

  }

  public function postFooter() {

    if( ! is_front_page() ) 
      Utility::getScopedTemplatePart( "template-parts/nav/nav", "footer" );

    Utility::getScopedTemplatePart("template-parts/footer/footer", "widgets");

  }

  public function favicon( $url, $size, $blog_id ) {

    $url = get_stylesheet_directory_uri() . "/assets/favicon.png";
    return $url;

  }

  public function bodyClasses( $classes ) {

    if( is_front_page() )
      $classes[] = "front-page";

    return $classes;

  }

  public function copyright( $copyright ) {

    return sprintf(
      '<p>&copy; %s <a href="%s">%s</a></p>',
      date( "Y" ),
      site_url(),
      get_bloginfo( 'name' )
    );

  }

  public function locAddAjaxurl() {

    echo '<script type="text/javascript">
            var ajaxurl = "' . admin_url('admin-ajax.php') . '";
          </script>';
 }

 public function removeQueryVars( $link ) {
   $link =  filter_input( INPUT_GET, 'action' ) ? remove_query_arg( 'action', $link ) : $link;
 
   return $link;
 }

  public function addLocVars($public_query_vars) {
      $public_query_vars[] = 'type';
      return $public_query_vars;
  }

}

new Loader();

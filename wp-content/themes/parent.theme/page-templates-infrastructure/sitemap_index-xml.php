<?php

header( 'Content-Type: application/xml; charset=utf-8' );

if( ! isset($GLOBALS['wp'] ) )
  die;

use Theme\Parent\Utility;

$contentTypes = [
  [
    'name'   => "post",
    'title'       => "Blog Posts",
    'changefreq'  => "weekly",
    'priority'    => "0.3"
  ],
  [
    'name'   => "page",
    'title'       => "Pages",
    'changefreq'  => "monthly",
    "priority"    => "0.5"
  ]
];
$contentTypes = apply_filters( 'sitemap_content_types', $contentTypes );

$args = apply_filters(
  'default_sitemap_args',
  [
    'post_status' => "publish",
    'orderby'     => "title",
    'order'       => "DESC"
  ],
  $contentTypes
);

ob_start();

echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";

?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <?php

    if( ! empty( $contentTypes ) ) {

      foreach( $contentTypes as $type ) {

        if( taxonomy_exists( $type['name'] ) ) {

          $terms = get_terms( [
            'taxonomy'    => $type['name'],
            'hide_empty'  => "true"
          ] );

          foreach( $terms as $term ) {

            echo sprintf(
              "<url><loc>%s</loc><changefreq>%s</changefreq><priority>%s</priority></url>\n",
              get_term_link( $term, $type['name'] ),
              $type['changefreq'],
              $type['priority']
            );

          }

        } else {

          $args['post_type']  = $type['name'];
          $query              = new WP_Query( $args );

          if( $query->have_posts() ) {

            $walkerArgs = [
              'markup'      => "xml",
              'changefreq'  => $type['changefreq'],
              'priority'    => $type['priority']
            ];

            $sitemapWalker = new Theme\Parent\Sitemap\Walker();
            echo $sitemapWalker->walk( $query->posts, -1, $walkerArgs );

          }

        }

      }

    }

  ?>
</urlset>

<?php echo ob_get_clean();

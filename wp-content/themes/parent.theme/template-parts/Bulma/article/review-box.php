<?php

use Theme\Parent\Utility;

if( ! isset( $args['review_details'] ) || ! isset( $args['source_details'] ) || ! isset( $args['source'] ) )
  return;

global $post;

if( isset( $args['post'] ) )
  $reviewPost = is_object( $args['post'] ) ? $args['post'] : get_post( $args['post'] );
else
  $reviewPost = $post;

$defaults = [
  'review' => [
    'description'   => "",
    'location'      => "",
    'name'          => "",
    'score'         => "",
    'url'           => "",
    'profile_photo' => "/wp-content/themes/parent.theme/assets/default-reviewer-profile.svg"
  ],
  'source' => [
    'icon'        => ""
  ]
];
$reviewDetails = wp_parse_args( $args['review_details'], $defaults['review'] );
$sourceDetails = wp_parse_args( $args['source_details'], $defaults['source'] );

$scoreVal = $reviewDetails['score'] * 20;

if( "review" !== $reviewPost->post_type || ! $args['source'] instanceof WP_Term )
  return;

?>

<article id="review-<?php echo $reviewPost->ID ?>" class="media review box">
  <figure class="media-left review-source">
    <a target="_new" href="<?php echo esc_attr( $reviewDetails['url'] ) ?>">
      <i class="<?php echo esc_attr( $sourceDetails['icon'] ) ?>"></i>
      <span><?php echo $args['source']->name ?></span>
    </a>
  </figure>
  <div class="media-content">
    <?php if( ! empty( $reviewDetails['profile_photo'] ) ) { ?>
    <div class="reviewer-icon">
      <img alt="Reviewer Profile Image" src="<?php echo esc_attr( $reviewDetails['profile_photo'] ) ?>" />
    </div>
    <?php } ?>
    <h6 class="title is-5">
      <a target="_new" href="<?php echo esc_attr( $reviewDetails['url'] ) ?>"><?php echo $reviewPost->post_title ?></a>
    </h6>
    <div class="score">
      <div class="foreground" style="width: <?php echo $scoreVal ?>%">
        <div class="inner-set">
          <?php echo str_repeat('<i class="fas fa-star fa-fw"></i>', 5) ?>
        </div>
      </div>
      <div class="background">
        <?php echo str_repeat('<i class="far fa-star fa-fw"></i>', 5) ?>
      </div>
    </div>
    <i class="reviewer">by <?php echo $reviewDetails['name'] ?> in <?php echo $reviewDetails['location'] ?></i>
    <div class="review-description"><?php echo wpautop( $reviewDetails['description'] ) ?></div>
  </div>
</article>

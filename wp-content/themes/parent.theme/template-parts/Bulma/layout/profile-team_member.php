<?php
/**
 * Card for a blog excerpt and author
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args) )
  exit;

use Theme\Parent\Utility;

$info = Utility::getPostMetaValue( "details" );

ob_start() ?>

<article class="team-profile">
  <a href="<?php echo $info['linkedin'] ?>" target="_new">
    <figure class="profile-photo">
      <?php

        if( has_post_thumbnail() )
          the_post_thumbnail( "large-mobile", [ 'class' => "is-rounded" ] );
        else
          echo sprintf( "<img class='is-rounded' src='%s' alt='%s' />", get_template_directory_uri() . "/assets/default-profile.svg", get_the_title() );
      ?>
    </figure>
    <h3>
      <?php the_title() ?>
    </h3>
    <h4>
      <?php echo $info['title'] ?>
    </h4>
  </a>
</article>

<?php echo ob_get_clean();

<?php
/**
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @param $args['main_classes'] array Additional classes to add to the <section> container.
 * @param $args['section_classes'] bool Whether or not to show the featured image on this post. Default is has_post_thumbnail().
 * @param $args['sidebar'] string The name of the sidebar that is included via the template. Used for generating classes only for intelligent styling.
 *
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

use Theme\Parent\Utility;

$breadCrumbs  = Utility::getBreadCrumbs();

$sidebar        = isset( $args['sidebar'] ) ? $args['sidebar'] : false;
$sectionClasses = isset( $args['section_classes'] ) ? $args['section_classes'] : [];
$mainClasses    = isset( $args['main_classes'] ) ? $args['main_classes'] : [ "no-sidebar", "full-width", "entry-content" ];
$author         = get_the_author();
$categories     = wp_get_post_categories( get_the_ID(), [ 'fields' => "all" ] );
$linkMap        = [
  'text'      => "name",
  'href'      => "slug",
  'title'     => "taxonomy"
];

if( false !== $sidebar )
  $mainClasses = [ "has-sidebar", "has-{$args['sidebar']}-sidebar", "entry-content" ];

$hookArgs = [
  'post_id'   => get_the_ID(),
  'framework' => "bulma",
  'sidebar'   => $sidebar
];

ob_start() ?>

<main id="main-content" role="main" class="<?php echo implode( " ", apply_filters( 'main_classes', $mainClasses, $hookArgs ) ) ?>">
  <section class="section <?php echo implode( " ", apply_filters( 'main_section_classes', $sectionClasses, $hookArgs ) ) ?>">
  <?php echo apply_filters( 'main_breadcrumbs', $breadCrumbs ) ?>
    <?php do_action( 'before_content', $hookArgs ) ?>
    <div class="container content">
      <figure>
        <?php the_post_thumbnail( "desktop" ) ?>
      </figure>
      <h1 class='title is-1'>
        <?php the_title() ?>
      </h1>
      <div class="post-details">
        <?php 

          if( ! empty( $categories ) )
            echo sprintf( "<span class=\"post-categories\">\n%s\n</span>\n", implode(", ", Utility::linkify( $linkMap, $categories ) ) );

        ?>
        /
        <span class="author">
          by <?php echo $author ?>
        </span>
      </div>
      <?php the_content() ?>
    </div>
    <?php do_action( 'after_content', $hookArgs ) ?>
  </section>

  <?php comments_template(); ?>

</main>

<?php echo ob_get_clean();

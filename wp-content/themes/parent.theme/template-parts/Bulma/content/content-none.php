<?php
/**
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

use Theme\Parent\Utility;

$breadCrumbs = Utility::getBreadCrumbs();

$filterArgs = [
  'no_content'  => true,
  'framework'   => "bulma"
];

ob_start() ?>

<main role="main" class="<?php echo implode( " ", apply_filters( 'main_classes', [ "no-content" ], $filterArgs ) ) ?>">
    <section class="section <?php echo implode( " ", apply_filters( 'main_section_classes', [], $filterArgs ) ) ?>">
    <?php echo apply_filters( 'main_breadcrumbs', $breadCrumbs ) ?>
      <div class="container content">
        <h2>Whoops!</h2>
        <p>
          Gnomes stole our content.  Again.
        </p>
        <div class="buttons">
          <a class="button is-primary is-large" href="/">Start Over</a>
        </div>
      </div>
    </section>
</main>

<?php echo ob_get_clean();

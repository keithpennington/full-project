<?php
/**
 * Header to display full-width featured image
 * 
 * @param $args['page_type'] string the type of page on which this feed will render.
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

use Theme\Parent\Utility;

$breadCrumbs  = Utility::getBreadCrumbs();

global $wp_query;

$sidebar      = isset( $args['sidebar'] ) ? $args['sidebar'] : false;
$pageType     = isset( $args['page_type'] ) ? $args['page_type'] : "";
$sectionClasses = isset( $args['section_classes'] ) ? $args['section_classes'] : [];
$mainClasses  = isset( $args['main_classes'] ) ? $args['main_classes'] : [ "no-sidebar", "full-width", $pageType ];

if( false !== $sidebar )
  $mainClasses = [ "has-sidebar", "has-{$args['sidebar']}-sidebar", $pageType ];

$hookArgs = [
  'framework' => "bulma",
  'sidebar'   => $sidebar
];

$args = [
  'atts' => [
    'align'     => "left",
    'format'    => "list",
    'limit'     => 10,
    'orderby'   => "date",
    'order'     => "DESC",
    'paginate'  => true,
    'pgn_style' => "normal",
    'pgn_top'   => true,
    'pgn_bot'   => true,
    'type'      => $pageType
  ],
  'items' => $wp_query
];

ob_start() ?>

<main id="main-content" role="main" class="<?php echo implode( " ", apply_filters( 'main_classes', $mainClasses, $hookArgs ) ) ?>">
    <section class="section <?php echo implode( " ", apply_filters( 'main_section_classes', $sectionClasses, $hookArgs ) ) ?>">
    <?php echo apply_filters( 'main_breadcrumbs', $breadCrumbs ) ?>
    <?php do_action( 'before_content', $hookArgs ) ?>
      <div class="container content">
      <?php Utility::getScopedTemplatePart( "template-parts/Vanilla/shortcode/feed", "container", apply_filters( 'default_archive_args', $args ) ) ?>
      </div>
      <?php do_action( 'after_content', $hookArgs ) ?>
    </section>
</main>

<?php echo ob_get_clean();

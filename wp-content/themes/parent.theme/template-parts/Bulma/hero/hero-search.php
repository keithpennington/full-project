<?php
/**
 * Search results page hero with tabs of content segments based on results.
 * 
 * @param $args['classes'] array Additional classes to add to the <section> container.
 * @param $args['image'] bool Whether or not to show the featured image on this post. Default is has_post_thumbnail().
 * @param $args['title'] string The title that should be printed to the page. Default is get_the_title().
 * @param $args['subtitle'] string The subtitle that should be printed to the page. Default is blank.
 * @param $args['description'] string The description provided to the relative taxonomy.
 * @param $args['tabs'] array Labels that will be used on the tabs based on the search result objects.
 *
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

$requiredClasses      = [
  "entry-header",
  "hero",
  "is-medium",
  "is-bold"
];
$requiredClasses[]    = \Theme\Parent\Framework\Bulma::getDefaultStyle( "hero" );
$args['classes']      = isset( $args['classes'] ) ? $args['classes'] : [];
$args['title']        = isset( $args['title'] ) ? $args['title'] : "Search Results";
$args['post_types']   = isset( $args['post_types'] ) ? $args['post_types'] : Theme\Parent\Defaults::instance()->getPostTypes();
$args['active_tab']   = get_query_var( "type", "post" );
$titles               = [];
$titles[]             = sprintf( '<h1 class="%s">%s</h1>', apply_filters( 'hero_title_class', "entry-title title is-1 mb-3" ),$args['title'] );

if( isset( $args['subtitle'] ) )
  $titles[] = sprintf( '<h2 class="is-4 entry-subtitle subtitle mb-3">%s</h2>', $args['subtitle'] );

if( isset( $args['description'] ) )
  $title[] = sprintf( '<p class="description">' );

$classes  = array_merge( $requiredClasses, $args['classes'] );
$hookArgs = [
  'framework' => "Bulma",
  'post_id'   => get_the_ID()
];


ob_start() ?>

<header id="main-hero" class="hero is-info is-medium">
  <?php do_action( 'html_before_hero_body', $hookArgs ) ?>
  <div class="hero-body">
    <div class="container has-text-centered">
      <?php echo implode( "\n", apply_filters( 'html_hero_archive_title', $titles, $hookArgs ) ) ?>
    </div>
  </div>

  <div class="hero-foot">
    <nav class="tabs is-boxed is-centered">
      <div class="container">
        <ul>
          <?php

          foreach( $args['post_types'] as $type => $name ) {
            $tab = "<li";
            if( $args['active_tab'] === $type )
              $tab .= ' class="is-active"';
            $tab .= ">";
            $tab .= "<a data-tab='{$type}' href='/?s={$_GET['s']}&type={$type}' >{$name}</a>";
            $tab .= "</li>";

            echo $tab;

          }

          ?>
        </ul>
      </div>
    </nav>
  </div>
</header>

<?php echo ob_get_clean();

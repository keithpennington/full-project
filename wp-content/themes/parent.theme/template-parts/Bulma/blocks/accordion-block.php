<?php

if ( ! isset( $args['blocks'] ) )
  exit;

$blocks     = $args['blocks'];
$class      = isset( $args['class'] ) ? $args['class'] : "accordion";

if( 0 < count( $blocks ) ) {

  $html = sprintf("\n<div class='%s'>\n", $class );

  for( $i = 0; $i < count( $blocks ); $i++ ) {

    if( isset( $blocks[$i]['blockName'] ) && "core/heading" === $blocks[$i]['blockName'] ) {

      $html .= "\t<div class='accordion-item'>\n";
      $html .= sprintf( "\t\t<div class=\"accordion-heading p-5\">%s\t\t</div>\n", render_block( $blocks[$i] ) );

    } else {

      $html .= sprintf( "\t\t<div class=\"accordion-content px-5\" hidden>%s\t\t</div>\n", render_block( $blocks[$i] ) );
      $html .= "\t</div>";

    }

  }

  $html .= "</div>\n";
  echo $html;

}

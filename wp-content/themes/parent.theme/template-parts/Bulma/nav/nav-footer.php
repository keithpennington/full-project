<?php
/**
 * Header to display on the standard Blog page
 *
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

 if( ! isset( $args ) )
  exit;

use Theme\Parent\Utility;

$defaultClasses = [
  "hero",
  \Theme\Parent\Framework\Bulma::getDefaultStyle( "nav-footer" ),
  "p-0",
  "has-text-centered"
];

$classes = isset( $args['classes'] ) ? $args['classes'] : $defaultClasses;

ob_start() ?>

<section class="<?php echo implode( " ", apply_filters( 'nav_footer_classes', $classes ) ) ?>">
  <div class="hero-body p-0">
    <div class="container">
      <?php wp_nav_menu( [
        'theme_location'  => "footer",
        'container'       => false,
        'menu_class'      => "nav-footer-items",
        'menu_id'         => "nav-footer",
        'echo'            => true,
        'fallback_cb'     => "wp_page_menu",
        'items_wrap'      => '<nav id="%1$s" class="%2$s">%3$s</nav>',
        'depth'           => 1,
        'walker'          => new \Theme\Parent\Menu\Footer\Walker()
      ] ) ?>
    </div>
  </div>
</section>

<?php echo ob_get_clean();

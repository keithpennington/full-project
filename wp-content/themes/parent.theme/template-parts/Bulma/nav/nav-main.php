<?php
/**
 * Main site header and navigation.
 *
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

 if( ! isset( $args ) )
  exit;

use Theme\Parent\Utility;

$navbarClasses = [ \Theme\Parent\Framework\Bulma::getDefaultStyle( "nav" ) ];
$topBarClasses  = [ 'is-white' ];

$hookArgs = [
  'framework' => "bulma"
];

$menuID = \Theme\Parent\Menu\Main::$menuID;

ob_start();

?>
<?php do_action( 'html_before_nav', $hookArgs ) ?>
<div id='top-bar' class="<?php echo implode( " ", apply_filters( 'topbar_classes', $topBarClasses ) ) ?>">
  <a id='top-btn-left' class='button is-primary' href='<?php echo Utility::getPhone( true ) ?>'><i class='fas fa-phone'></i>&nbsp;Call Us</a>
  <a id='top-btn-right' class='button is-primary' href='/contact-us/'><i class='fa fa-fw fa-envelope'></i> &nbsp;Contact Us</a>
</div>
<header id="site-header">
  <nav id="main-nav" class="navbar <?php echo implode( " ", apply_filters( 'navbar_classes', $navbarClasses ) ) ?>" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
      <?php if( ! has_action( 'nav_logo' ) ) { ?>

      <a class="navbar-item" href="<?php echo esc_url( site_url() ) ?>">
        <span class="screen-reader-text"><?php echo __("Navigate Home", "templatetheme") ?></span>
        <?php if ( ! empty( Utility::getOption( 'business_info', 'compact_logo_url' ) ) ) { ?>

          <img class="navbar-logo is-hidden-mobile" alt="<?php echo htmlentities( get_option( 'blogname' ) . " Logo" ) ?>" src="<?php echo esc_url( Utility::getOption( 'business_info', 'logo_url' ) ) ?>" />
          <img class="navbar-logo is-hidden-tablet" alt="<?php echo htmlentities( get_option( 'blogname' ) . " Logo" ) ?>" src="<?php echo esc_url( Utility::getOption( 'business_info', 'compact_logo_url' ) ) ?>" />

        <?php } else { ?>

          <img class="navbar-logo" alt="<?php echo htmlentities( get_option( 'blogname' ) . " Logo" ) ?>" src="<?php echo esc_url( Utility::getOption( 'business_info', 'logo_url' ) ) ?>" />

        <?php } ?>
      </a>
      <?php } else {

        do_action( 'nav_logo' );

      }
      
      if( ! empty( wp_get_nav_menu_items( $menuID ) ) ) { ?>
        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
          <i aria-hidden="true" class="fa fa-lg fa-fw fa-bars"></i>
          <i aria-hidden="true" class="fa fa-lg fa-fw fa-times"></i>
          <span class="screen-reader-text"><?php echo __("Open Main Menu", "templatetheme") ?></span>
        </a>
        <?php }?>
      
    </div>
    <div class="navbar-menu">
      <?php

      wp_nav_menu( [
        'theme_location'  => "main-nav",
        'container'       => false,
        'menu_class'      => "navbar-start",
        'menu_id'         => "navbar-nav-items",
        'echo'            => true,
        'fallback_cb'     => "wp_page_menu",
        'items_wrap'      => '<div id="%1$s" class="%2$s">%3$s</div>',
        'depth'           => 3,
        'walker'          => new Theme\Parent\Framework\Bulma\Walker()
      ] );
      if( class_exists("Theme\Parent\Menu\Social\Walker" ) ) {
          
        wp_nav_menu( [
          'theme_location'  => "social",
          'container'       => false,
          'menu_class'      => "navbar-end social-items",
          'menu_id'         => "navbar-social-items",
          'echo'            => true,
          'fallback_cb'     => "wp_page_menu",
          'items_wrap'      => '<div id="%1$s" class="%2$s">%3$s</div>',
          'depth'           => 1,
          'walker'          => new Theme\Parent\Menu\Social\Walker()
        ] );

      }

      ?>
    </div>
  </nav>
  <?php do_action( 'html_after_nav', $hookArgs ) ?>
</header>

<?php echo ob_get_clean();

<?php
/**
 * Mega menu template file.
 *
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

 if( ! isset( $args ) )
  exit;

use Theme\Parent\Utility;

$blocks = apply_filters( 'the_content', $args['mega_menu']->post_content );


  ?>

  <div id="mega-menu-<?php echo $args['item']->ID; ?>" class="<?php echo implode(" ", apply_filters('mega_menu_container_classes', [ 'mega-menu-container', 'is-light' ], $args['item'], $args['menu_args'])); ?>">
    <?php echo $blocks; ?>
  </div>

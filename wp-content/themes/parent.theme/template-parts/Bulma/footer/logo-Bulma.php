<?php
/**
 * Display the "Made with Bulma" logo
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset($args) )
  exit;

$style = isset( $args['style'] ) ? $args['style'] : "light";

ob_start();

?>

<div class="level">
  <div class="level-item">
    <p class="image is-128x128" style="height:auto;">
      <a target="_new" href="https://bulma.io/" >
        <img alt="Made with Bulma" src="<?php echo get_template_directory_uri() . "/assets/bulma-badges/made-with-bulma-{$style}.png" ?>" />
      </a>
    </p>
  </div>
</div>

<?php echo ob_get_clean();

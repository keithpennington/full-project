<?php
/*
 * @param string $args[id]
 * @param string $args[value]
 * @param  array $args[options]
 * $options = [
 *   'append',
 *   'class',
 *   'desc',
 *   'label',
 *   'prepend',
 *   'size',
 * ];
 *
 */

if( ! isset( $args['id'] ) )
  exit;

use Theme\Parent\Utility;

$htmlAtts = [
  "class",
  "placeholder",
  "pattern",
  "title"
];
$options          = isset( $args['options'] ) ? $args['options'] : [];
$options['size']  = isset( $options['size'] ) ? $options['size'] : "regular";
$classes          = isset( $options['class'] ) ? explode( " ", $options['class'] ) : [];
$classes          = array_merge( $classes, [ "form-group", "custom-field-wrapper" ] );
$options['class'] = implode( " ", [ "form-control", $options['size'] . "-text" ] );
$describedby      = isset( $options['desc'] )  ? 'aria-describedby="' . $args['id'] . '-description"' : '';

$inputField = sprintf(
  $options['render'],
  $args['id'],
  $args['id'],
  $args['value'],
  $describedby
);

$ouput = [];

// Container/label start
if( isset( $options['label'] ) ) {

  // Use a container if there is a label
  $output[] = sprintf( '<div id="field-%s-wrapper" class="%s">', $args['id'], implode( " ", $classes ) );
  $output[] = sprintf( '<label for="%s">%s</label>', $args['id'], $options['label'] );

}

// Input group start
$output[] = sprintf( '<div class="input-group %s-text">', $options['size'] );

// Prepend
if( isset( $options['prepend'] ) )
  $output[] = sprintf( '<div class="input-group-prepend"><span class="input-group-text">%s</span></div>', $options['prepend'] );

//Input
$output[] = $inputField;

// Append
if( isset( $options['append'] ) )
  $output[] = sprintf( '<div class="input-group-append"><span class="input-group-text">%s</span></div>', $options['append'] );

// Input group end
$output[] = "</div>";

// Description
if( isset( $options['desc'] ) )
  $output[] = sprintf( '<p class="description">%s</p>', $options['desc'] );

// Container end
if( isset( $options['label'] ) )
  $output[] = "</div>";

// Line-break the output
echo implode( "\n", $output );

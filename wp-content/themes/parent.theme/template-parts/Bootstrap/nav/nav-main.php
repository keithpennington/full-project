<?php
/**
 * Header to display on the standard Blog page
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

 if( ! isset( $args ) )
  exit;

ob_start() ?>

<header id="header">
  <?php do_action( 'html_pre_navbar' ) ?>
  <nav id="main-nav" class="navbar is-fixed-top" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
      <a class="navbar-item" href="<?php echo esc_html( get_option( 'site_url' ) ) ?>">
        <img class="navbar-logo" alt="<?php echo esc_html( get_option( 'blogname' ) . " Logo" ) ?>" src="<?php echo esc_html( Utility::getOption( 'business_info', 'logo_url' ) ) ?>" />
      </a>
      <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
        <i aria-hidden="true" class="fa fa-lg fa-fw fa-bars"></i>
        <i aria-hidden="true" class="fa fa-lg fa-fw fa-times"></i>
      </a>
    </div>
    <?php wp_nav_menu( [
      'theme_location'  => 'main-nav',
      'container'       => 'div',
      'container_class' => 'navbar-menu',
      'container_id'    => '',
      'menu_class'      => 'menu ',
      'menu_id'         => '',
      'echo'            => true,
      'fallback_cb'     => 'wp_page_menu',
      'items_wrap'      => '<ul id="" class="navbar-start %2$s">%3$s</div>',
      'depth'           => 3
    ] ); ?>
    </div>
    <div class="pull-right">
      <div class="quick-contact">
        <?php echo do_shortcode( "[address display=inline icon='fa-fw fa-map-pin' parts='city,state']" ) ?>
        <?php echo sprintf( "<a class='%s' href='%s'><i class='%s'></i> <span>%s</span></a>", "phone", Utility::getPhone( true ), "fa fa-fw fa-phone", Utility::getPhone() ) ?>
      </div>
    </div>
  </nav>
</header>

<?php echo ob_get_clean();

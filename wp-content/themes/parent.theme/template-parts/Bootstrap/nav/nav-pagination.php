<?php
/**
 * Pagination
 * 
 * @param $args['classes'] string Additional classes for this pagination nav.
 * @param $args['items'] WP_items result of items matching the 'type' passed to the shortcode.
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

$classes = isset( $args['classes'] ) ? $args['classes'] : [];

global $wp_rewrite;

$pagenum_link = html_entity_decode( get_pagenum_link() );
$url_parts    = explode( '?', $pagenum_link );

$total   = isset( $items->max_num_pages ) ? $items->max_num_pages : 1;
$current = 0 !== get_query_var( 'paged' ) ? intval( get_query_var('paged') ) : 1;

$pagenum_link = trailingslashit( $url_parts[0] ) . '%_%';

$format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
$format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';
$base = $pagenum_link;

$navItems = [];

// Previous Button
$prevClasses  = ["page-item"];
$disabled     = "";

if( $current && 1 < $current ) {

  $link     = str_replace( '%_%', 2 == $current ? '' : $format, $base );
  $link     = str_replace( '%#%', $current - 1, $link );

} else {

  $prevClasses[]  = "disabled";
  $link       = "#";
  $disabled     = 'tabindex="-1" aria-disable="true"';

}

$navItems[] = sprintf(
  '<li class="%s"><a class="page-link" href="%s" %s>Prev</a></li>',
  implode( " ", $prevClasses ),
  $link,
  $disabled
);

// Index buttons
for( $n = 1; $n <= $total; $n++ ) {

  $liClasses  = ["page-item", "index-item"];
  $li         = '<li class="%s" %s>%s</li>';
  $aria       = "";
  $a          = '<a class="page-link" href="%s">%s</a>';
  $link       = str_replace( '%_%', 1 == $n ? '' : $format, $base );
  $link       = str_replace( '%#%', $n, $link );
  $label      = number_format_i18n( $n );

  if( $n == $current ) {

    $liClasses[]  = "active";
    $aria         = ' aria-current="page"';
    $label        .= ' <span class="sr-only">(current)</span>';
  
  }

  $navItems[] = sprintf(
    $li,
    implode( " ", $liClasses ),
    $aria,
    sprintf(
      $a,
      $link,
      $label
    )
  );

}

$nextClasses  = ["page-item"];
$disabled     = "";

if( $current && $current < $total ) {

  $link   = str_replace( '%_%', $format, $base );
  $link   = str_replace( '%#%', $current + 1, $link );

} else {

  $nextClasses[]  = "disabled";
  $link       = "#";
  $disabled     = 'tabindex="-1" aria-disable="true"';

}

$navItems[] = sprintf(
  '<li class="%s"><a class="page-link" href="%s" %s>Next</a></li>',
  implode( " ", $nextClasses ),
  $link,
  $disabled
);

$output = sprintf(
  '<nav class="pagination-container" aria-label="..."><ul class="pagination %s">%s</ul></nav>',
  $classes,
  implode( "\n", $navItems )
);

echo apply_filters( "html_pagination", $output, $navItems );

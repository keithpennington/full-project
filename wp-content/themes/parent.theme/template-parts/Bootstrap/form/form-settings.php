<?php

if( ! isset( $args['id'] ) )
  exit;

$page_title = isset( $args['page_title'] ) && ! empty( $args['page_title'] ) ? $args['page_title'] : "";

?>

<div class="wrap custom-settings" id="section-<?php echo $args['id'] ?>">

  <form action='options.php' method='post'>
  
    <h1><?php echo $page_title ?></h1>

    <?php

      settings_fields( $args['id'] );
      do_settings_sections( $args['id'] );
      submit_button();
      
    ?>
  </form>
</div>

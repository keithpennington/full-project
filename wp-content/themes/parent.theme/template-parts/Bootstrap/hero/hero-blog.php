<?php
/**
 * Header to display on the standard Blog page
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

$id = isset( $args['id'] ) ? $args['id'] : get_option( 'page_for_posts' );

?>

<header class="entry-header gradient">
  <?php echo get_the_post_thumbnail( $id, 'full', ['height' => '', 'width' => ''] ); ?>
  <h1 class="entry-title">
    <?php echo get_the_title( $id ) ?>
  </h1>
</header>

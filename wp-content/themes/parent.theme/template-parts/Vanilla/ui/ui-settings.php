<?php
/**
 * The ui component for theme selector
 *
 * Template: UI Theme
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */
?>

<nav id="settings" data-tooltip="Change between light and dark mode here..." data-trigger="load">
  <button aria-label="Settings Panel">
    <i class="fas fa-fw fa-cog fa-lg"></i>
  </button>
  <ul>
    <li id="user-theme">
      <a aria-label="Change to dark theme" class="dark" title="Change site theme to dark theme."><i class="fas fa-fw fa-lg fa-moon"></i></a>
      <a aria-label="Change to light theme" class="light" title="Change site theme to light theme."><i class="fas fa-fw fa-lg fa-sun"></i></a>
    </li>
  </ul>
</nav>
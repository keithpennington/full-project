<?php
/**
 * Header to display full-width featured image
 * 
 * @param $args['atts'] array The shortcode attributes parsed in the blog.
 * @param $args['items'] WP_Query result of items matching the 'type' passed to the shortcode.
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

use Theme\Parent\Utility;

$atts     = isset( $args['atts'] ) ? $args['atts'] : false;
$items    = isset( $args['items'] ) ? $args['items'] : false;

if( ! $atts || ! $items )
  error_log("Missing required arguments in " . __FILE__ );

$paginate = true === $atts['paginate'] && 1 < $items->max_num_pages;
$containerClasses = [
  "feed-container",
  "{$atts['type']}-feed-container",
  "{$atts['format']}-layout"
];
$alignments = [
  'center'  => "is-centered",
  'left'    => "is-left",
  'right'   => "is-right"
];

$contents = "";

if( $paginate && true === $atts['pgn_top'] ) {

  ob_start();

  Utility::getScopedTemplatePart(
    "template-parts/nav/nav",
    "pagination",
    [
      'items'   => $items,
      'classes' => [
        $alignments[$atts['align']],
        $atts['pgn_style'] === "rounded" ? "is-rounded" : "",
        "top"
      ]
    ]
  );

  $contents .= ob_get_clean();

}

ob_start();

echo '<section class="section px-0">';

while( $items->have_posts() ) {

  $items->the_post();

  $postType = get_post_type();
  $path     = "template-parts/" . CONTENTFRAMEWORK . "/layout/{$atts['format']}";
  $file     = $postType . ".php";
  $template = get_theme_file_path( $path . "-" . $file );

  if( ! file_exists( $template ) )
    $file = "post";

  Utility::getScopedTemplatePart(
    $path,
    str_replace(".php", "", $file),
    [
      'id' => get_the_ID()
    ]
  );

}

echo "</section>";

$contents .= ob_get_clean();

if( $paginate && true === $atts['pgn_bot'] ) {

  ob_start();

  Utility::getScopedTemplatePart(
    "template-parts/nav/nav",
    "pagination",
    [
      'items'   => $items,
      'classes' => [
        $alignments[$atts['align']],
        $atts['pgn_style'] === "rounded" ? "is-rounded" : "",
        "bottom"
      ]
    ]
  );

  $contents .= ob_get_clean();

}

echo sprintf( "<div class=\"%s\">%s</div>", implode( " ", apply_filters( 'feed_container_classes', $containerClasses, $atts['type'] ) ), $contents );

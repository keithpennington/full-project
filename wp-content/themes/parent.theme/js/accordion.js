var accordion;

jQuery(document).ready(function($) {

  accordion = {

    containerSelector: ".accordion",
    headingSelector: ".accordion-heading",
    contentSelector: ".accordion-content",

    item: {
      selector: ".accordion-item",
      activeClass: "is-expanded"
    },

    icon: {

      selector: () => {
        var selector = "i" ;

        if( "undefined" != typeof theme.accordion )
          selector = theme.accordion.icon.iconSelector;

        return selector;
      },

      classes: {
        collapsed: () => {
          var classes = "fas fa-plus-circle";

          if( "undefined" != typeof theme.accordion )
            classes = theme.accordion.icon.classes.collapsed;

          return classes;
        },

        expanded: () => {
          var classes = "fas fa-minus-circle";

          if( "undefined" != typeof theme.accordion )
            classes = theme.accordion.icon.classes.expanded;
          
          return classes;
        }
      },
    },

    transition: {
      down: {
        options: () => {
          // Supports string, integer in milliseconds, or array of options for the jquery slideDown function
          var options = "fast";

          if ( "undefined" != typeof theme.accordion )
            options = theme.accordion.transition.down.options;
          
          return options;
        }
      },

      up: {
        // Supports string, integer in milliseconds, or array of options for the jquery slideDown function
        options: () => {
          var options = "fast";

          if ( "undefined" != typeof theme.accordion )
            options = theme.accordion.transition.up.options;
          
          return options;
        }
      }
    },

    current: {
      cell: null,
      container: null,
      event: null
    },

    init: function() {
      
      $("body").on( "click", this.headingSelector, this.toggleAccordion );

    },

    toggleAccordion: function(e) {

      var self        = accordion;
      var heading     = $(e.currentTarget);

      // Store current items for event dispatches
      self.current.cell       = $(heading).parents(self.item.selector);
      self.current.container  = $(heading).parents(self.containerSelector);
      self.current.event      = e;

      var content     = $(self.current.container).find(self.contentSelector);
      var allCells    = $(self.current.container).find(self.item.selector);
      var icon        = $(self.current.container).find(self.icon.selector());
      var timingUp    = self.transition.up.options();
      var timingDown  = self.transition.down.options();
      var collapsed   = self.icon.classes.collapsed();
      var expanded    = self.icon.classes.expanded();

      if( $(self.current.cell).hasClass(self.item.activeClass) ) {

        self.dispatchEvent("accordion.collapse");

        $(self.current.cell)
          .removeClass(self.item.activeClass)
          .find(content)
          .slideUp(timingUp);

        $(self.current.cell)
          .find(icon)
          .attr("class", collapsed );

        self.dispatchEvent("accordion.collapsed");

      } else {

        self.dispatchEvent("accordion.expand");

        $(self.current.cell)
          .addClass(self.item.activeClass)
          .find( content )
          .slideDown(timingDown);

        $(self.current.cell)
          .find(icon)
          .attr("class", expanded );

        // Remove active class from other items and slide up
        allCells
          .not(self.current.cell)
          .removeClass( self.item.activeClass )
          .find(content)
          .slideUp(timingUp);

        allCells
          .not(self.current.cell)
          .find(icon)
          .attr("class", collapsed );

        self.dispatchEvent("accordion.expanded");

      }

    },

    dispatchEvent: function(name) {

      var data = {
        clickEvent: accordion.current.event,
        accordion: accordion.current.container,
        cell: accordion.current.cell
      };

      general.dispatchEvent( name, data );

    },

  };

  try {
    accordion.init();
  } catch (ex) {
    console.log("Error caught: ", ex);
  }

});



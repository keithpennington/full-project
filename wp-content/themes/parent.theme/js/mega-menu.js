var megaMenu;

jQuery(document).ready(function($) {

  megaMenu = {

    links: {
      selector: $('.mega-menu-link')
    },

    init: function() {

      this.setupTriggers();

    },

    setupTriggers: function() {

      $(this.links.selector).hover(this.mouseIn, this.mouseOut);
    },

    mouseIn: function(e) {
      target = $(e.currentTarget).data('id');
      target = "#" + target;

      $(target).addClass('is-active');
    },

    mouseOut: function(e) {
      target = $(e.currentTarget).data('id');
      target = "#" + target;

      $(target).removeClass('is-active');
    }
  };

  try {
    megaMenu.init();
  } catch (ex) {
    console.log("Error caught: ", ex);
  }

});

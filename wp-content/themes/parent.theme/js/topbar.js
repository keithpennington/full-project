var topBar;

jQuery(document).ready(function($) {

  topBar = {
    
    bar = $("#top-bar"),
    cart: $("#top-btn-left"),
    contact: $("#top-btn-right"),
    navItems: $("#navbar-nav-items"),
    navbarMenu: $('.navbar-menu'),
    offset = $(bar).outerHeight(),
    inNav = false,
    siteHeader = $('#site-header'),
    headerHeight = offset + siteHeader.outerHeight(),

    init: function() {

      if( topBar.isDeskTop() ) {
        $(document).on( 'scroll', topBar.moveBar);

      }
      
      $(window).on('resize', function() {
        if ( topBar.isDeskTop() ) {
          $(document).on( 'scroll', topBar.moveBar );
          }
      });
    },

    isDeskTop: function() {
      result = false;

      if( $(window).width() > 1216 ) {
        result = true;
      }

      return result;
    },

    moveBar: function() {
      if ( $(document).scrollTop() > topBar.headerHeight ) {
        $(topBar.cart).addClass('navbar-item').removeClass('button');
        $(topBar.contact).addClass('navbar-item').removeClass('button').css({'align-self': 'auto'});

        if ( !inNav ) {
          $(topBar.bar).hide();
          $(topBar.siteHeader).hide();
          $(topBar.navItems).append(topBar.cart).append(topBar.contact);
          $(topBar.siteHeader).css('position', 'fixed');
          $(topBar.siteHeader).delay(400).fadeIn(300);
          $('body').css('padding-top', topBar.headerHeight + 'px');
          inNav = true;
        }
      } else {
        $(topBar.cart).removeClass('navbar-item').addClass('button');
        $(topBar.contact).removeClass('navbar-item').addClass('button');
        
        if ( inNav ) {
          $(topBar.bar).show();
          $(topBar.bar).append(topBar.cart).append(topBar.contact);
          $(topBar.siteHeader).css('position', 'relative');
          $('body').css('padding-top', 0 + 'px');
          inNav = false;
        }
      }
    },

    dispatchEvent: function(name) {

      var data = {
        clickEvent: topBar.current.event,
        topBar: topBar.current.container,
        cell: topBar.current.cell
      };

      general.dispatchEvent( name, data );

    },

  };

  try {
    topBar.init();
  } catch (ex) {
    console.log("Error caught: ", ex);
  }

});

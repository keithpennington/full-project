var mobileNav;

jQuery(document).ready(function($) {

  mobileNav = {

    links = $('#navbar-nav-items .navbar-item').not('.has-dropdown'),
    init: function() {

      this.setupTriggers();

    },

    setupTriggers: function() {

      var self = this;
      
      if( $(window).width() < 1216 ) {
        self.links.each(function(i) {
          $(this).on('click', function(e) {
            if (! $(this).hasClass('selected-link')) {
              $(this).addClass('selected-link');
            }

            if ($(this.nextElementSibling).is('.navbar-dropdown')) {
              if ($(this.nextElementSibling).is('.is-expanded')) {
                $(this.nextElementSibling).removeClass('is-expanded').slideUp('fast', 'linear');
              } else {
                e.preventDefault();
                $(this.nextElementSibling).addClass('is-expanded').slideDown('fast', 'linear');
              }
            } 

            var currentItem = self.links[i];

            self.links.each(function(j) {
              if (currentItem != self.links[j]) {
                $(self.links[j]).removeClass('selected-link');

                if ($(self.links[j]).hasClass('navbar-item-has-children')) {
                  $(self.links[j].nextElementSibling).not($(currentItem).parent()).removeClass('is-expanded').slideUp('fast', 'linear');
                }
              }
            });
          });
        });
      }
    }
  };

  try {
    mobileNav.init();
  } catch (ex) {
    console.log("Error caught: ", ex);
  }

});

var lightbox;

jQuery(document).ready(function($) {

  lightbox = {

    gallery: {
      selector: () => {

        var selector = ".blocks-gallery-grid";

        if( "undefined" != typeof theme.lightbox )
          selector = theme.lightbox.gallerySelector;

        return selector;

      },
      slider: null,
      slides: {
        selector: () => {

          var selector = ".blocks-gallery-item";

          if( "undefined" != typeof theme.lightbox )
            selector = theme.lightbox.slideSelector;

          return selector;

        },
        collection: null,
        initial: 0
      },
      options: {
        'autoAdjust': true,
        'cellSelector': ".slide"
      }
    },
    image: {
      selector: () => {

        var selector = ".wp-block-image";

        if( "undefined" != typeof theme.lightbox )
          selector = theme.lightbox.imageSelector;

        return selector;

      }
    },

    init: function(params) {

      if( Object.keys(params).length > 0 ) {

      } else {

        this.setupTriggers();

      }

    },

    setupTriggers: function() {

      this.sliderTriggers();
      this.imageTriggers();

    },

    sliderTriggers: function() {
      
      if( false !== this.gallery.selector() && "" !== this.gallery.selector() )
        $(document).on('click', this.gallery.slides.selector(), this.sliderModal);

      general.dispatchEvent("lightbox.gallery.setup", {
        options: this.gallery
      });

    },

    imageTriggers: function() {

      if( false !== this.image.selector() && "" !== this.image.selector() )
        $(document).on('click', this.image.selector(), this.imageModal);
      
      general.dispatchEvent("lightbox.image.setup", {
        options: this.image
      });

    },

    sliderModal: function(e) {

      if( $(e.currentTarget).parents(".modal").length > 0 )
        return;

      e.preventDefault();

      var self = lightbox;
      var parent = $(e.currentTarget).parents(self.gallery.selector());
      var slideNodes = $(parent).find(self.gallery.slides.selector());
      var slider = self.create.container();
      var slides = self.create.slides(slideNodes);
      self.gallery.slides.initial = $(slideNodes).index($(e.currentTarget));

      $(slider).append(slides);
      $(window).on('bulma.modal.shown', self.constructSlider);

      modal.init({
        type: "image",
        content: slider
      });

      general.dispatchEvent("lightbox.gallery.shown", {
        options: self.gallery
      });

    },

    constructSlider: (e) => {

      $(e.modal.content.target)
        .flickity({
          adaptiveHeight: true,
          cellSelector: ".slide",
          imagesLoaded: true,
          lazyLoad: true,
          pageDots: false
        })
        .flickity("select", lightbox.gallery.slides.initial)
        .flickity("resize")
        .addClass("is-visible is-lazyloaded")
        .focus();

      $(window).off('bulma.modal.shown', lightbox.constructSlider);

    },

    create: {

      container: (contents) => {

        return $("<div></div>")
          .attr({
            class: "slider transition when-visible fx-fade-in fx-slide-down",
            id: "gallery-slider"
          });

      },

      slides: (nodes) => {

        var slides = [];
        $(nodes).each(function(i, obj) {

          var contents = $(obj).clone();
          $(contents)
            .find("img")
            .each(function(n, img) {
              $(img)
                .removeAttr("loading")
                .removeAttr("src")
                .removeAttr("data-link")
                .removeAttr("srcset")
                .removeAttr("sizes")
                .attr({
                  "data-flickity-lazyload": $(this).data("full-url")
                });
            });

          slides[i] = $("<div></div>")
            .addClass("slide")
            .html($(contents).html());

        });

        return slides;

      }

    },

    imageModal: function(e) {

      if( $(e.currentTarget).parents(".modal").length > 0 )
        return;

      e.preventDefault();

      var img = $("<div></div>")
        .css({
          "max-width": "94vw",
          "margin": "auto"
        })
        .html($(e.currentTarget).html());

      modal.init({
        type: "image",
        content: img
      });

      general.dispatchEvent("lightbox.image.shown", {
        options: self.image
      });

    },

  };

  try {
    lightbox.init({});
  } catch (ex) {
    console.log("Ex caught: ", ex);
  }
  
});

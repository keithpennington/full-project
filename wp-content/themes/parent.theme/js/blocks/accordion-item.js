( function ( blocks, blockEditor, element, i18n ) {
  var el = element.createElement;
  var AlignmentToolbar = blockEditor.AlignmentToolbar;
  var BlockControls = blockEditor.BlockControls;
  var useBlockProps = blockEditor.useBlockProps;
  var InnerBlocks = blockEditor.InnerBlocks;

  blocks.registerBlockType( 'custom/accordion-item', {
      title: 'Accordion Item',
      icon: 'block-default',
      category: 'design',
      parent: [ 'custom/accordion' ],

      attributes: {
          alignment: {
              type: 'string',
              default: 'none',
          }
      },

      supports: {
        align: true,
        spacing: {
          padding: true
        },
        color: {
          gradients: true
        },
        fontSize: true
      },

      edit: function ( props ) {
          var alignment = props.attributes.alignment;
          const BLOCKS_TEMPLATE = [ 
            [ 'core/heading', { placeholder: 'Accordion Item Heading', className: 'accordion-heading' } ],
            [ 'core/group', { className: 'accordion-content', templateLock: false } ]
           ];

          function onChangeAlignment( newAlignment ) {
              props.setAttributes( {
                  alignment:
                      newAlignment === undefined ? 'none' : newAlignment,
              } );
          }


          return el(
              'div',
              useBlockProps(),
              el(
                  BlockControls,
                  { key: 'controls' },
                  el( AlignmentToolbar, {
                      value: alignment,
                      onChange: onChangeAlignment,
                  } )
              ),

              el(
                InnerBlocks,
                {
                  template: BLOCKS_TEMPLATE,
                  templateLock: true
                }
              )
              
            ); 
      },

      save: function ( props ) {
          var blockProps = useBlockProps.save( { className: 'accordion-item'} );

          return el(
              'div',
              blockProps,

              el(
                InnerBlocks.Content
              )
          );
      },
  });

} )( window.wp.blocks, window.wp.blockEditor, window.wp.element, window.wp.i18n );
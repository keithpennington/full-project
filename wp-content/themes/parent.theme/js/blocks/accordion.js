( function ( blocks, blockEditor, element ) {
  var el = element.createElement;
  var AlignmentToolbar = blockEditor.AlignmentToolbar;
  var BlockControls = blockEditor.BlockControls;
  var useBlockProps = blockEditor.useBlockProps;
  var InnerBlocks = blockEditor.InnerBlocks;

  blocks.registerBlockType( 'custom/accordion', {
      title: 'Accordion',
      icon: 'menu-alt3',
      category: 'design',
      className: 'accordion',

      attributes: {
          alignment: {
              type: 'string',
              default: 'none',
          }
      },

      supports: {
        align: true,
        spacing: {
          padding: true
        },
        color: {
          gradients: true,
          text: false,
          background: true
        }
      },
      example: {
        innerBlocks: [
          {
            name: 'core/heading',
            attributes: {
              content: 'Accordion Item 1'
            }
          },
          {
            name: 'core/heading',
            attributes: {
              content: 'Accordion Item 2'
            }
          },
          {
            name: 'core/heading',
            attributes: {
              content: 'Accordion Item 3'
            }
          }
        ]
      },

      edit: function ( props ) {
          var alignment = props.attributes.alignment;

          function onChangeAlignment( newAlignment ) {
              props.setAttributes( {
                  alignment:
                      newAlignment === undefined ? 'none' : newAlignment,
              } );
          }

          return el(
              'div',
              useBlockProps(),
              el(
                  BlockControls,
                  { key: 'controls' },
                  el( AlignmentToolbar, {
                      value: alignment,
                      onChange: onChangeAlignment,
                  } )
              ),
                el(
                  InnerBlocks,
                  { key: 'inner-blocks',
                  allowedBlocks: [ 'custom/accordion-item' ]
                }
                )
              
            );
      },

      save: function () {
          var blockProps = useBlockProps.save({ className: 'accordion' });

          return el(
              'div',
              blockProps,
              el(
                InnerBlocks.Content
              )
          );
      },
  });

} )( window.wp.blocks, window.wp.blockEditor, window.wp.element );
var analytics;

jQuery(document).ready(function($) {
  
  analytics = {

    init: function() {

      try {

        if( theme.analytics.type == "gtag" )
          this.setupGtag();

        this.setupGTM();

      } catch(ex) {

        console.log("Err:", ex);

      }

    },

    getUaIds: () => {

      return loadVars.uaIds;

    },

    getGtmIds: () => {

      return loadVars.gtmIds;

    },

    setupGtag: function() {

      if( ! theme.isDev() && ! theme.isLoggedIn() ) {

        console.log( "Enqueueing analytics IDs: ", this.getUaIds() );
        var ids = this.getUaIds();

        if( ids.length > 0 ) {

          (function(s,i,m,a,r,d) {

            r = s.createElement(i),
            d = s.getElementsByTagName(i)[0];
            r.src = m + a;
            d.parentNode.insertBefore(r,d);

            window.dataLayer  = window.dataLayer || [];
            window.gtag       = function() {
              dataLayer.push(arguments);
            }
            gtag('js', new Date());


          })(document, 'script', 'https://www.googletagmanager.com/gtag/js?id=', ids[0]);

          ids.forEach(function(id, n) {
            gtag('config', id)
          });

        }

      } else {

        console.log("Development site or admin login detected. No analytics enqueued.");

      }

    },

    setupGTM: function() {

      var self  = this;
      var ids   = self.getGtmIds();

      if( ids.length > 0 ) {

        ids.forEach( function( id, n ) {

          self.enqueueGtmContainer( window, document, 'script', 'dataLayer', id );

        });
        

      }

    },

    enqueueGtmContainer: ( w, d, s, l, i ) => {
      w[l]=w[l]||[];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event:'gtm.js'
      });

      var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),
        dl=l!='dataLayer'?'&l='+l:'';

      j.async=true;
      j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;
      f.parentNode.insertBefore(j,f);

    }

  };

  analytics.init();

});

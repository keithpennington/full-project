var effects;

jQuery(document).ready(function($) {
  
  effects = {

    init: function() {

      setTimeout( () => {

        this.loaded.init();
        this.scroll.init();
        this.visibility.init();
        this.resize.init();
        this.parallax.init();
        
      }, 500);

    },

    loaded: {

      loadedClass: "has-loaded",

      init: function() {

        $("body").addClass(this.loadedClass);
        $(window).on("unload", function() {
          $("body").removeClass(this.loadedClass);
        });
        
      }

    },

    scroll: {

      direction: null,
      bottomClass: "at-bottom",
      scrolledClass: "scrolled",

      bottom: () => {
        return $(document).height() - $(window).height() - 5;
      },

      init: function() {

        this.adjust();
        this.click();

        if( this.overThresh(theme.scrolledThreshold) )
          $("body").addClass( this.scrolledClass );

        if( this.overThresh( this.bottom() ) )
          $("body").addClass( this.bottomClass );

        this.listeners();

      },

      listeners: function() {

        document.addEventListener("scroll", (e) => {
          effects.scroll.scrolled(e);
        });

      },

      position: (destination) => {

        var hash, pos;

        if(typeof destination == "string" && destination.search("#") !== -1) {

          if(destination.charAt(0) !== "#")
            hash = "#" + destination.split("#")[1];
          else
            hash = destination;

          if( $(hash).length === 0)
            pos = false;
          else
            pos = $(hash).offset().top - 20;

        } else {

          pos = false;

        }

        return pos && theme.header.isFixed() ? pos - theme.header.height() : pos;

      },

      animate: (pos, hash) => {

        if(pos === false || typeof pos != "number")
          return;

        $("html,body").animate(
          {
            scrollTop: pos
          },
          800,
          'swing'
        );

        general.dispatchEvent("effects.scroll.animated", {
          hash: hash,
          position: pos
        });

      },

      click: function() {

        var self = this;
        
        $("body").on('click', "a:not([data-toggle])", (e) => {

          var link = e.currentTarget;

          var same = link.href.split("#")[0] == window.location.href.split("#")[0];

          if( same && self.position(link.href) || $(link).attr("href") == "#" ) {

            e.preventDefault();

            try {

              self.animate(
                self.position(link.href), link.href
              );

            } catch(e) {

              console.log("Err: ", e);

            }
          }

        });

      },

      scrolled: function(e) {

        if(effects.scroll.overThresh(theme.scrolledThreshold)) {

          if( ! $("body").hasClass(effects.scroll.scrolledClass) ) {

            $("body").addClass(effects.scroll.scrolledClass);
            general.dispatchEvent( "effects.scroll.scrolled", {
              scrollEvent: e,
              threshold: theme.scrolledThreshold
            });

          }

        } else if( $("body").hasClass(effects.scroll.scrolledClass) )
          $("body").removeClass(effects.scroll.scrolledClass);

        if( effects.scroll.overThresh( effects.scroll.bottom() ) ) {

          if( ! $("body").hasClass(effects.scroll.bottomClass) ) {

            $("body").addClass(effects.scroll.bottomClass);
            general.dispatchEvent( "effects.scroll.bottom", {
              scrollEvent: e,
              threshold: effects.scroll.bottom()
            });

          }

        } else if( $("body").hasClass(effects.scroll.bottomClass) )
          $("body").removeClass(effects.scroll.bottomClass);

      },

      overThresh: (thresh) => {

        return document.body.scrollTop > thresh || document.documentElement.scrollTop > thresh ? true : false;

      },

      adjust: function() {

        var hash = document.location.hash;
        if( hash != "" ) {

          this.animate(
            this.position( hash ), hash
          );

        }

      }

    },

    visibility: {

      elements: {
        winHeight: $(window).height(),
        invisible: $(".when-visible:not(.is-visible)"),
        nowVisible: new Array()
      },

      init: function() {

        var self = effects.visibility;

        self.setDataAttributes( () => {
          window.addEventListener( 'resize', () => {
            requestAnimationFrame(self.setDataAttributes);
          } );
          var resized = new ResizeObserver( entries => {

            if(entries.length > 0)
              requestAnimationFrame(self.setDataAttributes);

          });
          resized.observe(document.body);

          self.checkVisibility({}, () => {

            if( $(self.elements.invisible).length > 0 )
              window.addEventListener('scroll', self.listen);

          } );

        } );

      },

      setDataAttributes: (cb) => {

        self = effects.visibility;

        $(self.elements.invisible).each(function(i, el) {

          $(el).attr({
            "data-start": $(el).offset().top + ( self.elements.winHeight * .15 )
          });

        });

        if( typeof cb == "function" )
          cb();

      },

      checkVisibility: (e, cb) => {

        var items = effects.visibility.elements;

        if( ! items.nowVisible.length > 0 ) {

          $(items.invisible).each( function(i, el) {

            var start = $(el).data("start");
            var y = $(window).scrollTop() + items.winHeight;

            if( y > start && items.nowVisible.indexOf(el) === -1 ) {

              items.nowVisible.push(el);
              
            }

          } );

          if(items.nowVisible.length > 0)
            effects.visibility.turnVisible(e);

        }

        if( typeof cb == "function" )
          cb();

      },

      listen: (e) => {

        requestAnimationFrame( () => {

          effects.visibility.checkVisibility(e);

          if( $(effects.visibility.elements.invisible).length == 0 )
            window.removeEventListener('scroll', effects.visibility.listen);

        } );

      },

      turnVisible: function(e) {

        var self = effects.visibility;
        
        general.dispatchEvent(
          "effects.visibility.show",
          {
            event: e,
            items: self.elements.nowVisible
          }
        );
        
        $(self.elements.nowVisible).addClass("is-visible");
        self.elements.invisible = $(".when-visible:not(.is-visible)");
        self.elements.nowVisible = new Array();

      }

    },

    resize: {

      screen: null,
      width: 0,

      init: function() {

        this.screen = this.isSize({});
        this.width  = $(window).width();
        this.listen();

      },

      isSize: function( args ) {

        var result = false;
        var width  = $(window).width();
        var screen = typeof args.screen != "undefined" ? args.screen : false;
        var exact  = typeof args.exact != "undefined" ? args.exact : false;

        theme.breakpoints.forEach( (bp, i) => {

          if( ! result ) {

            if( ! screen && width >= bp.limits[0] && width <= bp.limits[1] ) {

              result = bp.name;

            } else if( bp.name === screen ) {

              if(i === 0) {

                result = width <= bp.limits[1] ? true : false;

              } else {

                // 
                result = width >= bp.limits[0] ? true : false;
                result = exact && width > bp.limits[1] ? false : result;

              }

            }

          }

        } );

        return result;

      },

      listen: () => {

        var self = effects.resize;

        $(window).on( "resize", function(e) {

          requestAnimationFrame( () => {

            var currentScreen = self.isSize({});

            if( self.screen != currentScreen ) {

              var dir   = $(window).width() > self.width ? "bigger" : "smaller";
              var data  = {
                direction: dir,
                resizeEvent: e,
                screen: currentScreen
              };
              general.dispatchEvent( "viewportChange", data );
              self.screen = currentScreen;
              self.width = $(window).width();

            }

          } );

        } );

      }

    },

    parallax: {

      defaults: {
        selector: ".has-parallax",
        speed: 0.5
      },
      items: [],

      getProp: function(prop) {

        var value = this.defaults[prop];

        if( "undefined" !== typeof theme.parallax && "undefined" !== typeof theme.parallax[prop] )
          value = theme.parallax[prop];

        return value;

      },

      init: function() {

        this.collectItems();
        this.listeners();

      },

      listeners: function() {

        var self = this;

        $(window).on("resize", function(e) {
          requestAnimationFrame(function() {
            self.collectItems();
          });
        });

        $(document).on("scroll", function(e) {
          requestAnimationFrame(function() {
            if( self.items.length > 0 )
              self.adjustImages();
          });
        });

      },

      collectItems: function(callback) {

        var sel = this.getProp("selector");
        var self = this;

        if( sel ) {

          $(sel).each(function(i, item) {

            var data = self.calcAttributes(item);
            self.items.push(data);

          });

        }

        this.adjustImages();

      },

      adjustImages: function() {

        var win = {
          bottom: $(window).scrollTop() + $(window).height(),
          top: $(window).scrollTop(),
          height: $(window).height(),
        }, self = this;

        $(this.items).each(function(i, item) {

          if( win.bottom > item.top && win.top < item.top + item.height )
            $(item.element).css("background-position-y", self.getBackgroundPosition(item, win));

        });

      },

      calcAttributes: function(el) {

        var data = {
          bottom: $(el).offset().top + $(el).outerHeight(),
          element: el,
          height: $(el).outerHeight(),
          top: $(el).offset().top,
        };

        return data;

      },

      getBackgroundPosition: function(item, win) {

        var speed = this.getProp("speed");
        var min = this.getProp("min");
        var max = this.getProp("max");
        var bot = (win.bottom - item.bottom) * speed;
        var top = win.height + item.height;
        console.log(speed, bot, top);
        var percent = (50 - (speed / 2)) + ((bot / top) * 100);

        return percent + "%";

      }

    }

  };

  effects.init();

});

<?php

/**
 * Archives template file used to render non-single queries
 *
 * This template will be used for Tag, Category and Taxonomy pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

global $wp_query;

$props = [];

if( $wp_query->is_tax || $wp_query->is_tag || $wp_query->is_category )
  $props = Utility::getArchivePageProperties( $wp_query->queried_object );

if( $wp_query->is_tax )
  $pageType = "taxonomy-archive";
elseif( $wp_query->is_category )
  $pageType = "category-archives";
elseif( $wp_query->is_tag )
  $pageType = "tag-archive";
elseif( $wp_query->is_date ) {

  $vars = $wp_query->query_vars;
  $date = date( 'M Y', mktime( intval($vars['hour']), intval($vars['minute']), intval($vars['second']), intval($vars['monthnum']), ( 0 === intval($vars['day']) ? 1 : intval($vars['day']) ), intval($vars['year']) ) );
  $props['sidebar']   = "blog";
  $pageType           = "date-archive";
  $props['title']     = "Recent Articles";
  $props['subtitle']  = $date;
  
}

get_header();

Utility::getScopedTemplatePart(
  "template-parts/hero/hero",
  "archive",
  $props
);

Utility::getScopedTemplatePart(
  "template-parts/content/content",
  "feed",
  [
    'page_type' => $pageType,
    'sidebar'   => $props['sidebar']
  ]
);

if( isset( $props['sidebar'] ) && false !== $props['sidebar'] ) {

  Utility::getScopedTemplatePart(
    "template-parts/aside/aside",
    null,
    [
      'sidebar' => $props['sidebar']
    ]
  );

}

get_footer();

<?php
/**
 * Template Name: Full Hero
 * Template Post Type: post, page, service
 *
 * Template for short-form content where the entire page is contained by a Bulma Hero
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

get_header();

if( have_posts() ) {

  while( have_posts() ) {

    the_post();

    Utility::getScopedTemplatePart(
      "template-parts/hero/hero",
      "full-content",
      [
        'classes' => ["has-text-centered"]
      ]
    );

  }

}

get_footer();

<?php
/**
 * Template Name: Full Width No Hero
 * Template Post Type: post, page, service
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

get_header();

if ( have_posts() ) {

  while ( have_posts() ) {

    the_post();

    Utility::getScopedTemplatePart(
      "template-parts/content/content",
      null,
      [
        'section_classes' => [ "p-0" ]
      ]
    );

  }

} else {

  Utility::getScopedTemplatePart("template-parts/hero/hero", null);
  Utility::getScopedTemplatePart("template-parts/content/content", "none");
  
}

get_footer();

<?php
/**
 * Template Name: Default Left Sidebar
 * Template Post Type: post, page, service
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

$sidebar  = is_active_sidebar( "blog" ) ? "blog" : false;

get_header();

if( have_posts() ) {

  while( have_posts() ) {

    the_post();

    Utility::getScopedTemplatePart(
      "template-parts/hero/hero",
      null
    );

    Utility::getScopedTemplatePart(
      "template-parts/content/content",
      null,
      [
        'main_classes' => [ "left-sidebar" ],
        'sidebar' => $sidebar
      ]
    );

    if( false !== $sidebar ) {
      Utility::getScopedTemplatePart(
        "template-parts/aside/aside",
        null,
        [
          'sidebar' => $sidebar
        ]
      );
    }

  }

} else {

  Utility::getScopedTemplatePart( "template-parts/hero/hero", null );
  Utility::getScopedTemplatePart( "template-parts/content/content", "none" );

}

get_footer() ?>

<?php
/**
 * Header Template
 *
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

if( is_single() || is_page() )
  $articleId = sprintf( "%s-%s", get_post_type(), get_the_ID() );
else
  $articleId = "site-article";

$metaTags = [
  'google-site-verification'      => Utility::getGoogleProperty( 'gsc' ),
  'facebook-domain-verification'  => Utility::getOtherProperty( 'fbdv' ),
  'ahrefs-site-verification'      => Utility::getOtherProperty( 'ahrefv' ),
  'author'                        => Utility::getAuthor(),
  'description'                   => Utility::getMetaDescription()
];

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
  <link data-prerender="keep" rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri() . "/assets/favicon.png" ?>" />
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php

  foreach( $metaTags as $name => $content )
    echo $content && ! empty( $content ) ? sprintf( "\t<meta name=\"%s\" content=\"%s\" />\n", $name, $content ) : "";

  echo sprintf( "<!-- Open Graph -->\n%s<!-- End Open Graph -->\n", Utility::theOg() );

  echo sprintf( "<title>%s</title>\n", Utility::getMetaTitle() );

  wp_head();

  ?>
  </head>
  <body <?php body_class() ?>>

  <?php Utility::getScopedTemplatePart( "template-parts/nav/nav", "main" ); ?>
  <article id="<?php echo $articleId ?>" <?php post_class("hentry") ?>>

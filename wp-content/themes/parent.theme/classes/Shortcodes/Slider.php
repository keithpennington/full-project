<?php

namespace Theme\Parent\Shortcode;

use Theme\Parent\{ Shortcode, Utility, PostType\Slide };
use Theme\Child\Loader as Theme;
use WP_Query;

class Slider extends Shortcode {
  
  use \Theme\Parent\Traits\Instance;

  public $slug = "slider";
  public $name = "Slider";
  public $atts;

  function __construct() {

    parent::__construct();

    add_action( "wp_enqueue_scripts", [ $this, "addDependencies"] );

  }

  public function addDependencies() {

    global $post;

    if( has_shortcode( get_the_content( $post ), $this->slug ) ) {

      wp_enqueue_script(
        "flickity",
        get_template_directory_uri() . "/vendor/flickity-2.2.1/flickity.min.js",
        [ "jquery" ],
      );

      wp_enqueue_script(
        "flickity-fullscreen",
        get_template_directory_uri() . "/vendor/flickity-2.2.1/fullscreen/fullscreen.js",
        [ "jquery", "flickity" ],
      );

      wp_enqueue_style(
        "flickity",
        get_template_directory_uri() . "/vendor/flickity-2.2.1/flickity.css",
      );

    }

  }

  public function execute( $atts, $content = null ) {

    $slideCPT       = Slide::instance();
    $term           = get_term_by( "slug", $atts['name'], "sliders" );
    $defaults       = $this->getSliderDefaults();
    $savedOptions   = false !== $term ? get_term_meta( $term->term_id, "_" . $slideCPT->slidersMetabox['id'], true ) : [];

    if( ! empty( $savedOptions ) )
      array_walk( $savedOptions, "Theme\Parent\Utility::parseNumberVals", Slide::$numberFields );

    $this->atts     = $atts;
    $atts           = shortcode_atts(
      array_merge(
        wp_parse_args( $savedOptions, $defaults ),
        [
          'name'          => "",
          'classes'       => "",
          'cellSelector'  => ".slide"
        ]
      ),
      $atts,
      $this->slug
    );

    $name = str_replace( " ", "-", strtolower( $atts['name'] ) );

    // Get slides for slider
    $args   = $this->getSlideQuery();
    $slides = new WP_Query( $args );

    ob_start();

    if( $slides->have_posts() ) {

      $atts     = apply_filters( 'slider_options', $atts, $this->atts );
      $id       = "slider-" . $name;
      $classes  = self::getSliderClasses( $atts );

      // Slider container
      echo sprintf( "<section id='%s' class='%s'>\n", $id, implode( " ", $classes ) );

      while( $slides->have_posts() ) {

        $slides->the_post();

        echo $this->getSlideContent( $atts );

      }

      // End slider container
      echo "</section>\n";
      echo self::getSliderStyles( "#" . $id, $atts );

      // Initialization script
      if( $slides->post_count > 1 )
        echo self::getSliderInit( "#" . $id, $atts );

    } else {

      echo $content;

    }

    wp_reset_postdata();

    $output = apply_filters( "slider_{$name}_output", ob_get_clean(), $atts );

    return apply_filters( 'slider_output', $output, $atts );

  }

  public function getSlideQuery() {

    $query = [
      'post_type' => "slide",
      'post_status' => "publish",
      'tax_query'   => [
        'relation' => "OR",
        [
          'taxonomy'  => "sliders",
          'field'     => "slug", 
          'terms'     => $this->atts['name']
        ],
        [
          'taxonomy'  => "sliders",
          'field'     => "name",
          'terms'     => $this->atts['name']
        ]
      ],
      'orderby'   => "menu_order",
      'order'     => "ASC"
    ];

    return apply_filters( 'slider_slide_query', $query, $this->atts );

  }

  public function getSliderDefaults() {

    $defaults = array_combine(
      array_column(
        Slide::instance()->slidersMetabox['fields'],
        "id"
      ),
      array_column(
        Slide::instance()->slidersMetabox['fields'],
        "default"
      )
    );

    $theme = Theme::instance();

    if( property_exists( $theme, "defaultSliderOptions" ) ) {

      $themeDefaults  = $theme->defaultSliderOptions;
      $defaults       = wp_parse_args( $themeDefaults, $defaults );

    }

    return apply_filters( 'slider_option_defaults', $defaults, $this->atts );

  }

  public function getSlideContent( $atts ) {

    $options      = get_post_meta( get_the_ID(), "_options", true );
    $slideWrapper = "<div class='" . ltrim( $atts['cellSelector'], "." ) . "'>\n\t%s\n</div>\n";

    if( $options && isset( $options['link'] ) && ! empty( $options['link'] ) ) {

      $linkWrapper  = sprintf( "<a class='slide-link' target='%s' href='%s'>", $options['target'], $options['link'] );
      $linkWrapper .= "\n\t\t%s\n\t</a>\n";
      $slideWrapper = sprintf( $slideWrapper, $linkWrapper );

    }

    return sprintf( $slideWrapper, str_replace( "]]>", "]]&gt;", apply_filters( 'the_content', "<div class='content'>\n\t" . get_the_content() . "\n</div>" ) ) );

  }

  public static function getSliderInit( $selector, $options ) {

    $js = sprintf(
      "jQuery('%s').flickity(%s)",
      $selector,
      json_encode( $options )
    );

    return sprintf( '<script type="text/javascript">%s</script>', $js );

  }

  public static function getSliderStyles( $selector, $options ) {

    $css = sprintf(
      "<style>%s %s { padding: %s; width: %s }</style>",
      $selector,
      $options['cellSelector'],
      $options['slidePadding'],
      $options['slideWidth']
    );

    return $css;

  }

  public static function getSliderClasses( $atts ) {

    $classes      = array_merge( [ "slider" ], explode( " ", $atts['classes'] ) );
    $classAttMap  = [
      'adaptiveHeight'  => "slide-nav-top",
      'lazyLoad'        => "is-lazyloaded"
    ];

    foreach( $classAttMap as $att => $class ) {

      if( array_key_exists( $att, $atts ) && false !== $atts[$att] )
        $classes[] = $class;

    }

    return $classes;

  }

}

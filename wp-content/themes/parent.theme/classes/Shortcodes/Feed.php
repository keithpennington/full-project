<?php

namespace Theme\Parent\Shortcode;

use Theme\Parent\{ Shortcode, Utility };
use WP_Query;

class Feed extends Shortcode {
  
  use \Theme\Parent\Traits\Instance;
  
  public $slug = "feed";
  public $name = "Feed";

  protected static $_instance;
  protected $_defaults = [
    'align'     => "center",
    'format'    => "list",
    'limit'     => 10,
    'orderby'   => "date",
    'order'     => "DESC",
    'paginate'  => true,
    'pgn_style' => "normal",
    'pgn_top'   => true,
    'pgn_bot'   => true,
    'type'      => "post"
  ];

  private $_defaultTaxonomies = [
    'category' => "category",
    'post_tag' => "tag"
  ];

  public function execute( $atts, $content = null ) {

    if( ! is_array( $atts ) )
      $atts = [];

    if( ! isset( $atts['type'] ) )
      $atts['type'] = "post";

    if( $atts['type'] === "post" ) {

      $taxonomies = $this->_getUrlTaxonomies();

      if( ! empty( $taxonomies ) ) {

        $this->_defaults['category'] = isset( $taxonomies['category'][1] ) ? $taxonomies['category'][1] : '';
        $this->_defaults['tags'] = isset( $taxonomies['tag'][1] ) ? $taxonomies['tag'][1] : '';

      }

    } else {

      $taxonomies = $this->_getPostTypeTaxonomiesFromAtts( $atts, $atts['type'] );
      $this->_defaults['category'] = isset( $taxonomies['category'][0] ) ? $taxonomies['category'][0] : '';
      $this->_defaults['tags'] = isset( $taxonomies['tag'][0] ) ? $taxonomies['tag'][0] : '';

    }
    
    $atts = shortcode_atts( $this->_defaults, $atts, $this->slug );
    $page = get_query_var( 'paged' ) ? absint( get_query_var( 'paged') ) : 1;
    $args = [
      'orderby'         => $atts['orderby'],
      'order'           => $atts['order'],
      'paged'           => $page,
      'posts_per_page'  => $atts['limit'],
      'post_type'       => $atts['type']
    ];

    if( ! empty( $atts['category'] ) ) {
      $categories = array_map( 'trim', explode( ",", $atts['category'] ) );
      $args['tax_query'][] = [
        'taxonomy'  => "category",
        'field'   => "name",
        'terms'   => $categories
      ];
    }

    if( ! empty( $atts['tags'] ) ) {
      $tags = array_map( 'trim', explode( ",", $atts['tags'] ) );
      $args['tax_query'][] = [
        'taxonomy'  => "post_tag",
        'field'   => "name",
        'terms'   => $tags,
        'operator'  => 'AND'
      ];
    }

    if( isset( $args['tax_query'] ) && count( $args['tax_query'] ) > 1 )
      $args['tax_query']['relation'] = "AND";
    
    $items = new WP_Query( $args );
    
    ob_start();

    if( $items->have_posts() ) {

      Utility::getScopedTemplatePart(
        "template-parts/Vanilla/shortcode/feed",
        "container",
        [
          'atts'  => $atts,
          'items' => $items
        ]
      );

    }

    $output = apply_filters( "html_{$this->slug}_{$atts['type']}_output", ob_get_clean(), $atts, $items );

    wp_reset_postdata();

    return apply_filters( "html_{$this->slug}_output", $output, $atts, $items );

  }

  private function _getUrlTaxonomies() {
    
    global $wp_rewrite;
    $taxonomies = [];
    $pageLink   = html_entity_decode( get_pagenum_link() );

    foreach( $this->_defaultTaxonomies as $taxonomy => $slug ) {

      $structure  = $wp_rewrite->extra_permastructs[$taxonomy]['struct'];
      $pattern  = str_replace( "/", "\/", ltrim( $structure, "/" ) );
      $pattern  = "/" . str_replace( "%" . $taxonomy . "%", "([-\w]+)", $pattern ) . "/";
      preg_match( $pattern, $pageLink, $taxonomies[$slug] );

    }

    return $taxonomies;

  }

  private function _getPostTypeTaxonomiesFromAtts( array $atts, string $type ): array {

    $available = get_object_taxonomies( $type, 'names' );
    
    $taxonomies = [];

    foreach( $atts as $att => $val ) {

      if( taxonomy_exists( $att ) ) {

        $args = [
          'taxonomy' => $att,
          'hide_empty' => true,
        ];
        $terms = get_terms( $args );

        if( is_array( $terms ) && 0 < count($terms) )
          $taxonomies[$att] = array_map( "trim", explode( ",", $val ) );

      }

    }

    return $taxonomies;

  }

  public static function printReference() {

    ob_start() ?>

    <strong>Basic Usage:</strong> <code>[feed]</code>
    <p class="card-text mt-2">
      Shortcode to print media articles for any post type on a page.  This should be used for the standard <a target="_new" href="/blog/">Blog page</a>. Using this shortcode on a page will support pagination if there are more than 1 page of posts (total number of posts is more than the limit).  Pagination clicks will result in page URLs like <samp>/blog/page/3/</samp>
    </p>
    <table class="table mt-2">
      <thead class="thead-light">
        <tr>
          <th scope="col">Attribute</th>
          <th scope="col">Functionality</th>
          <th scope="col">Default</th>
          <th scope="col">Possible Values</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">
            align
          </th>
          <td>
            Alignment of pagination items.
          </td>
          <td>
            "center"
          </td>
          <td>
            "center", "left" or "right"
          </td>
        </tr>
        <tr>
          <th scope="row">
            category
          </th>
          <td>
            A comma-separated list of the cateogies you want to display from.  These should match the "Name" as listed in your <a target="_new" href="/wp-admin/edit-tags.php?taxonomy=category">post categories</a>.
          </td>
          <td>
            <var>all</var>
          </td>
          <td>
            Any <a target="_new" href="/wp-admin/edit-tags.php?taxonomy=category">post category</a> or categories.  Listing multiple categories will show all posts that have any of the categories.  For example, "Event, Sale" would show posts with either category.
          </td>
        </tr>
        <tr>
          <th scope="row">
            format
          </th>
          <td>
            Display posts as a list, or as tiles. Tile view is not yet supported.
          </td>
          <td>
            "list"
          </td>
          <td>
            <p>List is the only current option.</p>
            <p>Future options will include <code>tile</code> and <code>hero</code></p>
          </td>
        </tr>
        <tr>
          <th scope="row">
            limit
          </th>
          <td>
            Define how many posts per page you want to display. Must be an integer.
          </td>
          <td>
            10
          </td>
          <td>
            "3", "10", "20", etc.  You can use this in tandem with the pagination attribute to show a strict number of posts without pagination.  See below for example.
          </td>
        </tr>
        <tr>
          <th scope="row">
            orderby
          </th>
          <td>
            Order the posts by a specified attribute. The default value orders by the official published date (not the modified date).  You can force a single post to the top of the list by marking is as a "sticky" post.
            <br><a href="https://www.wpbeginner.com/beginners-guide/how-to-make-sticky-posts-in-wordpress/" target="_new">Learn more about sticky posts</a>
          </td>
          <td>
            "date"
          </td>
          <td>
            One of the available <a href="https://developer.wordpress.org/reference/classes/wp_query/#order-orderby-parameters" target="_new">orderby parameters</a>.
          </td>
        </tr>
        <tr>
          <th scope="row">
            order
          </th>
          <td>
            The order, ascending or descending, that the posts should render based on the <var>orderby</var> attribute.
          </td>
          <td>
            "DESC"
          </td>
          <td>
            "ASC" or "DESC" only. Using "DESC" on a date or numeric attribute will render newest to oldest, or highest to lowest. Using "ASC" on an alphabetical attribute will render A-Z.
          </td>
        </tr>
        <tr>
          <th scope="row">
            paginate
          </th>
          <td>
            Whether or not pagination should be shown if there are more posts available than the number used in <samp>limit</samp>.
          </td>
          <td>
            <var>true</var>
          </td>
          <td>
            "true" or "false"
          </td>
        </tr>
        <tr>
          <th scope="row">
            pgn_style
          </th>
          <td>
            
          </td>
          <td>
            
          </td>
          <td>
            
          </td>
        </tr>
        <tr>
          <th scope="row">
            pgn_top
          </th>
          <td>
            Boolean whether to show the top pagination.
          </td>
          <td>
            <var>true</var>
          </td>
          <td>
            "true" or "false"
          </td>
        </tr>
        <tr>
          <th scope="row">
            pgn_bot
          </th>
          <td>
            Boolean whether to show the bottom pagination.
          </td>
          <td>
            <var>true</var>
          </td>
          <td>
            "true" or "false"
          </td>
        </tr>
        <tr>
          <th scope="row">
            tags
          </th>
          <td>
            A tag or list of tags that you want to limit the posts by. These should match the "Name" as listed in your <a target="_new" href="/wp-admin/edit-tags.php?taxonomy=post_tag">post tags</a>.
          </td>
          <td>
            <var>all</var>
          </td>
          <td>
            One or many <a target="_new" href="/wp-admin/edit-tags.php?taxonomy=post_tag">post tag names</a>. Listing multiple tags will show posts that have all of the matching tags.
          </td>
        </tr>
        <tr>
          <th scope="row">
            type
          </th>
          <td>
            The slug name of the post type you can to show in the content feed.
          </td>
          <td>
            "post"
          </td>
          <td>
            Any registered post type slug.  This can be found in the URL structure <samp>/wp-admin/edit.php?post_type=<kbd>service</kbd></samp> (except for posts and pages, which are part of the core).  This field only accepts one value for a post type.
          </td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Example</th>
          <th scope="col">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">
            1
          </td>
          <td>
            <code>[feed category="Event, Course" tags="free" heading="Free Events and Courses" orderby="modified"]</code>
          </td>
          <td>
            Show posts that are in the "Event" or "Course" category that have a tag of "free". Prints a centered <code>h3</code> tag that reads "Free Events and Courses" at the top. Orders the posts by the modified date so that most recently updated ones show first.<br><b>Note:</b> when keeping the order as "DESC", it's not necessary to re-declare it since that's already the default.
          </td>
        </tr>
        <tr>
          <th scope="row">
            2
          </td>
          <td>
            <code>[feed limit="3" pagination="false"]</code>
          </td>
          <td>
            Show the last 3 posts, ordered by the published date, descending.  The homepage is a good place to use a feed like this.
          </td>
        </tr>
        <tr>
          <th scope="row">
            3
          </td>
          <td>
            <code>[feed limit="6" tags="video, DIY"]</code>
          </td>
          <td>
            Show posts with tags "video" and "DIY", displaying 6 posts per page.
          </td>
        </tr>
      </tbody>
    </table>

    <?php echo ob_get_clean();

  }

}

<?php

namespace Theme\Parent\Shortcode;

use Theme\Parent\{ Shortcode, Utility, Framework\Bulma };

class Modal extends Shortcode {
  
  use \Theme\Parent\Traits\Instance;

  private $_counter = 0;

  public $slug = "modal";
  public $name = "Modal";
  
  protected static $_instance;

  public function execute( $atts, $content = null ) {

    if( defined( "CONTENTFRAMEWORK" ) && "Bulma" === CONTENTFRAMEWORK )
      $output = $this->bulma( $atts, $content );
    else
      $output = $content;

    return apply_filters( 'modal_output', $output, $atts, $content );

  }

  public function bulma( $atts, $content ) {

    $defaults = [
      'class'   => "block box",
      'label'   => "",
      'style'   => Bulma::getDefaultStyle( "button" ),
      'target'  => false,
      'title'   => "",
      'type'    => "basic"
    ];
    $atts       = shortcode_atts( $defaults, $atts, $this->slug );
    $style      = "none" !== $atts['style'] ? Bulma::getStyle( $atts['style'] ) : false;
    $btnClasses = false !== $style ? array_unique( [ "button", $style ] + explode( " ", $atts['style'] ) ) : [];
    $divClass   = false !== $atts['target'] && null === $content ? false : "modal-target";
    $selector   = false !== $atts['target'] ? $atts['target'] : "#modal-content-" . $this->_counter;

    $output = sprintf(
      '<a class="%s" href="%s" data-toggle="modal" data-title="%s" data-type="%s">%s</a>',
      implode( " ", $btnClasses ),
      $selector,
      $atts['title'],
      $atts['type'],
      $atts['label']
    );

    if( false !== $divClass ) {
      
      $classes = $atts['class'] . " " . $divClass;
      $output .= sprintf(
        '<div id="%s" class="%s">%s</div>',
        ltrim( $selector, "#" ),
        $classes,
        do_shortcode( $content )
      );

    }

    $this->_counter++;

    return $output;

  }

  public static function printReference() {

    ob_start() ?>

    <strong>Basic Usage:</strong> <code>[modal label="Read Our Story"] << Story Content Here >> [/modal]</code>
    <p class="card-text mt-2">
      Shortcode for rendering a button that opens a modal to a user.  This shortcode streamlines the html output that is used for setting up the triggers between buttons and shown content.  Eventually it will be developed into a Guternberg Block or block group so that users can set it up within the UI by altering the options available on standard the button block.
    </p>
    <p class="card-text mt-1">
      Write content&mdash;or stack WP Blocks&mdash;between the opening and closing shortcode tags.  This content will be shown when the modal button is clicked.  The shortcode automatically generates a styled button using the value of the <code>label</code> attribute wherever it is placed.
    </p>

    <table class="table mt-2">
      <thead class="thead-light">
        <tr>
          <th scope="col">Attribute</th>
          <th scope="col">Functionality</th>
          <th scope="col">Default</th>
          <th scope="col">Possible Values</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">
            class
          </th>
          <td>
            Sets the class names on the content container that will be shown in the modal.
          </td>
          <td>
            block box
          </td>
          <td>
            <var>any</var> <i class="ml-1">(only classes that are included in the stylesheet will have an effect)</i>
          </td>
        </tr>
        <tr>
          <th scope="row">
            label
          </th>
          <td>
            The label of the button that will open the modal.
          </td>
          <td>
            <var>blank</var>
          </td>
          <td>
            <var>any</var>
          </td>
        </tr>
        <tr>
          <th scope="row">
            style
          </th>
          <td>
            Button styles that will be used to style the button that opens the modal.
          </td>
          <td>
            "button <?php echo Bulma::getDefaultStyle( "button" ) ?>"
          </td>
          <td>
            These should match the stylesheet conventions for button names, typically using <a target="_new" href="https://bulma.io/documentation/elements/button/">Bulma button element style class names</a>.
          </td>
        </tr>
        <tr>
          <th scope="row">
            target
          </th>
          <td>
            CSS selector of the target content that will be shown when the button is clicked to show the modal.
          </td>
          <td>
            <var>false</var> &mdash; No target is provided, and the content found between the shortcode tags is generated into a hidden div container automatically.
          </td>
          <td>
            Any CSS selector for a shown or hidden element on the page. E.g. <code>#our-story</code>, <code>#services .service.lawns</code>
          </td>
        </tr>
        <tr>
          <th scope="row">
            title
          </th>
          <td>
            The title of the modal that will be shown. <i>Applies to the "card" modal type only.</i>
          </td>
          <td>
            <var>blank</var>
          </td>
          <td>
            <var>any</var>
          </td>
        </tr>
        <tr>
          <th scope="row">
            type
          </th>
          <td>
            The type of modal that should be used to present the modal content.  Modal type should compliment the content presentation.
          </td>
          <td>
            "basic"
          </td>
          <td>
            "basic", "card" or "image" &mdash; <a target="_new" href="https://bulma.io/documentation/components/modal/">See Bulma documentation</a> for generic examples.
          </td>
        </tr>
      </tbody>
    </table>

    <table class="table mt-2">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Example</th>
          <th scope="col">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">
            1
          </th>
          <td>
            <code>[modal label="Share" type="basic" target="#share-post" style="button is-secondary is-outlined" /]</code>
          </td>
          <td>
            An outline-styled button with a label "Share", which opens a share modal for a blog post.  This shortcode example assumes that the <var>#share-post</var> div is already on the page.
          </td>
        </tr>
        <tr>
          <th scope="row">
            2
          </th>
          <td>
            <code>[modal label="Why <?php echo bloginfo( 'name' ) ?>?" type="card" class="content has-background-dark has-text-light" title="Discover <?php echo bloginfo( 'name' ) ?>" style="button is-secondary is-large is-fullwidth"]</code>
            <br>
            &lt;!-- WP Blocks Here --&gt;
            <br>
            <code>[/modal]</code>
          </td>
          <td>
            A large, full-width button that says "Why <?php echo bloginfo( 'name' ) ?>?".  When clicked, it opens a card modal with a title "Discover <?php echo bloginfo( 'name' ) ?>", showing the content of the WP Blocks between the opening modal and the closing one.
          </td>
        </tr>
      </tbody>
    </table>

    <?php echo ob_get_clean();

  }

}

<?php 

namespace Theme\Parent;

use DOMElement, WP_Term, WP_Post, WP_Query, WP_User_Query;

class Utility {

  public static $locals = [
    "127.0.0.1",
    "localhost"
  ];

  /**
   * Regular Expression to capture Font Awesome icon names throughout theme.
   * @access public
   * @var string RegEx
   */
  public static $faExp = '/(fa-.*|^fas|^fab|^far|^fad|^fal)/im';
  
  /**
   * isLocal
   *
   * @access public
   *
   * @return bool Whether or not the current environment is local or production.
   */
  public static function isLocal() {

    return in_array( $_SERVER['SERVER_ADDR'], self::$locals ) ? true : false;

  }

  /**
   * debug
   *
   * @access public
   *
   * @param array $terms The entities that you want to debug -- they will be dumped and printed in a <pre> tag.
   *
   * Similar to:
   * @link https://developer.wordpress.org/reference/functions/wp_die/
   */
  public static function debug( $terms, $logOnly = false ) {

    if( defined( "WP_DEBUG" ) && false === WP_DEBUG )
      return;

    if( ! is_array( $terms ) )
      $terms = [ $terms ];

    ob_start();
    
    foreach( $terms as $term )
      var_dump( $term );

    $debug = ob_get_clean();
    
    error_log($debug);
    
    if( false === $logOnly )
      echo sprintf( "<pre style='max-width: 800px;margin: 20px auto;overflow-x: scroll;border: 1px solid #aaa;padding: 20px;box-sizing: border-box;'>%s</pre>", htmlentities( $debug ) );

  }

  /**
   * Object Updates
   * Method for checking whether an object passed to it has any updates
   * that need processing by checking an option's array of updates that happened
   * on this theme already.  Updates the option with result.
   *
   * @param (object) A class against which to check updates
   *
   * @return void
   */
  public static function objectUpdates( $obj ) {

    if( ! is_object( $obj ) || ! property_exists( $obj, "updates" ) )
      return;

    $name           = get_class( $obj );
    $themeUpdates   = get_option( "loc_object_updates", [] );
    $objUpdates     = $obj->updates;
    $updateVersions = array_keys( $obj->updates );
    $updated        = false;
    $newUpdates     = false;

    if( isset( $themeUpdates[$name] ) && is_array( $themeUpdates[$name] ) && ! empty( $updateVersions ) )
      $newUpdates = array_diff( $updateVersions, $themeUpdates[$name] );
    elseif( ! isset( $themeUpdates[$name] ) && ! empty( $updateVersions ) )
      $newUpdates = $updateVersions;

    if( $newUpdates ) {
      
      $result = self::_handleObjectUpdates( $newUpdates, $obj );

      if( ! empty( $result ) ) {
        
        if( array_key_exists( $name, $themeUpdates) )
          $themeUpdates = array_merge( $themeUpdates[$name], $result );
        else
          $themeUpdates[$name] = $result;

        update_option( "loc_object_updates", $themeUpdates );
        
      }

    }

  }

  /**
   * Handle Object Updates
   * Returns an array of confirmed update methods run against a provided object.
   *
   * @param $updates (array) collection of update handles to process.
   * @param $obj (object) the object against which methods will process.
   *
   * @return (array) list of successful update handles processed.
   */
  private static function _handleObjectUpdates( $updates, $obj ) {

    if( ! is_array( $updates ) )
      $updates = [ $updates ];

    $updatedVersions = [];

    foreach( $updates as $version ) {

      if( isset( $obj->updates[$version] ) ) {

        $method = $obj->updates[$version];

        if( $method && method_exists( $obj, $method ) ) {

          $obj->$method();
          $updatedVersions[] = $version;

        }
          
      }

    }

    return $updatedVersions;

  }

  /**
   * getScopedTemplatePart
   *
   * @access public
   *
   * @param string $slug The slug of the template file location, typically beginning with "template-parts/.."
   * @param string (optional) $name The name of the template file relative to the slug. This should be the second half of the file name's hyphenation.
   *
   * Can be used instead of:
   * @link https://developer.wordpress.org/reference/functions/get_template_part/
   */
  public static function getScopedTemplatePart( string $slug, $name = null, $args = [] ) {

    $frameworks     = \Theme\Parent\Loader::$availableFrameworks;
    $frameworkRegEx = "/(" . implode( "|", $frameworks ) . ")/";

    if( ! is_null( $name ) && is_string( $name ) )
      $template_path = "{$slug}-{$name}.php";
    else
      $template_path = "{$slug}.php";

    if( ! preg_match( $frameworkRegEx, $template_path ) )
      $template_path = str_replace( "template-parts", "template-parts/" . CONTENTFRAMEWORK , $template_path );

    if( file_exists( STYLESHEETPATH . "/" . $template_path ) ) {

      do_action( "getScopedTemplatePart_{$slug}", $name, $args );
      include( STYLESHEETPATH . "/" . $template_path );

    } elseif( file_exists( TEMPLATEPATH . "/" . $template_path ) ) {

      do_action( "getScopedTemplatePart_{$slug}", $name, $args );
      include( TEMPLATEPATH . "/" . $template_path );

    } else {

      error_log( "Missing requested file " . $template_path );

    }

  }

  /**
   * Deep Compare
   * 
   * Compare a provided set of parameters against a larger, optionally multi-dimensional data set,
   * to see if all the key/value pairs in $args have matches in $compare
   * 
   * @access public
   * 
   * @param $args (array) The basis arguments required to match against
   * @param $compare (array) The array to compare against
   * 
   * @return (bool) whether all the key/value pairs are found in corresponding key/value pairs in the compare array
   */
  public static function deepCompare( array $args, array $compare ) {

    $return = true;

    foreach( $args as $key => $val ) {

      if( ! $return )
        break;

      if( ! is_array( $val ) ) {

        if( in_array( $key, [ "class", "classes", "className" ] ) && isset( $compare[$key] ) ) {

          $compareClasses = array_map( "trim", explode( " ", $compare[$key] ) );
          if( ! in_array( $val, $compareClasses ) )
            $return = false;

        } elseif( ! array_key_exists( $key,  $compare ) || ( array_key_exists( $key,  $compare ) && $compare[$key] !== $val ) ) {

          $return = false;

        }

      } elseif( ! array_key_exists( $key, $compare ) || ! self::deepCompare( $val, $compare[$key] ) ) {

        $return = false;

      }

    }

    return $return;

  }

  /**
   * stringifyHtmlAttributes
   *
   * @access public
   *
   * @param array $available Array of available keys and values that can be turned into attributes.  This data set can be more than needed, and reduced by specific attribute keys provided in the $possible array.
   * @param array (optional) $possible Array of which data keys you want to keep as attributes in the stringification, if the $available data set is too greedy.
   *
   * @return string html-formatted string of key/value pairs after reducing the available keys/values against the requested possible keys as attributes.
   * @see self::htmlAttribute for return format.
   */
  public static function stringifyHtmlAttributes( array $available, array $possible = [] ): string {

    $atts = "";
    $array = [];

    if( ! empty( $possible ) ) {

      $possible = array_fill_keys( $possible, "" );
      $merged   = array_merge( $possible, $available );
      $array = array_filter( array_intersect_key( $merged, $possible ) );

    } else {

      $array = array_filter( $available );

    }

    return self::arrayToHtmlAttributes( $array );

  }

  /**
   * Array to HTML Attributes
   * Provide an array of key/value pairs to be converted into a full string of space-separated HTML-ready attributes.
   * 
   * @param $atts (array) the keys and values to be transformed into an HTML attribute string
   * 
   * @return (string) a space-separated HTML attribute string
   */
  public static function arrayToHtmlAttributes( array $atts ): string {

    $keys = array_keys( $atts );
    $vals = array_values( $atts );
    $htmlAttributes = array_map(
      "self::htmlAttribute",
      array_filter( $keys, 'trim' ),
      array_filter( $vals, 'trim' )
    );

    return implode( " ", $htmlAttributes );

  }

  /**
   * HTML Attribute
   * Formats a key/value pair as a string there $key="$value" for use as an inline HTML attribute.
   * 
   * @param $key (string) the HTML attribute's name
   * @param $value (string) the HTML attribute's value
   * 
   * @return string
   */
  public static function htmlAttribute( string $key, string $value ): string {

    return "{$key}=\"{$value}\"";

  }

  /**
   * Linkify
   * Receive an array of data and a map that is used to match data values to attributes
   * which are compiled into an array of strings containing html anchors.
   *
   * @param $map (array)
   * @param $data (array|object)
   *
   * @return (array) array of html anchor tag strings w/mapped atttributes
   */
  public static function linkify( array $map, $data ): array {

    $dataArr  = json_decode( json_encode( $data ), true );
    $links    = [];
    $texts    = array_column( $data, $map['text'] );
    unset($map['text']);

    foreach( $dataArr as $i => $link ) {

      $atts = [];

      foreach( $map as $att => $val ) {

        if( "href" === $att ) {
          
          if( $data[$i] instanceof WP_Term )
            $atts[$att] = get_term_link( $data[$i]->term_id, $data[$i]->taxonomy );
          elseif( $data[$i] instanceof WP_Post )
            $atts[$att] = get_the_permalink( $data[$i]->ID );

        } else {

          $atts[$att] = $link[$val];
          
        }
      }
      
      $htmlAtts = self::arrayToHtmlAttributes( $atts );
      $links[] = sprintf( "<a %s>%s</a>", $htmlAtts, $texts[$i] );

    }

    return $links;

  }

  /**
   * updateElementAttr
   *
   * @access public
   * @see https://www.php.net/manual/en/class.domelement.php
   *
   * @param DOMElement $el An reference DOMElement object that can be directly manipulated in this method.
   * @param string $attr The attribute name that needs updating with values. If the attribute does not exist it will be added.
   * @param array $values Values that will be added to any existing values for that attribute.
   * @param bool $replace (default: false) Flag to determine if the attribute's existing values should be replaced by new ones.
   *
   * @return string An html element string with updated classes.
   */
  public static function updateElementAttr( DOMElement &$el, string $attr, array $values, $replace = false ) {

    if( false === $replace || $el->hasAttribute( $attr ) ) {

      $values   = is_array( $values ) ? $values : [ $values ];
      $attrs    = explode( " ", $el->getAttribute( $attr ) );
      $attrs    = array_map( "trim", array_filter( $attrs ) );
      $newVals  = array_merge( $attrs, $values );

      sort( $newVals );

      $el->setAttribute( $attr, implode( " ", $newVals ) );

    } else {

      $el->setAttribute( $attr, implode( " ", $values ) );

    }

  }

  /**
   * Update Meta Property
   * 
   * @param $args (array) must contain keys 'key', field' and 'value' to determine what to update.
   *  May optionally contain a 'post' key to specify a partifular post on which the meta should be updated. Defaults to current post.
   *  'field' should be the slug/group for the meta field -- identified by the ID of the metabox that the input is found in, usually preceded with an underscore "_"
   *  'key' should refer to the specific input value's name
   *  'value' should be the new value that you want to set on that key
   *  Visual format:  $field[$key] = $value
   * 
   * @return void
   */
  public static function updateMetaProperty( array $args ) {

    if( ! isset( $args['post'] ) )
      $args['post'] = get_the_ID();

    $field = get_post_meta( $args['post'], $args['field'], true );
    $field[$args['key']] = $args['value'];

    update_post_meta( $args['post'], $args['field'], $field );

  }

  /**
   * Check Meta Property
   * Method to get a serialized post meta values and return a specific key's value.
   * 
   * @param $args (array) arguments to use
   *
   * @example $args = [
   *   'field'     => "_seo_props",
   *   'key'       => "sitemap_exclusion",
   *   'value'     => "on"
   * ];
   *
   * @return (bool) whether the meta value matches the one provided in $args['value']
   */
  public static function checkMetaProperty( array $args ): bool {

    if( ! isset( $args['post'] ) )
      $args['post'] = get_the_ID();

    $value = get_post_meta( $args['post'], $args['field'], true );

    if( ! isset( $value[$args['key']] ) ) {

      $check = false;

    } elseif( is_array( $args['value'] ) ) {

      $check = in_array( $value[$args['key']], $args['value'] );

    } elseif( $value[$args['key']] === $args['value'] ) {

      $check = true;

    } else {

      $check = false;

    }

    return $check;

  }

  public static function camelCase( $string, $first = false ) {

    $string = preg_replace( "/([-_\s]+)/", "^", $string );
    $words = explode( "^", $string );

    if( $first ) {

      $camelCase = implode( "", array_map( "ucfirst", $words ) );

    } else {

      $firstWord = strtolower( array_shift( $words ) );
      $words = array_merge( [ $firstWord ], array_map( "ucfirst", $words ) );

      $camelCase = implode( "", $words );

    }

    return $camelCase;

  }

  public static function parseNumberVals( &$val, $key, $changeKeys ) {

    if( in_array( $key, $changeKeys ) )
      $val = floatval($val);

  }

  public static function falseTerms() {

    return [ "false", "off", "disable", "hide", "no", "never", "nope", "0", "disabled" ];

  }

  public static function trueTerms() {

    return [ "true", "show", "yes", "always", "yup", "1", "on", "enabled" ];

  }

  public static function getPostMetaValue( string $group, $key = "", $postID = null ) {

    if( null === $postID )
      $postID = get_the_ID();

    if( 0 !== strpos( $group, "_" ) )
      $group = "_" . $group;

    $meta = get_post_meta( $postID, $group, true );

    if( ! empty( $key ) && isset( $meta[$key] ) )
      $value = $meta[$key];
    else
      $value = $meta;

    return $value;

  }

  public static function getOption( $group, $name = null ) {

    $options = get_option( $group );

    if( null !== $name )
      $value = isset( $options[$name] ) ? $options[$name] : "";
    else
      $value = $options;
    
    return $value;

  }

  public static function getPhone( $clickToCall = false ) {

    $info = get_option( 'business_info' );

    return $clickToCall ? "tel:" . preg_replace("/([^+0-9]+)/", "", $info['phone']) : $info['phone'];

  }

  public static function getEmail( $mailTo = false ) {

    $info = get_option( 'business_info' );

    return false !== $mailTo ? "mailto:" . $info['email'] : $info['email'];

  }

  public static function getAuthor() {

    $author = get_the_author();
    $owner  = Utility::getOption( 'business_info', "owner");

    if( $owner && ! empty( $owner ) )
      $author = get_user_by( "ID", $owner )->data->display_name;
    
    return $author;

  }

  public static function getMetaDescription() {

    global $post, $wp_query;

    $description  = get_option( "blogdescription" );
    $excerpt      = get_the_excerpt();

    if( is_object( $post ) && $post instanceof WP_Post && ! empty( $excerpt ) ) {

      $description = $excerpt;

    } elseif( $wp_query->is_tax || $wp_query->is_tag || $wp_query->is_category ) {

      $props        = self::getArchivePageProperties( $wp_query->queried_object );
      $description  = $props['description'];

    }

    return apply_filters( 'meta_description', str_replace( '"', "'", strip_tags( $description ) ) );

  }

  public static function getFacebookUrl( $handle = false ) {

    $fbbp = get_option( 'fbbp' );

    return $handle ? $fbbp['page_url'] : "https://facebook.com/" . $fbbp['page_url'];

  }

  public static function getInstagramUrl( $handle = false ) {

    $ig = get_option( 'instagram' );

    return $handle ? "@" . $ig['ig_handle'] : "https://instagram.com/" . $ig['ig_handle'];
  }

  public static function getMapsUrl() {

    $info     = get_option( 'business_info' );
    $fallback = isset( $info['latitude'] ) && isset( $info['longitude'] ) ? sprintf( "https://www.google.com/maps/@%s,%s,17z", $info['latitude'], $info['longitude'] ) : sprintf( "https://maps.google.com/?q=%s", str_replace(" ", "+", bloginfo( "name" ) ) );

    return isset( $info['map_url'] ) ? $info['map_url'] : $fallback;

  }

  public static function getGoogleProperty( $prop ) {

    $properties = get_option( 'google_props' );

    return isset( $properties[$prop] ) ? $properties[$prop] : "";

  }

  public static function getOtherProperty( $prop ) {

    $properties = get_option( 'other_props' );

    return isset( $properties[$prop] ) ? $properties[$prop] : "";

  }

  public static function getAddress( $part = null ) {

    $info   = get_option( 'business_info' );
    $output = false;

    if( $info && ! empty( $info ) ) {

      $output  = [];
      switch( $part ) {

        case "street":
        case "street_address":

          $output['street'] = $info['street_address'];

          break;

        case "city":

          $output['city'] = $info['city'];

          break;

        case "state":
        case "province":

          $output['state'] = $info['state'];

          break;

        case "postal":
        case "postal_code":
        case "zip":
        case "zip_code":

          $output['zip'] = $info['zip'];

          break;

        case "country":

          $output['country'] = $info['country'];

          break;

        default:

          $output = [
            'street'  => $info['street_address'],
            'city'    => $info['city'],
            'state'   => $info['state'],
            'zip'     => $info['zip'],
            'country' => $info['country']
          ];
          break;
      }

    }

    return $output;

  }

  public static function convertState( $state, $type = "long" ) {

    $states = [
      'AL' => 'ALABAMA',
      'AK' => 'ALASKA',
      'AS' => 'AMERICAN SAMOA',
      'AZ' => 'ARIZONA',
      'AR' => 'ARKANSAS',
      'CA' => 'CALIFORNIA',
      'CO' => 'COLORADO',
      'CT' => 'CONNECTICUT',
      'DE' => 'DELAWARE',
      'DC' => 'DISTRICT OF COLUMBIA',
      'FM' => 'FEDERATED STATES OF MICRONESIA',
      'FL' => 'FLORIDA',
      'GA' => 'GEORGIA',
      'GU' => 'GUAM GU',
      'HI' => 'HAWAII',
      'ID' => 'IDAHO',
      'IL' => 'ILLINOIS',
      'IN' => 'INDIANA',
      'IA' => 'IOWA',
      'KS' => 'KANSAS',
      'KY' => 'KENTUCKY',
      'LA' => 'LOUISIANA',
      'ME' => 'MAINE',
      'MH' => 'MARSHALL ISLANDS',
      'MD' => 'MARYLAND',
      'MA' => 'MASSACHUSETTS',
      'MI' => 'MICHIGAN',
      'MN' => 'MINNESOTA',
      'MS' => 'MISSISSIPPI',
      'MO' => 'MISSOURI',
      'MT' => 'MONTANA',
      'NE' => 'NEBRASKA',
      'NV' => 'NEVADA',
      'NH' => 'NEW HAMPSHIRE',
      'NJ' => 'NEW JERSEY',
      'NM' => 'NEW MEXICO',
      'NY' => 'NEW YORK',
      'NC' => 'NORTH CAROLINA',
      'ND' => 'NORTH DAKOTA',
      'MP' => 'NORTHERN MARIANA ISLANDS',
      'OH' => 'OHIO',
      'OK' => 'OKLAHOMA',
      'OR' => 'OREGON',
      'PW' => 'PALAU',
      'PA' => 'PENNSYLVANIA',
      'PR' => 'PUERTO RICO',
      'RI' => 'RHODE ISLAND',
      'SC' => 'SOUTH CAROLINA',
      'SD' => 'SOUTH DAKOTA',
      'TN' => 'TENNESSEE',
      'TX' => 'TEXAS',
      'UT' => 'UTAH',
      'VT' => 'VERMONT',
      'VI' => 'VIRGIN ISLANDS',
      'VA' => 'VIRGINIA',
      'WA' => 'WASHINGTON',
      'WV' => 'WEST VIRGINIA',
      'WI' => 'WISCONSIN',
      'WY' => 'WYOMING',
      'AE' => 'ARMED FORCES AFRICA \ CANADA \ EUROPE \ MIDDLE EAST',
      'AA' => 'ARMED FORCES AMERICA (EXCEPT CANADA)',
      'AP' => 'ARMED FORCES PACIFIC'
    ];

    if( "short" === $type )
      $stateOut = in_array( strtoupper( $state ), $states ) ? array_search( strtoupper( $state ), $states ) : $state;
    else
      $stateOut = key_exists( strtoupper( $state ), $states ) ? ucwords( strtolower( $states[$state] ) ) : $state;

    return $stateOut;

  }

  public static function getAttachmentAltText( int $id ): string {

    $text = "";
    $alt  = get_post_meta( $id, '_wp_attachment_image_alt', true );

    if( $alt )
      $text = $alt;

    return $text;

  }

  public static function getFeaturedImgAltText( $postID = null ): string {

    if( null === $postID )
      $postID = get_the_ID();

    $alt  = "";
    $id   = get_post_thumbnail_id( $postID );

    if( $id )
      $alt  = self::getAttachmentAltText( $id );

    return $alt;

  }

  public static function getDynamicThumbnail( WP_Post $post ): string {

    $img    = '';
    $output = preg_match('/<img[^>]+?src=[\'"]([^\'"]+)[\'"]/im', get_the_content( $post->ID ), $matches);

    if( $matches && isset($matches[1] ) )
      $img = $matches[1];

    return apply_filters( 'dynamic_thumbnail', $img, $post );

  }

  /**
   * SEO HELPERS
   */

  public static function getOgTitle( $postID = null ) {

    $title = get_the_title();

    if( is_single() || is_page() ) {

      if( null === $postID )
        $postID = get_the_ID();

      $ogTitle  = self::getPostMetaValue( "seo_props", "og_title", $postID );
      
      if( ! empty( $ogTitle ) )
        $title = $ogTitle;

    }

    return $title;

  }

  public static function getMetaTitle( $postID = null ) {
    
    $title = get_the_title();

    if( is_single() || is_page() ) {
      
      if ( null === $postID )

      $metaTitle = self::getPostMetaValue( "seo_props", "meta_title", $postID );

      if( ! empty( $metaTitle ) )
        $title = $metaTitle;
    }

    return apply_filters( 'meta_title', $title );
  }

  public static function getOg() {

    global $post;

    $og = [
      'title'         => self::getOgTitle(),
      'site_name'     => get_option( 'blogname' ),
      'url'           => get_option( 'siteurl' ),
      'type'          => 'website',
      'image'         => self::getOption( 'business_info', 'logo_url' )
    ];

    if( is_object( $post ) && $post instanceof WP_Post && ! is_front_page() ) {
      
      global $wp_query;

      if( has_post_thumbnail( $post->ID ) )
        $og['image'] = get_the_post_thumbnail_url( $post->ID, 'full' );
      else
        $og['image'] = self::getDynamicThumbnail( $post );

      if( $wp_query->is_tax || $wp_query->is_tag || $wp_query->is_category ) {

        $props              = self::getArchivePageProperties( $wp_query->queried_object );
        $og['title']        = $props['title'] . " | " . get_option( 'blogname' );
        $og['url']          = get_term_link( $wp_query->queried_object );

      } else {

        $og['url']          = get_the_permalink( $post->ID );

        if ( "post" === $post->post_type )
            $og['type'] = "article";

      }

    }

    return apply_filters( 'open_graph', $og );

  }

  public static function getTwitterCard() {

    $card = [
      'card'    => "summary",
      'site'    => get_option( "siteurl" ),
      'author'  => self::getAuthor()
    ];

    return apply_filters( 'twitter_card_meta', $card );

  }

  public static function theOg() {

    $meta = "";

    foreach( self::getTwitterCard() as $prop => $val )
      $meta .= sprintf( "\t<meta name=\"twitter:%s\" content=\"%s\" />\n", $prop, $val );

    foreach( self::getOg() as $prop => $val ) 
      $meta .= sprintf( "\t<meta property=\"og:%s\" name=\"%s\" content=\"%s\" />\n", $prop, $prop, $val );

    return apply_filters( 'open_graph_meta', $meta );

  }

  public static function getSchema() {

    $info   = get_option( 'business_info' );
    $facebook = get_option( 'fbbp' );
    $twitter  = get_option( 'twitter' );
    $instagram  = get_option( 'instagram' );
    $linkedin  =  get_option( 'linkedin' );
    $og     = false;
    $hours  = false;
    $schema = [];

    if( $info && ! empty( $info ) ) {

      $og = self::getOg();    
      $hours = array_map( 'trim', explode( ",", $info['hours'] ) );

      $schema = [
        [
          "@context"      => "https://schema.org",
          "@type"         => "Organization",
          "name"          => $info['organization'],
          "url"           => get_site_url(),
          "logo"          => $info['logo_url'],
          "sameAs"        => self::getSocialSchema(),
          "address"       => [
            "@context"        =>  "https://schema.org",
            "@type"           =>  "PostalAddress",
            "streetAddress"   =>  $info['street_address'],
            "addressLocality" =>  $info['city'],
            "addressRegion"   =>  $info['state'],
            "postalCode"      =>  $info['zip'],
            "addressCountry"  =>  $info['country']
          ],
          "contactPoint"  => [
            "@context"        =>  "https://schema.org",
            "@type"           =>  "ContactPoint",
            "contactType"     =>  "General",
            "email"           =>  $info['email'],
            "telephone"       =>  $info['phone']
          ]
        ],
        [
          "@context"  => "https://schema.org",
          "@type"     => "LocalBusiness",
          "address"   => [
            "@type"             => "PostalAddress",
            "addressLocality"   => $info['city'],
            "addressRegion"     => self::convertState( $info['state'] ),
            "postalCode"        => $info['zip'],
            "streetAddress"     => $info['street_address']
          ],
          "geo" => [
            "@type"       => "GeoCoordinates",
            "latitude"    => $info['latitude'],
            "longitude"   => $info['longitude']
          ],
          "name"          => $og['site_name'],
          "logo"          => $info['logo_url'],
          "image"         => $og['image'],
          "description"   => $info['business_description'],
          "url"           => $og['url'],
          "email"         => $info['email'],
          "telephone"     => $info['phone'],
          "priceRange"    => $info['price_range'],
          "openingHours"  => $hours,
          "areaServed"    => [
            "@type"   => "State",
            "name"    => self::convertState( $info['state'] )
          ]
        ]
      ];

    }

    return apply_filters( 'site_schema', $schema, $info, $og, $hours );

  }

  public static function getSocialSchema() {

    $facebook = get_option( 'fbbp' );
    $twitter  = get_option( 'twitter' );
    $instagram  = get_option( 'instagram' );
    $linkedin  =  get_option( 'linkedin' );

    $social = [];

    if ( isset( $facebook['page_url'] ) && !empty( $facebook['page_url'] ) ) 
      $social[] .= 'https://facebook.com/' . $facebook['page_url'];

    if ( isset( $twitter['profile_url'] ) && !empty( $twitter['profile_url'] ) ) 
      $social[] .= 'https://twitter.com/' . $twitter['profile_url'];

    if ( isset( $instagram['ig_url'] ) && !empty( $instagram['ig_url'] ) ) 
      $social[] .= 'https://instagram.com/' . $instagram['ig_url'];

    if ( isset( $linkedin['linkedin_url'] ) && !empty( $linkedin['linkedin_url'] ) ) 
      $social[] .= 'https://linkedin.com/company/' . $linkedin['linkedin_url'];

    return $social;

  } 



  /**
   * Get Schema Object
   * Method to retrieve a standard array containing the provided schema @type
   * and @context.
   *
   * @param $type (string) the given type of the schema object
   * @param $properties (array) the additional properties to be included with the returned object
   * @param $context (bool) flag for weather or not to include the @context property; default: true
   *
   * @return (array) Schema object data
   *
   * @link https://schema.org/$type
   */
  public static function getSchemaObject( $type, $properties = [], $context = true ) {

    $object = [
      '@type'     => $type
    ];

    if( $context )
      $object['@context'] = "https://schema.org";

    if( ! empty( $properties ) && is_array( $properties ) )
      $object = array_merge( $object, $properties );

    return apply_filters( "schema_object_{$type}", $object, $properties );

  }

   /**
   * isMultiLocationSite
   *
   * @return true if multi location checkbox is checked in Business Settings page, otherwise false
   */
   public static function isMultiLocationSite() {
    return ( in_array( Self::getOption( 'business_info', 'multi_location' ), Self::trueTerms() ) ) ? true : false;
  }

  public static function getHeroBgImageStyles( $post_id, $classes = [ "hero", "entry-header" ] ) {

    $sizeData     = wp_get_additional_image_sizes();
    $orderedSizes = array_combine( array_column( $sizeData, "width"), array_keys( $sizeData ) );
    ksort( $orderedSizes );
    $sizes        = array_keys( $orderedSizes );
    $handles      = array_values( $orderedSizes );

    if( ! is_array( $classes ) )
      $classes = [ $classes ];

    $class  = implode( ".", $classes );
    $style  = "<style>\n";
    $style .= sprintf( ".%s {background-image:url(%s)}\n", $class, get_the_post_thumbnail_url( $post_id, $handles[0] ) );

    foreach( $sizes as $i => $size ) {

      if( $i === 0 )
        continue;

      $style .= sprintf( "@media screen and (min-width:%spx){ .%s {background-image:url(%s)}}\n", ($sizes[$i - 1] + 1), $class, get_the_post_thumbnail_url( $post_id, $handles[$i] ) );

    }

    $style .= "</style>";

    return $style;
    
  }

  public static function getPostTypesByTaxonomy( string $tax, string $label = null ): array {

    global $wp_taxonomies;
    $objects = [];

    $objects = isset( $wp_taxonomies[$tax] ) ? $wp_taxonomies[$tax]->object_type : [];

    if( null !== $label ) {

      $objects = array_map( function ( $post_type ) use ( $label ) {

        $postObject = get_post_type_object( $post_type );
        $postLabel = $postObject->labels->$label;

        return $postLabel;

      }, $objects );

    }

    return $objects;

  }

  /**
   * Get Archive Page Properties
   * Takes a WP object and returns an array of properties to use in the archive page.
   *
   * @param $obj WP_Term - the queried object from the archive page.
   *
   * @return array
   */
  public static function getArchivePageProperties( WP_Term $obj ): array {

    $properties = [
      'title'       => $obj->name,
      'description' => $obj->description,
      'sidebar'     => apply_filters(
        'sidebar',
        self::getTaxonomySidebar( $obj->taxonomy ),
        [
          'location'  => "archive",
          'object'    => $obj
        ]
      ),
      'term_id'     => $obj->term_id
    ];

    return apply_filters( 'archive_page_properties', $properties, $obj );

  }

  public static function getTaxonomySidebar( $taxonomy ) {

    $sidebars = [
      $taxonomy,
      'blog'
    ];
    $sidebars = apply_filters( 'taxonomy_sidebars', $sidebars, $taxonomy );
    $found = false;

    foreach( $sidebars as $s ) {

      if( is_active_sidebar($s) ) {

        $found = $s;
        break;

      }

    }

    return $found;

  }

  /**
   * Get Post Select Options
   * Create a select option set that can be used by /template-parts/Bootstrap/field-select.php
   * from a provided single or array of post objects.
   *
   * @param $object (string|array) List of post types to be included as valid options
   * @param $args (array) Arguments to be used in handling outcomes.
   *
   * @return (array) array of post data using $post->value_key => $post->label_key structure
   */
  public static function getPostSelectOptions( $objects = [ "post", "page" ], array $args = [] ): array {

    if( ! is_array( $objects ) )
      $objects = [ $objects ];
    if( ! isset( $args['value_key'] ) )
      $args['value_key'] = "ID";
    if( ! isset( $args['label_key'] ) )
      $args['label_key'] = "post_title";


    if( count( $objects ) > 1 ) {

      $optionObjects  = $objects;
      $last           = ucfirst( array_pop( $optionObjects ) );
      $stringList     = implode( ", ", array_map( "ucfirst", $optionObjects ) );
      $stringList    .= " or " . $last;

    } else {
      $stringList = ucfirst( $objects[0] );
    }

    $options = [
      '' => "Choose a " . $stringList
    ];

    $queryArgs = [
      'post_type' => $objects,
      'status'    => "publish",
      'orderby'   => "modified",
      'order'     => "DESC"
    ];

    if( isset( $args['query_args'] ) )
      $queryArgs = array_merge( $queryArgs, $args['query_args'] );

    $posts = new WP_Query( $queryArgs );

    if( $posts->have_posts() ) {
      
      $vals     = array_column( $posts->posts, $args['value_key'] );
      $labels   = array_column( $posts->posts, $args['label_key'] );
      $options  = $options + array_combine( $vals, $labels );

    }

    return $options;

  }
  
  /**
   * Get Term Select Options
   * Create a select option set that can be used by /template-parts/Bootstrap/field-select.php
   * from a provided single or array of term objects.
   *
   * @param $object (string|array) List of post types to be included as valid options
   * @param $args (array) Arguments to be used in handling outcomes.
   *
   * @return (array) array of post data using $post->value_key => $post->label_key structure
   */
  public static function getTermSelectOptions( $objects = [ "post_tag", "category" ], array $args = [] ): array {

    if( ! is_array( $objects ) )
      $objects = [ $objects ];
    if( ! isset( $args['value_key'] ) )
      $args['value_key'] = "term_id";
    if( ! isset( $args['label_key'] ) )
      $args['label_key'] = "name";

    

    if( count( $objects ) > 1 ) {

      $optionObjects  = $objects;
      $last           = ucfirst( array_pop( $optionObjects ) );
      $stringList     = implode( ", ", array_map( "ucfirst", $optionObjects ) );
      $stringList    .= " or " . $last;

    } else {
      $stringList = ucfirst( $objects[0] );
    }

    $options = [
      '' => "Choose a " . $stringList
    ];

    $queryArgs = [
      'taxonomy'    => $objects,
      'hide_empty'  => false,
      'orderby'     => "modified",
      'order'       => "DESC"
    ];

    if( isset( $args['query_args'] ) )
      $queryArgs = array_merge( $queryArgs, $args['query_args'] );

    $terms = get_terms( $queryArgs );

    if( false !== $terms ) {

      $vals     = array_column( $terms, $args['value_key'] );
      $labels   = array_column( $terms, $args['label_key'] );
      $options  = $options + array_combine( $vals, $labels );

    }

    return $options;

  }

  /**
   * Get Author Select Options
   * Create a select option set of available authors on the site.
   *
   * @return (array) array of authors using $wp_user->ID => $wp_user->display_name
   */
  public static function getAuthorSelectOptions( $args = [] ) {

    $defaults = [
      'role__in'  => [
        "administrator",
        "editor",
        "author"
      ]
    ];
    $args     = wp_parse_args( $args, $defaults );
    $users    = new WP_User_Query( $args );
    $options  = [
      ''    => "Choose user..."
    ];

    if( ! empty( $users->get_results() ) ) {

      foreach( $users->get_results() as $user )
        $options[$user->ID] = $user->data->display_name;

    }

    return $options;

  }

  public static function sortBySubMetaValue( $objectArray, $metaKey, $metaSubKey ) {

    $sortedArray = [];

    if( 0 !== strpos( $metaKey, "_" ) )
      $metaKey = "_" . $metaKey;
    
    foreach( $objectArray as $object ) {

      if( $object instanceof WP_Post ) {

        $meta = get_post_meta( $object->ID, $metaKey, true );
        if( $meta && ! empty( $meta ) ) {

          $sortKey = $meta[$metaKey];
          $sortedArray[$sortKey][] = $object;

        } else {

          $sortedArray[][] = $object;

        }

      } elseif( $object instanceof WP_Term ) {

        $meta = get_term_meta( $object->term_id, $metaKey, true );

        if( $meta && ! empty( $meta ) ) {

          $sortKey = $meta[$metaSubKey];
          $sortedArray[$sortKey][] = $object;

        } else {

          $sortedArray[][] = $object;

        }

      }

    }

    ksort( $sortedArray );
    
    return $sortedArray;

  }

  public static function getBreadCrumbs() {

    global $wp_query;

    global $post;

    $html = '<ul class="breadcrumbs">';

    $parents = [];
    $parentLinks = [];

    if ( $wp_query->is_category  && $wp_query->queried_object->category_parent > 0 ) {
        $parents = get_category_parents( $wp_query->queried_object->term_id );
        $parents = explode( '/', $parents );
        unset( $parents[ array_key_last( $parents ) ] );
        unset( $parents[ array_key_last( $parents ) ] );

        $html .= ( ! is_front_page() ) ? '<li class="breadcrumb"><a href="' . get_home_url() . '">' . get_the_title( get_option('page_on_front') ) . '</a></li><span> > </span>' : '';

        foreach ( $parents as $key => $val ) {
          $parentLinks[$key] = get_category_link( get_cat_ID( $val ) );
          $html .= '<li class="breadcrumb"><a href="' . $parentLinks[ $key ] . '">' . $val . '</a></li>';
          
          if ( ( count( $parents ) - 1 ) != $key )
            $html .= '<span> > </span>';
        }
    } else {

      $ids = get_ancestors( $post->ID, $post->post_type );

      for ( $i = count($ids) -1 ; $i >= 0; $i-- ) {
        $parents[] .= $ids[$i];
      }

      $html .= ( ! is_front_page() ) ? '<li class="breadcrumb"><a href="' . get_home_url() . '">' . get_the_title( get_option('page_on_front') ) . '</a></li>' : '';

      if ( has_post_parent( $post->ID ) )
       $html .= '<span> > </span>';

      foreach( $parents as $key => $val ) {
        $parentLinks[$key] = get_the_permalink( (int) $val );
        $parentTitle[$key] = get_the_title( (int) $val );
        $html .= '<li class="breadcrumb"><a href="' . $parentLinks[ $key ] . '">' . $parentTitle[$key] . '</a></li>';
        
        if ( ( count( $parents ) -1 ) != $key )
          $html .= '<span> > </span>';
        
      }
    }

    $html .= '</ul>';

    return $html;
  }

}

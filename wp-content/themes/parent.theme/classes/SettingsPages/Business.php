<?php

namespace Theme\Parent\SettingsPage;

use Theme\Parent\{ SettingsPage as Page, Utility };

class Business extends Page {
  
  protected $_args = [
    'id'          => 'business',
    'menu_title'  => 'Business',
    'page_title'  => 'General Business Settings',
    'menu_type'   => 'Menu',
    'icon'        => 'dashicons-store',
    'position'    => 3,
    'sections'    => [
      'business_info'   => [
        'title'         => "Business Info",
        'description'   => "General information about your business that will be used throughout the website.",
        'fields'    => [
          [
            'id'            => "organization",
            'type'          => "text",
            'label'         => "Organization Name",
            'desc'          =>  "<span class='ml-2 d-block'>The parent organization name or the name of the company. This will be used in the Organization Schema.</span>"
          ],
          [
            'id'            => "owner",
            'type'          => "select",
            'label'         => "Owner / Primary Author",
            'options_cb'    => "\Theme\Parent\Utility::getAuthorSelectOptions"
          ],
          [
            'id'            => "override_author",
            'type'          => "checkbox",
            'label'         => "Override Author",
            'desc'          => "<span class='ml-2 d-block'>When enabled, sets the <b>Owner / Primary Author</b> above as the author for newly created content.</span>"
          ],
          'logo_url' => [
            'id'            => "logo_url",
            'label'         => "Full Logo URL",
            'type'          => "text",
            'placeholder'   => "https://..."
          ],
          'compact_logo_url' => [
            'id'            => "compact_logo_url",
            'label'         => "Compact Logo URL",
            'type'          => "text",
            'placeholder'   => "https://..."
          ],
          'multi_location' => [
			      'id'			=> "multi_location",
			      'label'		=> "Multiple Locations",
			      'type'		=> "checkbox",
			      'desc'		=> "<span class='ml-2 d-block'>When enabled, the site-wide location schema will not be used. Instead, the location schema for each Location post type will be used on their respective page and only the Organization schema will be used site-wide.</span>"
		      ],
          [
            'id'        => "map_url",
            'label'     => "Map URL",
            'type'      => "text",
            'placeholder'   => "https://..."
          ],
          [
            'id'        => "street_address",
            'label'     => "Street Address",
            'type'      => "text"
          ],
          [
            'id'        => "city",
            'label'     => "City",
            'type'      => "text"
          ],
          [
            'id'        => "state",
            'label'     => "State/Region",
            'type'      => 'text',
            'size'      => "medium",
            "desc"      => "For U.S. states, use the two-letter abbreviation."
          ],
          [
            'id'        => "zip",
            'label'     => "Postal Code",
            'type'      => "text",
            'size'      => "medium"
          ],
          [
            'id'        => "country",
            'label'     => "Country",
            'type'      => "text",
            'size'      => "medium"
          ],
          [
            'id'        => "latitude",
            'label'     => "Geo Latitude",
            'type'      => "text",
            'size'      => "medium",
            'desc'      => 'Latitude coordinate can be found in the URL after locating your business on <a href="https://maps.google.com" target="_blank">Google Maps</a>.'
          ],
          [
            'id'        => "longitude",
            'label'     => "Geo Longitude",
            'type'      => "text",
            'size'      => "medium",
            'desc'      => 'Longitude coordinate can be found in the URL after locating your business on <a href="https://maps.google.com" target="_blank">Google Maps</a>.'
          ],
          [
            'id'            => "hours",
            'label'         => "Business Hours",
            'type'          => "text",
            'placeholder'   => "i.e. Mo-Fr 09:00-17:00, Sa 09:00-15:00",
            'desc'          => 'See the <a href="https://schema.org/openingHours" target="_blank">Schema.org openingHours documentation</a> for formatting; comma-separate the values that would be used in the array.'
          ],
          [
            'id'            => "price_range",
            'label'         => "Price Range",
            'type'          => "text",
            'size'          => "small",
            'placeholder'   => '$$',
            'desc'          => 'See the <a href="https://schema.org/priceRange" target="_blank">Schema.org priceRange documentation</a>.'
          ],
          [
            'id'            => "phone",
            'label'         => "Phone Number",
            'type'          => "text",
            'size'          => "medium",
            'placeholder'   => "(555) 555-5555",
            'desc'          => "Format the way you want it to read throughout the website.  This will also be used as your click-to-call number."
          ],
          [
            'id'        => "email",
            'label'     => "Contact Email",
            'type'      => "text",
            'size'      => "medium",
            'desc'      => "Email to use for contact schema, forms, visitors, and system notifications."
          ],
          [
            'id'        => "business_description",
            'type'      => "input",
            'label'     => "Business Description",
            'desc'      => "A short description of your company. This will be used in the description field for the local business schema.",
            'render'    => '<textarea id="%s" name="%s" type="text" class="form-control">%s</textarea>'
          ]
        ]
      ],
      'google_props' => [
        'title'         => "Google Properties",
        'description'   => "Load <a target=\"_new\" href=\"https://analytics.google.com/analytics/web/\">Google Analytics</a>, set a Google Maps API Key for custom map embeds, and verify this site with <a target=\"_new\" href=\"https://search.google.com/search-console/\">Google Search Console</a>.",
        'fields'    => [
          [
            'id'            => "ga_id",
            'label'         => "Google Analytics ID",
            'type'          => "text",
            'size'          => "medium",
            'placeholder'   => "i.e. UA-123456789-00",
            'desc'          => "The ID of the Google Analytics property that should be loaded on the site."
          ],
          [
            'id'            => "gtm_id",
            'label'         => "Google Tag Manager ID(s)",
            'type'          => "text",
            'size'          => "medium",
            'placeholder'   => "i.e. GTM-ABCD123",
            'desc'          => "The ID(s) of the Google Tag Manager containers that should be loaded on the site."
          ],
          [
            'id'        => "maps_api_key",
            'label'     => "Maps API Key",
            'type'      => "text",
            'desc'      => "API Key that is properly configured to enable the SDK with Google Cloud Platform."
          ],
          [
            'id'        => "gsc",
            'label'     => "Google Site Verification",
            'type'      => "text",
            'desc'      => 'The property ID used to verify against <a href="https://search.google.com/search-console" target="_blank">Google Search Console</a> for sitemap indexing and other technical SEO details.'
          ]
        ]
      ],
      'other_props' => [
        'title'       => "Other Properties",
        'description' => "Use this section for various Social Network integrations and site verification methods.",
        'fields'      => [
          [
            'id'        => "fbdv",
            'label'     => "Facebook Site Verification",
            'type'      => "text",
            'desc'      => 'The value that appears in the "content" attribute of your meta tag.',
          ],
          [
            'id'        => "ahrefv",
            'label'     => "Ahref's Site Verification",
            'type'      => "text",
            'desc'      => 'The value that appears in the "content" attribute of your meta tag.',
          ]
        ]
      ]
    ]
  ];

  function __construct() {

    $this->_args['sections']['business_info']['fields']['logo_url']['default']          = get_stylesheet_directory_uri() . "/assets/logo.png";
    $this->_args['sections']['business_info']['fields']['compact_logo_url']['default']  = get_stylesheet_directory_uri() . "/assets/logo-compact.png";

    parent::__construct();

  }

  protected function _adjust() {

    add_action( 'admin_init', [ $this, "updateLabel" ] );

  }

  public function updateLabel() {

    global $submenu;

    if( is_array( $submenu ) && array_key_exists( $this->_args['id'], $submenu ) )
      $submenu[$this->_args['id']][0][0] = "General";

  }

}

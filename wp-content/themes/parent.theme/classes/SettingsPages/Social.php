<?php

namespace Theme\Parent\SettingsPage;

use Theme\Parent\{ SettingsPage as Page, Utility };

class Social extends Page {
  
  protected $_args = [
    'id'          => 'social',
    'menu_title'  => 'Social Media',
    'page_title'  => 'Social Media Settings',
    'menu_type'   => 'SubMenu',
    'anchor'      => 'business',
    'position'    => 10,
    'sections'    => [
      'fbbp'      => [
        'title'     => "Facebook",
        'fields'    => [
          [
            'id'        => 'page_url',
            'label'     => "Page URL",
            'prepend'   => "https://facebook.com/",
            'type'      => "group-text"
          ],
          [
            'id'        => 'fbbp_id',
            'desc'      => "The ID of the <a href='https://business.facebook.com/home/accounts' target='blank'>business page you manage.</a>",
            'label'     => "Business Page ID",
            'type'      => "group-text"
          ]
        ]
      ],
      'twitter'   =>  [
        'title'   =>  "Twitter",
        'fields'  =>  [
          [
            'id'        =>  'profile_url',
            'label'     =>  'Twitter Profile',
            'prepend'   =>  'https://twitter.com/',
            'type'      =>  'group-text'
          ]
        ]
      ],
      'linkedin'   =>  [
        'title'   =>  "Linkedin",
        'fields'  =>  [
          [
            'id'        =>  'linkedin_url',
            'label'     =>  'Linkedin Company Page',
            'prepend'   =>  'https://linkedin.com/company/',
            'type'      =>  'group-text'
          ]
        ]
      ],
      'instagram'   => [
        'title'     => "Instagram",
        'fields'    => [
          [
            'id'        => 'ig_url',
            'label'     => "Profile URL",
            'prepend'   => "https://instagram.com/",
            'size'      => "medium",
            'type'      => "group-text"
          ],
          [
            'id'        => 'ig_handle',
            'label'     => "Handle",
            'prepend'   => "@",
            'size'      => "medium",
            'type'      => "group-text"
          ]
        ]
      ]
    ]
  ];

}

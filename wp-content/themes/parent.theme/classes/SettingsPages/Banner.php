<?php

namespace Theme\Parent\SettingsPage;

use Theme\Parent\{ SettingsPage as Page, Utility };

class Banner extends Page {
  
  protected $_args = [
    'id'          => "banner",
    'menu_title'  => "Site Banner",
    'page_title'  => "Site Banner Settings",
    'menu_type'   => "SubMenu",
    'anchor'      => "business",
    'position'    => 11,
    'sections'    => [
      'banner_settings' => [
        'title'         => "Banner Settings",
        'description'   => "Add a banner to the header of the site and display it's contents in a modal.",
        'fields'        => [
          [
            'id'            => "show",
            'label'         => "Show Banner",
            'type'          => "checkbox",
            'desc'          => "Site banner will show when enabled."
          ],
          [
            'id'            => "message",
            'label'         => "Banner Message",
            'type'          => "text",
            'placeholder'   => "Write a message...",
            'desc'          => "The verbiage that will display in the site banner."
          ],
          [
            'id'            => "modal_toggle",
            'label'         => "Display Modal",
            'type'          => "checkbox",
            'desc'          => "When clicked, the banner will cause a modal to show, using the content below."
          ],
          [
            'id'            => "modal_content",
            'label'         => "Modal Content",
            'type'          => "input",
            'render'        => '<textarea id="%s" name="%s" class="form-control" rows="10" placeholder="Write some modal content...">%s</textarea>'
          ]
        ]
      ]
    ]
  ];

}

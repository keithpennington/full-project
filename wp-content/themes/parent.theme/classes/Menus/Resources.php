<?php

namespace Theme\Parent\Menu;

use WP_Admin_Bar, Theme\Parent\Menu;

class Resources extends Menu {

  public $slug = "resource-links";
  public $name = "Resources";

  protected function _setup () {
    
    add_action( 'init', [ $this, "setMenuID" ] );
    add_action( 'admin_bar_menu', [ $this, "create" ], 90 );

  }

  public function create( WP_Admin_Bar $admin_bar ) {
    
    $locations  = get_nav_menu_locations();
    $menu       = false;
    $items      = false;

    if( $locations && isset( $locations[$this->slug] ) )
      $menu = wp_get_nav_menu_object( $locations[$this->slug] );

    if( $menu )
      $items = wp_get_nav_menu_items( $menu->term_id );

    if( $items ) {

      $admin_bar->add_menu( [
        'id'    => $this->slug,
        'title' => "<span class=\"ab-icon\"></span> Resources",
      ] );

      foreach( $items as $item ) {

        $admin_bar->add_menu( [
          'id'      => $item->post_name,
          'parent'  => $this->slug,
          'group'   => false,
          'title'   => $item->post_title,
          'href'    => $item->url,
          'meta'    => [
            'title'   => $item->post_title,
            'target'  => $item->target
          ]
        ] );

      }

    }

  }

  public static function printReference() {

    ob_start() ?>

    <p class="card-text">
      This is a dashboard-only menu.  This location is the menu that will be used to populate links in on the admin bar under "Resource  Links".  You can add as many menu items as you'd like to this  menu.  It  is  designed as a sort of "bookmark" bar to  reference  content  resources while  you're  publishing or managing your WordPress content.
    </p>
    <?php if( isset( self::$menuID ) ) { ?>
    <a class="btn btn-info btn-sm" href="/wp-admin/nav-menus.php?menu=<?php echo self::$menuID ?>">Edit Menu</a>
    <?php } ?>

    <?php echo ob_get_clean();

  }

}

<?php

namespace Theme\Parent\Menu;

use Theme\Parent\{ Menu, Utility };

class Footer extends Menu {

  public $slug = "footer";
  public $name = "Footer";

  public function defaultArgs( $args ) {

    if( ! empty( $args['menu'] ) && $args['menu']->term_id === self::$menuID ) {

      $overrides = [
        'theme_location'  => $this->slug,
        'container'       => false,
        'menu_class'      => "nav-footer-items",
        'menu_id'         => "nav-footer",
        'echo'            => true,
        'fallback_cb'     => "wp_page_menu",
        'items_wrap'      => '<nav id="%1$s" class="%2$s">%3$s</nav>',
        'depth'           => 1,
        'walker'          => new \Theme\Parent\Menu\Footer\Walker()
      ];
      $args = wp_parse_args( $overrides, $args );

    }

    return $args;

  }

  public static function printReference() {

    ob_start() ?>

    <p class="card-text">
      This menu location will place a small navigation menu in the footer, typically reserved for global site links like a Sitemap, Terms & Conditions, and/or a Privacy Policy.
    </p>
    <?php if( isset( self::$menuID ) ) { ?>
    <a class="btn btn-info btn-sm" href="/wp-admin/nav-menus.php?menu=<?php echo self::$menuID ?>">Edit Menu</a>
    <?php } ?>

    <?php echo ob_get_clean();

  }

}

namespace Theme\Parent\Menu\Footer;

use Walker_Nav_Menu, Theme\Parent\Utility;

class Walker extends Walker_Nav_Menu {

  /**
   * Strip Default Classes
   *
   * Remove excessive default WP classes that are noisy.
   * Prefer a clean menu markup experience;
   * custom classes added in this Walker.
   *
   * @see self::start_el
   *
   * @param array $classes - Array of classes generated for a WP_Nav_Item
   *
   * @return array $classes
   */
  private function _stripDefaultClasses( array $classes ): array {

    $classes = array_map( function( $class ) {

      if( preg_match("/(menu-item)/i", $class) )
        return "";

      return $class;

    }, $classes );

    return $classes;

  }

  public function start_lvl( &$output, $depth = 0, $args = null ) { }

  public function end_lvl( &$output, $depth = 0, $args = null ) { }

  /**
   * Start Element
   * 
   * Construct nav item html.
   *
   * @see Walker_Nav_Menu::start_el()
   *
   * @param string   $output Used to append additional content (passed by reference).
   * @param WP_Post  $item   Menu item data object.
   * @param int      $depth  Depth of menu item. Used for padding.
   * @param stdClass $args   An object of wp_nav_menu() arguments.
   * @param int      $id     Current item ID.
   *
   * @link https://developer.wordpress.org/reference/classes/walker_nav_menu/
   */
  public function start_el( &$output, $item, $depth = 0, $args = null, $id = 0 ) {

    if( ! is_array( $item->classes ) )
      return;

    $defaultClasses     = [ "footer-item" ];
    $addedClasses       = [];
    $iconClasses        = [];
    $item->classes      = array_filter( $this->_stripDefaultClasses( $item->classes ) );
      

    if( ! empty( $item->classes ) ) {

      foreach( $item->classes as $class ) {

        if( preg_match( '/(fa-.*|^fas|^fab)/i', $class ) )
          $iconClasses[] = $class;
        else
          $addedClasses[] = $class;

      }

    }

    $itemClasses = array_merge( $defaultClasses, $addedClasses );

    if( ! empty( $iconClasses ) )
      $item->title = sprintf('<i class="%s" aria-hidden="true"></i> %s', implode( " ", $iconClasses ), $item->title );

    $output .= sprintf( '<a class="%s" href="%s">%s', implode( " ", apply_filters( 'nav_menu_css_class', $itemClasses, $item, $args, $depth ) ), $item->url, $item->title );

  }
  /**
   * Ends the element output, if needed.
   *
   * @see Walker_Nav_Menu::end_el()
   *
   * @param string   $output Used to append additional content (passed by reference).
   * @param WP_Post  $item   Page data object. Not used.
   * @param int      $depth  Depth of page. Not Used.
   * @param stdClass $args   An object of wp_nav_menu() arguments.
   *
   * @link https://developer.wordpress.org/reference/classes/walker_nav_menu/
   */
  public function end_el( &$output, $item, $depth = 0, $args = null ) {

      $output .= "</a>\n";

  }

}

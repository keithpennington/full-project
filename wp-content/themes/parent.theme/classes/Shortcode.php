<?php

namespace Theme\Parent;

class Shortcode {

  use Traits\Reference;

  public $slug;
  public $name;

  function __construct( $slug = null, $name = null, $function = null ) {
    
    if( property_exists( __CLASS__, "_instance" ) )
      self::$_instance = $this;

    if( ! is_null( $slug ) )
      $this->slug = $slug;

    if( ! is_null( $name ) )
      $this->name = $name;

    if( ! is_null( $function ) )
      $execute = $function;
    else
      $execute = [ $this, "execute" ];

    /**
     * Add Shortcode
     * 
     * @param tag (string)
     * @param callback (callable)
     *
     * @link https://developer.wordpress.org/reference/functions/add_shortcode/
     */
    add_shortcode( $this->slug, $execute );

    $this->_themeReference( "shortcodes" );

  }
  /**
   * Execute
   * The function invoked by the shortcode,
   * which receives the parameters as arguments.
   *
   * @param $atts (array) - the attributes passed to the shortcode
   * @param $content (string) - the html content used inside of the shortcode tags.
   *
   * @link https://developer.wordpress.org/reference/functions/add_shortcode/#parameters
   */
  public function execute( $atts, $content = null ) { }

}

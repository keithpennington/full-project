<?php

namespace Theme\Parent;

use Theme\Parent\{ Utility, Field };

class SettingsPage {
  
  protected $_args = [];

  function __construct( $args = null ) {

    if( null !== $args )
      $this->_args = $args;

    add_action( 'admin_menu', [ $this, "adminMenu" ] );
    add_action( 'admin_init', [ $this, "pageSections" ] );

  }

  private function _addMenuPage( $capabilities, $position ) {

    add_menu_page(
      $this->_args['page_title'],
      $this->_args['menu_title'],
      $capabilities,
      $this->_args['id'],
      [ $this, "pageRenderCallback" ],
      $this->_args['icon'],
      $position
    );

    $this->_adjust();

  }

  private function _addSubMenuPage( $capabilities, $position ) {

    add_submenu_page(
      $this->_args['anchor'],
      $this->_args['page_title'],
      $this->_args['menu_title'],
      $capabilities,
      $this->_args['id'],
      [ $this, "pageRenderCallback" ],
      $position
    );

    $this->_adjust();

  }

  private function _addSection( $pageId, $sectionName, $details ) {

    register_setting( $pageId, $sectionName );
    add_settings_section(
      $sectionName,
      $details['title'],
      [ $this, "sectionRenderCallback" ],
      $pageId
    );

    foreach( $details['fields'] as $field ) {

      $label = $field['label'];
      unset( $field['label'] );

      add_settings_field(
        $field['id'],
        $label,
        [ $this, 'fieldRenderCallback' ],
        $pageId,
        $sectionName,
        [
          'field'   => $field,
          'section' => $sectionName
        ]
      );
      
    }

  }

  protected function _adjust() { }

  public function adminMenu() {

    if( empty( $this->_args ) || ! isset( $this->_args['menu_type'] ) )
      return;

    $capabilities       = isset( $this->_args['capabilities'] ) ? $this->_args['capabilities'] : 'manage_options';
    $position           = isset( $this->_args['position'] ) ? $this->_args['position'] : 0;
    $addPageOfMenuType  = "_add" . $this->_args['menu_type'] . "Page";

    if( method_exists( $this, $addPageOfMenuType ) )
      $this->$addPageOfMenuType( $capabilities, $position );

  }

  public function pageSections() {

    if( ! empty( $this->_args ) && isset( $this->_args['sections'] ) ) {

      foreach( $this->_args['sections'] as $section => $details )
        $this->_addSection( $this->_args['id'], $section, $details );
      
    }

  }

  public function pageRenderCallback() {

    Utility::getScopedTemplatePart(
      'template-parts/form/form',
      'settings',
      $this->_args
    );

  }

  public function sectionRenderCallback( $args ) {

    if( array_key_exists( $args['id'], $this->_args['sections'] ) && isset( $this->_args['sections'][$args['id']]['description'] ) )
      echo $this->_args['sections'][$args['id']]['description'];

  }

  public function fieldRenderCallback( $args ) {
    
    $options      = $args['field'];
    $value        = Utility::getOption( $args['section'], $options['id'] );
    $value        = empty( $value ) && isset( $args['field']['default'] ) ? $args['field']['default'] : $value;
    $templateArgs = [
      'id'      => sprintf( "%s[%s]", $args['section'], $options['id'] ),
      'value'   => $value,
      'options' => $options
    ];

    if( "text" === $options['type'] )
      $options['class'] = $args['section'] . "-field " . $options['type'] . "-field form-control";

    Utility::getScopedTemplatePart(
      "template-parts/field/field",
      $options['type'],
      $templateArgs
    );

  }

}

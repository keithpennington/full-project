<?php

namespace Theme\Parent\PostType;

use Theme\Parent\{ PostType, Utility };
use WP_Query;

class Message extends PostType {

  use \Theme\Parent\Traits\Instance;

  public $name = "Message";
  public $slug = "message";
  public $args = [
    'description'          => "Create messages for notifications and user acknowledgements.",
    'public'               => false,
    'hierarchical'         => false,
    'exclude_from_search'  => true,
    'publicly_queryable'   => false,
    'show_ui'              => true,
    'show_in_menu'         => true,
    'show_in_nav_menus'    => false,
    'show_in_admin_bar'    => true,
    'show_in_rest'         => true,
    'menu_position'        => 30,
    'menu_icon'            => "dashicons-megaphone",
    'supports' => [
      "title",
      "page-attributes",
      "editor"
    ],
    'register_meta_box_cb' => [],
    'labels' => [
      'name'                       => "Messages",
      'singular_name'              => "Message",
      'add_new'                    => "Add Message",
      'add_new_item'               => "Add New Message",
      'edit_item'                  => "Edit Message",
      'new_item'                   => "New Message",
      'view_item'                  => "View Message",
      'view_items'                 => "View Messages",
      'search_items'               => "Search Messages",
      'not_found'                  => "No Messages found.",
      'not_found_in_trash'         => "No Messages found in trash.",
      'all_items'                  => "All Messages",
      'archives'                   => "Message Archives",
      'attributes'                 => "Message Attributes",
      'insert_into_item'           => "Insert into Message",
      'uploaded_to_this_item'      => "Uploaded to this Message",
      'featured_image'             => "Message Image",
      'set_featured_image'         => "Set Message image",
      'remove_featured_image'      => "Remove Message image",
      'use_featured_image'         => "Use as Message image",
      'menu_name'                  => "Messages",
      'filter_items_list'          => "Filter Messages list",
      'items_list_navigation'      => "Message list navigation",
      'items_list'                 => "Messages list",
      'item_published'             => "Message published.",
      'item_published_privately'   => "Message published privately.",
      'item_reverted_to_draft'     => "Message reverted to draft.",
      'item_scheduled'             => "Message scheduled.",
      'item_updated'               => "Message updated."
    ]
  ];
  public $metaboxes = [
    [
      'id'        => "details",
      'context'   => "side",
      'title'     => "Message Details",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "style",
          'type'    => "select",
          'options' => [
            'label'       => "Style",
            'default'     => "is-info",
            'desc'        => "Choose a coloring theme for this message.",
            'options_cb'  => "Theme\Parent\Framework\Bulma::styleOptions"
          ]
        ],
        [
          'id'      => "size",
          'type'    => "select",
          'options' => [
            'label'       => "Size",
            'default'     => "is-small",
            'desc'        => "Choose a size for this message.",
            'options_cb'  => "Theme\Parent\Framework\Bulma::sizeOptions"
          ]
        ],
        [
          'id'      => "dismiss_behavior",
          'type'    => "select",
          'options' => [
            'label'     => "Dismiss Behavior",
            'default'   => "remember",
            'desc'      => "Choose if a site visitor can dismiss the message, and whether their browser should remember this action.",
            'options'   => [
              'disallowed'  => "Not allowed",
              'allowed'     => "Allowed, not remembered",
              'remember'    => "Allowed and remembered"
            ]
          ]
        ]
      ],
      'nonce'     => [
        'name'    => "loc_message_details",
        'action'  => "save_loc_message_details"
      ]
    ]
  ];
  public $useDefaultMeta = false;

  private $_boardArgs = [
    'post_type'   => "message",
    'status'      => "publish",
    'orderby'     => "menu_order",
    'order'       => "DESC"
  ];

  function __construct() {

    parent::__construct();

    add_action( 'message_board', [ $this, "renderMessages" ] );

  }

  public function renderMessages() {

    $args   = apply_filters( 'message_args', $this->_boardArgs );
    $board  = new WP_Query( $args );

    if( $board->have_posts() ) {

      while( $board->have_posts() ) {

        $board->the_post();
        $props  = get_post_meta( get_the_ID(), '_details', true );
        $id     = "message-" . get_the_ID();

        if( ! isset( $_COOKIE[$id] ) || "dismissed" !== $_COOKIE[$id] || "remember" !== $props['dismiss_behavior'] ) {

          Utility::getScopedTemplatePart(
            "template-parts/ui/ui",
            "message",
            $props
          );

        }

      }

    }

    wp_reset_postdata();

  }

  public static function printReference() {

    ob_start() ?>

    <p class="card-text">
      Messages are a way to alert or notify website visitors.  They display as a toast when the site loads.  Use this to make announcements about new services, special hours, or privacy policies.
    </p>
    <a class="btn btn-info btn-sm" href="/wp-admin/edit.php?post_type=message">Edit Messages</a>

    <?php echo ob_get_clean();

  }

}

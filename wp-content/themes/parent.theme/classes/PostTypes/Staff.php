<?php

namespace Theme\Parent\PostType;

use Theme\Parent\{ PostType, Utility };

class Staff extends PostType {

  use \Theme\Parent\Traits\Instance;

  public $name = "Staff Members";
  public $slug = "staff_member";
  public $args = [
    'description'          => "Create staff members and departments for dynamic syndication to staff pages.",
    'public'               => false,
    'hierarchical'         => false,
    'exclude_from_search'  => true,
    'publicly_queryable'   => false,
    'show_ui'              => true,
    'show_in_menu'         => true,
    'show_in_nav_menus'    => false,
    'show_in_admin_bar'    => true,
    'show_in_rest'         => true,
    'menu_position'        => 20,
    'menu_icon'            => "dashicons-groups",
    'supports' => [
      "editor",
      "page-attributes",
      "thumbnail",
      "title",
    ],
    'register_meta_box_cb' => [],
    'labels' => [
      'name'                       => "Staff Members",
      'singular_name'              => "Staff Member",
      'add_new'                    => "Add Staff Member",
      'add_new_item'               => "Add New Staff Member",
      'edit_item'                  => "Edit Staff Member",
      'new_item'                   => "New Staff Member",
      'view_item'                  => "View Staff Member",
      'view_items'                 => "View Staff Members",
      'search_items'               => "Search Staff Members",
      'not_found'                  => "No Staff Members found.",
      'not_found_in_trash'         => "No Staff Members found in trash.",
      'all_items'                  => "All Staff Members",
      'archives'                   => "Staff Member Archives",
      'attributes'                 => "Staff Member Attributes",
      'insert_into_item'           => "Insert into Staff Member",
      'uploaded_to_this_item'      => "Uploaded to this Staff Member",
      'featured_image'             => "Profile Image",
      'set_featured_image'         => "Set profile image",
      'remove_featured_image'      => "Remove profile image",
      'use_featured_image'         => "Use as profile image",
      'menu_name'                  => "Staff",
      'filter_items_list'          => "Filter Staff Member list",
      'items_list_navigation'      => "Staff Member list navigation",
      'items_list'                 => "Staff Members list",
      'item_published'             => "Staff Member published.",
      'item_published_privately'   => "Staff Member published privately.",
      'item_reverted_to_draft'     => "Staff Member reverted to draft.",
      'item_scheduled'             => "Staff Member scheduled.",
      'item_updated'               => "Staff Member updated."
    ]
  ];
  public $metaboxes = [
    [
      'id'        => "details",
      'context'   => "advanced",
      'title'     => "Staff Member Details",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "title",
          'type'    => "input",
          'options' => [
            'label'     => "Job Title",
            'render'    => '<input type="text" id="%s" name="%s" value="%s" class="form-control" />'
          ]
        ],
        [
          'id'      => "linkedin",
          'type'    => "input",
          'options' => [
            'label'     => "LinkedIn Profile",
            'render'    => '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="https://linkedin.com/..." />'
          ]
        ],
        [
          'id'      => "facebook",
          'type'    => "input",
          'options' => [
            'label'     => "Facebook Profile",
            'render'    => '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="https://facebook.com/..." />'
          ]
        ],
        [
          'id'      => "twitter",
          'type'    => "input",
          'options' => [
            'label'     => "Twitter Profile",
            'render'    => '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="https://twitter.com/..." />'
          ]
        ]
      ],
      'nonce'     => [
        'name'    => "loc_staff_member_details",
        'action'  => "save_loc_staff_member_details"
      ]
    ]
  ];
  public $taxonomies = [
    'department' => [
      'hierarchical'        => true,
      'show_ui'             => true,
      'show_admin_column'   => true,
      'show_in_nav_menus'   => false,
      'show_in_rest'        => true,
      'show_tagcloud'       => false,
      'query_var'           => true,
      'labels'              => [
        'name'                    => "Departments",
        'singular_name'           => "Department",
        'search_items'            => "Search Departments",
        'all_items'               => "All Departments",
        'edit_item'               => "Edit Departments",
        'update_item'             => "Update Departments",
        'add_new_item'            => "Add New Department",
        'new_item_name'           => "New Department Name",
        'menu_name'               => "Departments",
        'not_found'               => "No Departments founds.",
        'no_terms'                => "No Departments",
        'items_list_navigation'   => "Department list navigation",
        'items_list'              => "Department list",
        'back_to_items'           => "&larr; Back to Departments"
      ]
    ]
  ];
  public $useDefaultMeta = false;

  public static function printReference() {

    ob_start() ?>

    <p class="card-text">
      Staff Members are a way to dynamically create a staff page with current employees by managing them in one place.
    </p>
    <p class="card-text">
      Staff Members have a taxonomy of Departments that can be used to group them together for department-specific pages.
    </p>
    <a class="btn btn-info btn-sm" href="/wp-admin/edit.php?post_type=staff_member">Edit Staff Members</a>

    <?php echo ob_get_clean();

  }

}

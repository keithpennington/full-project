<?php

namespace Theme\Parent\PostType;

use Theme\Parent\{ PostType, Utility };

class Service extends PostType {

  use \Theme\Parent\Traits\TermMetaFields;
  use \Theme\Parent\Traits\Instance;

  public $name    = "Service";
  public $slug    = "service";

  public $args    = [
    'description'          => "Create Services for use on the website. These will exhibit fields to set the relevant <a target='_new' href='https://schema.org/Service'>Schema.org Service properties</a> for structure data.",
    'public'               => true,
    'hierarchical'         => true,
    'exclude_from_search'  => false,
    'publicly_queryable'   => true,
    'show_ui'              => true,
    'show_in_menu'         => true,
    'show_in_nav_menus'    => true,
    'show_in_admin_bar'    => true,
    'show_in_rest'         => true,
    'menu_position'        => 15,
    'menu_icon'            => "dashicons-money-alt",
    'supports' => [
      "author",
      "custom-fields",
      "editor",
      "excerpt",
      "page-attributes",
      "revisions",
      "thumbnail",
      "title"
    ],
    'taxonomies'            => [
      "category"
    ],
    'register_meta_box_cb' => [],
    'labels' => [
      'name'                       => "Services",
      'singular_name'              => "Service",
      'add_new'                    => "Add Service",
      'add_new_item'               => "Add New Service",
      'edit_item'                  => "Edit Service",
      'new_item'                   => "New Service",
      'view_item'                  => "View Service",
      'view_items'                 => "View Services",
      'search_items'               => "Search Services",
      'not_found'                  => "No Services found.",
      'not_found_in_trash'         => "No Services found in trash.",
      'parent_item_colon'          => "Parent Service:",
      'all_items'                  => "All Services",
      'archives'                   => "Service Archives",
      'attributes'                 => "Service Attributes",
      'insert_into_item'           => "Insert into Service",
      'uploaded_to_this_item'      => "Uploaded to this Service",
      'featured_image'             => "Featured Image",
      'set_featured_image'         => "Set featured image",
      'remove_featured_image'      => "Remove featured image",
      'use_featured_image'         => "Use as featured image",
      'menu_name'                  => "Services",
      'filter_items_list'          => "Filter Services list",
      'items_list_navigation'      => "Service list navigation",
      'items_list'                 => "Services list",
      'item_published'             => "Service published.",
      'item_published_privately'   => "Service published privately.",
      'item_reverted_to_draft'     => "Service reverted to draft.",
      'item_scheduled'             => "Service scheduled.",
      'item_updated'               => "Service updated."
    ]
  ];
  public $metaboxes = [
    [
      'id'        => "details",
      'context'   => "side",
      'title'     => "Service Details",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "icon",
          'type'    => "input",
          'options' => [
            'desc'      => "Put the URL for an SVG (preferred) or PNG icon here that can be used throughout the site to represent this service.",
            'label'     => "Icon",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" placeholder="https://..." class="form-control" />'
          ]
        ]
      ],
      'nonce' => [
        'name'    => "loc_service_details",
        'action'  => "save_loc_service_details"
      ]
    ],
    [
      'id'        => "schema",
      'context'   => "advanced",
      'title'     => "Service Schema",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "name",
          'type'    => "input",
          'options' => [
            'desc'      => "The name of the item.",
            'label'     => "Service Name",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "alternateName",
          'type'    => "input",
          'options' => [
            'desc'      => "An alias for the item.",
            'label'     => "Alternate Name",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "serviceType",
          'type'    => "input",
          'options' => [
            'desc'      => "The type of service being offered, e.g. veterans' benefits, emergency relief, etc.",
            'label'     => "Service Type",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "additionalType",
          'type'    => "input",
          'options' => [
            'desc'      => "An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in.",
            'label'     => "Additional Type",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "award",
          'type'    => "input",
          'options' => [
            'desc'      => "An award won by or for this item. Supersedes <a target='_new' href='https://schema.org/awards'>awards</a>.",
            'label'     => "Award",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "category",
          'type'    => "input",
          'options' => [
            'desc'      => "A category for the item. Greater signs or slashes can be used to informally indicate a category hierarchy.",
            'label'     => "Category",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "providerMobility",
          'type'    => "input",
          'options' => [
            'desc'      => "Indicates the mobility of a provided service (e.g. 'static', 'dynamic').",
            'label'     => "Provider Mobility",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "description",
          'type'    => "input",
          'options' => [
            'desc'      => "A description of the item. <b>If left blank, this Service's excerpt will be used in the Schema for this field &mdash; fill in the excerpt instead for better time savings and decreased redundancy.</b>",
            'label'     => "Description",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "image",
          'type'    => "input",
          'options' => [
            'desc'      => "An image of the item. <b>If left blank, this Service's featured image will be used in the Schema for this field &mdash; use the featured image instead for better time savings and decreased redundancy.</b>",
            'label'     => "Image (URL)",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" placeholder="https://..." />',
          ]
        ],
        [
          'id'      => "mainEntityOfPage",
          'type'    => "input",
          'options' => [
            'desc'      => "Indicates a page (or other CreativeWork) for which this thing is the main entity being described. See background notes for details.",
            'label'     => "Main Entity of Page (URL)",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" placeholder="https://..." />',
          ]
        ],
        [
          'id'      => "sameAs",
          'type'    => "input",
          'options' => [
            'desc'      => "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website.",
            'label'     => "Same As (URL)",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" placeholder="https://..." />',
          ]
        ],
        [
          'id'      => "url",
          'type'    => "input",
          'options' => [
            'desc'      => "URL of the item.  <b>If left blank, this will be automatically filled in with the full permalink of this page.</b>",
            'label'     => "URL of Service",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" placeholder="https://..." />',
          ]
        ]
      ],
      'nonce'       => [
        'name'    => "loc_service_schema_fields",
        'action'  => "save_loc_service_schema"
      ]
    ]
  ];
  public $useDefaultMeta = true;

  function __construct() {

    parent::__construct();

    // Filter
    add_filter( 'site_schema', [ $this, "serviceSchema" ], 10, 4 );

  }

  public function serviceSchema( $schema, $info, $og, $hours ) {

    global $post;

    if( $this->slug === $post->post_type ) {

      $serviceSchema = Utility::getPostMetaValue( "schema" );

      if( ! empty( $serviceSchema ) && is_array( $serviceSchema ) ) {

        if( ( ! isset( $serviceSchema['image'] ) || empty( $serviceSchema['image'] ) ) && has_post_thumbnail( $post->ID ) )
          $serviceSchema['image'] = get_the_post_thumbnail_url();

        if( ! isset( $serviceSchema['description'] ) || empty( $serviceSchema['description'] ) )
          $serviceSchema['description'] = get_the_excerpt();

        if( ! isset( $serviceSchema['url'] ) || empty( $serviceSchema['url'] ) )
          $serviceSchema['url'] = get_the_permalink();

        $schema[] = Utility::getSchemaObject( "Service", $serviceSchema );

      }

    }
    
    return $schema;

  }

  public static function printReference() {

    ob_start() ?>

    <p class="card-text">
      Service posts are designed to act exactly like regular pages, but with additional properties and fields that allow us to distinguish them from regular pages for both SEO and content distribution purposes across the site.  By breaking out Service pages into a separate piece of content, we can work in my dynamic behavior and additional technical functionality to support organic search and advertising efforts.
    </p>
    <p class="card-text">
      Services exhibit unique fields for <a target="_new" href="https://schema.org/Service">Schema.org structured data</a>, as well as additional fields that can be used across the site, for example an icon URL.  As we add services to the website, we won't also have to update other pages to ensure that they are listed and linked to, since they are distinct from other pages, and we can query against them specifically as part of our content strategy and site build process.
    </p>
    <p class="card-text">
      Services also use the <a target="_new" href="/wp-admin/edit-tags.php?taxonomy=category&post_type=service">Category taxonomy</a>, exactly the same as Blog Posts -- so we can match the latest blogs to a Service page using the matching categories that they fall into.  In this way we can syndicate content across the site automatically to reduce our touch points in managing updates.
    </p>
    <a class="btn btn-info btn-sm" href="/wp-admin/edit.php?post_type=service">Edit Services</a>

    <?php echo ob_get_clean();

  }

}

<?php

namespace Theme\Parent\PostType;

use Theme\Parent\{ PostType, Utility };

class Location extends PostType {

  use \Theme\Parent\Traits\TermMetaFields;
  use \Theme\Parent\Traits\Instance;

  public $name    = "Location";
  public $slug    = "location";

  public $args    = [
    'description'          => "Create Services for use on the website. These will exhibit fields to set the relevant <a target='_new' href='https://schema.org/Service'>Schema.org Service properties</a> for structure data.",
    'public'               => true,
    'hierarchical'         => true,
    'exclude_from_search'  => false,
    'publicly_queryable'   => true,
    'show_ui'              => true,
    'show_in_menu'         => true,
    'show_in_nav_menus'    => true,
    'show_in_admin_bar'    => true,
    'show_in_rest'         => true,
    'menu_position'        => 15,
    'menu_icon'            => "dashicons-money-alt",
    'supports' => [
      "author",
      "custom-fields",
      "editor",
      "excerpt",
      "page-attributes",
      "revisions",
      "thumbnail",
      "title"
    ],
    'taxonomies'            => [
      "category"
    ],
    'register_meta_box_cb' => [],
    'labels' => [
      'name'                       => "Locations",
      'singular_name'              => "Location",
      'add_new'                    => "Add Location",
      'add_new_item'               => "Add New Location",
      'edit_item'                  => "Edit Location",
      'new_item'                   => "New Location",
      'view_item'                  => "View Location",
      'view_items'                 => "View Location",
      'search_items'               => "Search Locations",
      'not_found'                  => "No Locations found.",
      'not_found_in_trash'         => "No Locations found in trash.",
      'all_items'                  => "All Locations",
      'archives'                   => "Location Archives",
      'attributes'                 => "Location Attributes",
      'insert_into_item'           => "Insert into Location",
      'uploaded_to_this_item'      => "Uploaded to this Location",
      'featured_image'             => "Featured Image",
      'set_featured_image'         => "Set featured image",
      'remove_featured_image'      => "Remove featured image",
      'use_featured_image'         => "Use as featured image",
      'menu_name'                  => "Locations",
      'filter_items_list'          => "Filter Locations list",
      'items_list_navigation'      => "Location list navigation",
      'items_list'                 => "Locations list",
      'item_published'             => "Location published.",
      'item_published_privately'   => "Location published privately.",
      'item_reverted_to_draft'     => "Location reverted to draft.",
      'item_scheduled'             => "Location scheduled.",
      'item_updated'               => "Location updated."
    ]
  ];
  public $metaboxes = [
    [
      'id'        => "schema",
      'context'   => "advanced",
      'title'     => "Local Business Schema",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "name",
          'type'    => "input",
          'options' => [
            'desc'      => "Name of business location",
            'label'     => "Business Location Name",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "streetAddress",
          'type'    => "input",
          'options' => [
            'desc'      => "Street address of this location",
            'label'     => "Street Address",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "addressLocality",
          'type'    => "input",
          'options' => [
            'desc'      => "City of the location",
            'label'     => "City",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "addressRegion",
          'type'    => "input",
          'options' => [
            'desc'      => "State where the business is located",
            'label'     => "State",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "postalCode",
          'type'    => "input",
          'options' => [
            'desc'      => "Zip code of the business location",
            'label'     => "Zip Code",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "latitude",
          'type'    => "input",
          'options' => [
            'desc'      => "Geographical latitude can be pulled from Google Maps URL",
            'label'     => "Latitude",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "longitude",
          'type'    => "input",
          'options' => [
            'desc'      => "Geographical longitude can be pulled from Google Maps URL",
            'label'     => "Longitude",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "email",
          'type'    => "input",
          'options' => [
            'desc'      => "Email address for this location",
            'label'     => "Contact email",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "telephone",
          'type'    => "input",
          'options' => [
            'desc'      => "Phone number of this location",
            'label'     => "Phone Number",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "sameAs",
          'type'    => "input",
          'options' => [
            'desc'      => "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website.",
            'label'     => "Same As (URL)",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" placeholder="https://..." />',
          ]
        ],
        [
          'id'      => "priceRange",
          'type'    => "input",
          'options' => [
            'desc'      => "Price range for this location",
            'label'     => "Price Range",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" placeholder="$ - $$$$" />',
          ]
        ],
        [
          'id'      => "openingHours",
          'type'    => "input",
          'options' => [
            'desc'      => "Hours for this location",
            'label'     => "Hours of Operation",
            'render'    => '<input id="%s" name="%s" value="%s" type="text" class="form-control" />',
          ]
        ],
        [
          'id'      => "areaServed",
          'type'    => "input",
          'options' => [
            'desc'    => "Geographical areas served",
            'label'   => "Areas Served",
            'render'  => '<input id="%s" name="%s" value="%s" type="text" class="form-control />',  
          ]
        ]
      ],
      'nonce'       => [
        'name'    => "loc_service_schema_fields",
        'action'  => "save_loc_service_schema"
      ]
    ]
  ];
  public $useDefaultMeta = true;

  function __construct() {

    parent::__construct();

    // Filter
    add_filter( 'site_schema', [ $this, "locationSchema" ], 10, 4 );

  }

  public function locationSchema( $schema, $info, $og, $hours ) {

    global $post;

    if( $this->slug === $post->post_type ) {

      $locationSchema = Utility::getPostMetaValue( "schema" );

      if( ! empty( $locationSchema ) && is_array( $locationSchema ) ) {

        $arr = [
          'description' => get_the_excerpt(),
          'address' => [
            '@type'   =>  "PostalAddress",
            'streetAddress' => $locationSchema['streetAddress'],
            'addressLocality' =>  $locationSchema['addressLocality'],
            'addressRegion'   =>  $locationSchema['addressRegion'],
            'postalCode'      =>  $locationSchema['postalCode']
          ],
          'geo' => [
            '@type'   =>  "GeoCoordinates",
            'latitude'  =>  $locationSchema['latitude'],
            'longitude' =>  $locationSchema['longitude']
          ]
        ];

        $locationSchema = array_merge( $locationSchema, $arr );

        $remove = [ 'streetAddress', 'addressLocality', 'addressRegion', 'postalCode', 'latitude', 'longitude' ];

        foreach ( $remove as $key ) {
          unset( $locationSchema[$key] );
        }

        if( Utility::isMultiLocationSite() ) {
          $schema[] = Utility::getSchemaObject( "LocalBusiness", $locationSchema );
        }
      }

    }
    
    return $schema;

  }

  public static function printReference() {

    ob_start() ?>

    <p class="card-text">
      Service posts are designed to act exactly like regular pages, but with additional properties and fields that allow us to distinguish them from regular pages for both SEO and content distribution purposes across the site.  By breaking out Service pages into a separate piece of content, we can work in my dynamic behavior and additional technical functionality to support organic search and advertising efforts.
    </p>
    <p class="card-text">
      Services exhibit unique fields for <a target="_new" href="https://schema.org/Service">Schema.org structured data</a>, as well as additional fields that can be used across the site, for example an icon URL.  As we add services to the website, we won't also have to update other pages to ensure that they are listed and linked to, since they are distinct from other pages, and we can query against them specifically as part of our content strategy and site build process.
    </p>
    <p class="card-text">
      Services also use the <a target="_new" href="/wp-admin/edit-tags.php?taxonomy=category&post_type=service">Category taxonomy</a>, exactly the same as Blog Posts -- so we can match the latest blogs to a Service page using the matching categories that they fall into.  In this way we can syndicate content across the site automatically to reduce our touch points in managing updates.
    </p>
    <a class="btn btn-info btn-sm" href="/wp-admin/edit.php?post_type=location">Edit Locations</a>

    <?php echo ob_get_clean();

  }

}

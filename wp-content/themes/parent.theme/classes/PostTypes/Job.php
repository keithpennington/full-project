<?php

namespace Theme\Parent\PostType;

use Theme\Parent\{ PostType, Utility };

class Job extends PostType {

  use \Theme\Parent\Traits\Instance;

  public $name = "Job Postings";
  public $slug = "jobs";
  public $args = [
    'description'          => "Create Job Postings and departments for dynamic syndication to employment pages.",
    'public'               => true,
    'hierarchical'         => false,
    'exclude_from_search'  => false,
    'publicly_queryable'   => true,
    'show_ui'              => true,
    'show_in_menu'         => true,
    'show_in_nav_menus'    => false,
    'show_in_admin_bar'    => true,
    'show_in_rest'         => true,
    'menu_position'        => 30,
    'menu_icon'            => "dashicons-groups",
    'supports' => [
      "editor",
      "page-attributes",
      "thumbnail",
      "title",
    ],
    'taxonomies'            => [
      "department"
    ],

    'register_meta_box_cb' => [],
    'labels' => [
      'name'                       => "Job Postings",
      'singular_name'              => "Job Posting",
      'add_new'                    => "Add Job Posting",
      'add_new_item'               => "Add New Job Posting",
      'edit_item'                  => "Edit Job Posting",
      'new_item'                   => "New Job Posting",
      'view_item'                  => "View Job Posting",
      'view_items'                 => "View Job Postings",
      'search_items'               => "Search Job Postings",
      'not_found'                  => "No Job Postings found.",
      'not_found_in_trash'         => "No Job Postings found in trash.",
      'all_items'                  => "All Job Postings",
      'archives'                   => "Job Posting Archives",
      'attributes'                 => "Job Posting Attributes",
      'insert_into_item'           => "Insert into Job Posting",
      'uploaded_to_this_item'      => "Uploaded to this Job Posting",
      'featured_image'             => "Featured Image",
      'set_featured_image'         => "Set featured image",
      'remove_featured_image'      => "Remove featured image",
      'use_featured_image'         => "Use as featured image",
      'menu_name'                  => "Jobs",
      'filter_items_list'          => "Filter Job Posting list",
      'items_list_navigation'      => "Job Posting list navigation",
      'items_list'                 => "Job Posting list",
      'item_published'             => "Job Posting published.",
      'item_published_privately'   => "Job Posting published privately.",
      'item_reverted_to_draft'     => "Job Posting reverted to draft.",
      'item_scheduled'             => "Job Posting scheduled.",
      'item_updated'               => "Job Posting updated."
    ]
  ];
  public $metaboxes = [
    [
      'id'        => "details",
      'context'   => "advanced",
      'title'     => "Job Details",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "type",
          'type'    => "select",
          'options' => [
            'label'     => "Job Type",
            'options'   => [
              ''          => "Select a job type...",
              'full_time' => "Full Time",
              'part_time' => "Part Time",
              'temporary' => "Temporary",
              'contracted'  => "Contracted",
              'internship'  => "Internship"
            ]
          ]
        ],
        [
          'id'      =>  "salary",
          'type'    =>  "input",
          'options' =>  [
            'label'       =>  "Salary",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Salary Range..." />'

          ]
        ],
        [
          'id'      =>  "remote",
          'type'    =>  "checkbox",
          'options' =>  [
            'label'       =>  "Position is remote",
            'render'      =>  '<input type="checkbox" id="%s" value="%s" class="form-control" />'
          ]
        ]
      ],
      'nonce'     => [
        'name'    => "loc_job_post_details",
        'action'  => "save_loc_job_post_details"
      ]
    ],
    [
      'id'        => "location",
      'context'   => "advanced",
      'title'     => "Job Location",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      =>  "city",
          'type'    =>  "input",
          'options' =>  [
            'label'       =>  "City",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="City..." />'
          ]
        ],
        [
          'id'      =>  "state",
          'type'    =>  "input",
          'options' =>  [
            'label'       =>  "State",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="State..." />'
          ]
        ],
        [
          'id'      =>  "zip",
          'type'    =>  "input",
          'options' =>  [
            'label'       =>  "Zip",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" pattern="[0-9]*" placeholder="Zip..." />'
          ]
        ]
      ],
      'nonce'     => [
        'name'    => "loc_job_post_location",
        'action'  => "save_loc_job_post_location"
      ]
    ],
    [
      'id'        => "schema",
      'context'   => "advanced",
      'title'     => "Job Posting Schema",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      =>  "title",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The title of this position",
            'label'       =>  "Job Title",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Job Title..." />'
          ]
        ],
        [
          'id'      =>  "description",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "A short description of this job posting",
            'label'       =>  "Job Description",
            'render'      =>  '<textarea id="%s" name="%s" class="form-control">%s</textarea>'
          ]
        ],
        [
          'id'      =>  "jobLocation",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The location of the job",
            'label'       =>  "Job Location",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="City, State, Zip..." />'
          ]
        ],
        [
          'id'      =>  "jobLocationType",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The job location type. (Ex: Remote, Telecommute, In Office)",
            'label'       =>  "Job Location Type",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Job Location Type..." />'
          ]
        ],
        [
          'id'      =>  "employmentType",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The position type (Ex: Full Time, Part Time, Contracted, etc.)",
            'label'       =>  "Employment Type",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Employment Type..." />'
          ]
        ],
        [
          'id'      =>  "baseSalary",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The base salary for this position",
            'label'       =>  "Salary",
            'render'      =>  '<input type="number" id="%s" name="%s" value="%s" class="form-control" placeholder="Base Salary..." />'
          ]
        ],
        [
          'id'      =>  "hiringOrganization",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The name of the company that posted the postion",
            'label'       =>  "Hiring Organization",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Hiring Organization..." />'
          ]
        ],
        [
          'id'      =>  "employmentUnit",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The department or team this position is a part of",
            'label'       =>  "Department",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Department..." />'
          ]
        ],
        [
          'id'      =>  "industry",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The industry that this position is a part of",
            'label'       =>  "Industry",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Industry..." />'
          ]
        ],
        [
          'id'      =>  "skills",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "A comma separated list or description of the skills required for this position",
            'label'       =>  "Skills",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Skills..." />'
          ]
        ],
        [
          'id'      =>  "experienceRequirements",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "Description of skills and experience needed for the position or Occupation",
            'label'       =>  "Experience",
            'render'      =>  '<textarea type="text" id="%s" name="%s" class="form-control">%s</textarea>'
          ]
        ],
        [
          'id'      =>  "qualifications",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "Specific qualifications required for this role or Occupation",
            'label'       =>  "Qualifications",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Qualifications..." />'
          ]
        ],
        [
          'id'      =>  "responsibilities",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "Responsibilities associated with this role or Occupation",
            'label'       =>  "Responsibilities",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Responsibilities..." />'
          ]
        ],
        [
          'id'      =>  "workHours",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The typical working hours for this job (e.g. 1st shift, night shift, 8am-5pm)",
            'label'       =>  "Work Hours",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Work Hours..." />'
          ]
        ],
        [
          'id'      =>  "datePosted",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The date this position was posted",
            'label'       =>  "Date Posted",
            'render'      =>  '<input type="date" id="%s" name="%s" value="%s" class="form-control" />'
          ]
        ]
      ],
      'nonce'     => [
        'name'    => "loc_job_post_schema_fields",
        'action'  => "save_loc_job_post_schema"
      ]
    ]

  ];

  public $useDefaultMeta = true;

  function __construct() {

    parent::__construct();

    // Filters
    add_filter( 'site_schema', [ $this, "jobSchema" ], 10, 4 );

  }

  public function jobSchema( $schema, $info, $og, $hours ) {

    global $post;

    if( $this->slug === $post->post_type ) {

      $jobSchema = Utility::getPostMetaValue( "schema" );

      if( ! empty( $jobSchema ) && is_array( $jobSchema ) ) {

        if( isset( $jobSchema['baseSalary'] ) || ! empty( $jobSchema['baseSalary'] ) ) {
          $jobSchema['baseSalary'] = [
            '@type'       => 'Number', 
            'value'       =>  $jobSchema['baseSalary'], 
            'currency'    =>  'USD'
          ];
        }

        $schema[] = Utility::getSchemaObject( "JobPosting", $jobSchema );

      }

    }
    
    return $schema;

  }

  public static function printReference() {

    ob_start() ?>

    <p class="card-text">
      Job Postings are a way to dynamically create an employment page with current job postings by managing them in one place.
    </p>
    <p class="card-text">
      Job Postings have a taxonomy of Departments that can be used to group them together for department-specific employment pages.
    </p>
    <a class="btn btn-info btn-sm" href="/wp-admin/edit.php?post_type=job">Edit Jobs</a>

    <?php echo ob_get_clean();

  }

}

<?php

namespace Theme\Parent\PostType;

use Theme\Parent\{ PostType, Utility };

class MegaMenu extends PostType {

  use \Theme\Parent\Traits\TermMetaFields;
  use \Theme\Parent\Traits\Instance;

  public $name    = "Mega Menu";
  public $slug    = "megamenu";

  public $args    = [
    'description'          => "Create Mega Menus for use in navigation menus.",
    'public'               => true,
    'hierarchical'         => false,
    'exclude_from_search'  => true,
    'publicly_queryable'   => true,
    'show_ui'              => true,
    'show_in_menu'         => true,
    'show_in_nav_menus'    => false,
    'show_in_admin_bar'    => true,
    'show_in_rest'         => true,
    'menu_position'        => 24,
    'menu_icon'            => "dashicons-menu-alt3",
    'supports' => [
      "editor",
      "page-attributes",
      "title"
    ],
    'register_meta_box_cb' => [],
    'labels' => [
      'name'                       => "Mega Menus",
      'singular_name'              => "Mega Menu",
      'add_new'                    => "Add Mega Menu",
      'add_new_item'               => "Add New Mega Menu",
      'edit_item'                  => "Edit Mega Menu",
      'new_item'                   => "New Mega Menu",
      'view_item'                  => "View Mega Menu",
      'view_items'                 => "View Mega Menu",
      'search_items'               => "Search Mega Menus",
      'not_found'                  => "No Mega Menus found.",
      'not_found_in_trash'         => "No Mega Menus found in trash.",
      'all_items'                  => "All Mega Menus",
      'archives'                   => "Mega Menu Archives",
      'attributes'                 => "Mega Menu Attributes",
      'insert_into_item'           => "Insert into Mega Menu",
      'uploaded_to_this_item'      => "Uploaded to this Mega Menu",
      'featured_image'             => "Mega Menu Image",
      'set_featured_image'         => "Set Mega Menu image",
      'remove_featured_image'      => "Remove Mega Menu image",
      'use_featured_image'         => "Use as Mega Menu image",
      'menu_name'                  => "Mega Menus",
      'filter_items_list'          => "Filter Mega Menu list",
      'items_list_navigation'      => "Mega Menu list navigation",
      'items_list'                 => "Mega Menu list",
      'item_published'             => "Mega Menu published.",
      'item_published_privately'   => "Mega Menu published privately.",
      'item_reverted_to_draft'     => "Mega Menu reverted to draft.",
      'item_scheduled'             => "Mega Menu scheduled.",
      'item_updated'               => "Mega Menu updated."
    ]
  ];
 
  public $useDefaultMeta = false;

  public static function printReference() {

    ob_start() ?>

    <p class="card-text">
      Mega Menus can be assigned to navigation items in menus to display a fully customizable drop down menu.
    </p>
    <p>
     To assign a Mega Menu as a drop down, find the navigation item you want to assign it to, check the "Mega Menu" checkbox, and then select the title of the Mega Menu that you want to display.
    </p>
    <div class="alert alert-warning">
      Mega Menus will only display on desktop screens. The default children items that are assigned under a menu item will appear on mobiel and tablet screens. 
    </div>
    <a class="btn btn-info btn-sm" href="/wp-admin/edit.php?post_type=megamenu">Manage Mega Menus</a>

    <?php echo ob_get_clean();

  }

}

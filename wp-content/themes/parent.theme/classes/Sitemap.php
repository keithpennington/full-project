<?php

namespace Theme\Parent;

use WP_Query;

class Sitemap {
  
  use Traits\Instance, Traits\Redirect;

  private $_rewrites = [
    '(^sitemap\.xml$)' => "page-templates-infrastructure/sitemap_index-xml"
  ];

  public $pages = [];
  public $indexedPostTypes;

  function __construct() {

    self::$_instance = $this;

    $this->indexedPostTypes = Defaults::instance()->getPostTypes();

    // Actions
    $this->redirects( $this->_rewrites );
    add_action( 'wp_loaded', [ $this, "cleanupYoast" ] );

    // Filters
    add_filter( 'rewrite_rules_array', [ $this, "cleanupRewrites" ], 90 );
    add_filter( 'robots_txt', [ $this, "robots" ], 99, 2 );

  }

  public function cleanupRewrites( $rewrite_rules ) {

    foreach( $rewrite_rules as $rule => $rewrite) {

      if( preg_match( '/((?<!wp-)sitemap[^\.]+\.xsl|wp-sitemap)/', $rule ) )
        unset($rewrite_rules[$rule]);
      elseif( preg_match( '/((?<!wp-)sitemap[^\.]+\.xml)/', $rule ) )
        $rewrite_rules[$rule] = "sitemap-xml.php";

    }
    
    return $rewrite_rules;

  }

  /**
   * Robots TXT
   *
   * @param $output (string) The robots.txt output.
   * @param $public (bool) Whether the site is considered "public".
   *
   * @return $output (string)
   */
  public function robots( $output, $public ) {

    $privatePosts = apply_filters( 'robots_disallowed', [] );
    $args         = [
      'post_type'   => $this->indexedPostTypes,
      'post_status' => "publish",
      'meta_query'  => [
        'key'     => "_seo_props",
        'compare' => "EXISTS"
      ]
    ];
    $unindexed    = new WP_Query( $args );

    if( $unindexed->have_posts() ) {

      $output .= "\nUser-agent: *";

      while( $unindexed->have_posts() ) {

        $unindexed->the_post();

        $check = [
          'field'   => "_seo_props",
          'key'     => "sitemap_exclusion",
          'value'   => "on"
        ];

        if( Utility::checkMetaProperty( $check ) ) {

          $output .= "\nDisallow: ";
          $output .= str_replace( home_url(), "", get_the_permalink() );

        }

      }

    }
    wp_reset_postdata();

    if( ! empty( $privatePosts ) ) {

      foreach( $privatePosts as $postType )
        $output .= "\nDisallow: /" . $postType . "/";
      
    }

    $output .= "\n\nSitemap: " . site_url( "/sitemap.xml" );
    return $output;

  }

  /**
   * Cleanup Yoast
   * For sites that have had Yoast installed,
   * this carries over the search-excluded content
   * to the theme's native sitemap rendering.
   */
  public function cleanupYoast() {

    $metaField = "_seo_props";
    $defaultTypes = $this->indexedPostTypes;
    $args = [
      'post_status' => "any",
      'post_type'   => $defaultTypes,
      'meta_query'  => [
        'relation' => "AND",
        [
          'key'     => $metaField,
          'compare' => "NOT EXISTS"
        ],
        [
          'key'     => "_yoast_wpseo_meta-robots-noindex",
          'value'   => "1"
        ]
      ]
    ];
    $posts = new WP_Query( $args );

    if( $posts->have_posts() ) {

      while( $posts->have_posts() ) {

        $posts->the_post();

        Utility::updateMetaProperty( [
          'field' => $metaField,
          'key'   => "sitemap_exclusion",
          'value' => "on"
        ] );

      }

    }

    wp_reset_postdata();

  }

}

namespace Theme\Parent\Sitemap;

use Walker as W, Theme\Parent\Utility;

class Walker extends W {

  private $_excluded = false;

  public $db_fields = [
    'parent'  => "post_parent",
    'id'      => "ID"
  ];

  /**
   * Starts the list before the elements are added.
   *
   * The $args parameter holds additional values that may be used with the child
   * class methods. This method is called at the start of the output list.
   *
   * @since 2.1.0
   * @abstract
   *
   * @param string $output Used to append additional content (passed by reference).
   * @param int    $depth  Depth of the item.
   * @param array  $args   An array of additional arguments.
   */
  public function start_lvl( &$output, $depth = 0, $args = [] ) {

    if( "xml" === $args['markup'] )
      return;

    $indent  = str_repeat( "\t", $depth );
    $output .= $indent . "<ul class='child-page-list'>";

  }

  /**
   * Ends the list of after the elements are added.
   *
   * The $args parameter holds additional values that may be used with the child
   * class methods. This method finishes the list at the end of output of the elements.
   *
   * @since 2.1.0
   * @abstract
   *
   * @param string $output Used to append additional content (passed by reference).
   * @param int    $depth  Depth of the item.
   * @param array  $args   An array of additional arguments.
   */
  public function end_lvl( &$output, $depth = 0, $args = [] ) {

    if( "xml" !== $args['markup'] ) {

      $indent  = str_repeat( "\t", $depth );
      $output .= $indent . "</ul>\n";

    }

  }

  /**
   * Start the element output.
   *
   * The $args parameter holds additional values that may be used with the child
   * class methods. Includes the element output also.
   *
   * @since 2.1.0
   * @abstract
   *
   * @param string $output            Used to append additional content (passed by reference).
   * @param object $object            The data object.
   * @param int    $depth             Depth of the item.
   * @param array  $args              An array of additional arguments.
   * @param int    $current_object_id ID of the current item.
   */
  public function start_el( &$output, $object, $depth = 0, $args = [], $current_object_id = 0 ) {

    $propVal = [
      'post'  => $object->ID,
      'field' => "_seo_props",
      'key'   => "sitemap_exclusion",
      'value' => "on"
    ];

    $this->_excluded = false;

    if( Utility::checkMetaProperty( $propVal ) ) {

      $this->_excluded = true;
      return;

    }

    $indent  = str_repeat( "\t", $depth );

    if( "xml" === $args['markup'] ) {

      $output .= "\t<url>\n";
      $output .= "\t\t<loc>" . htmlentities( get_the_permalink( $object->ID ) ) . "</loc>\n";
      $output .= "\t\t<lastmod>" . date( "Y-m-d", strtotime( $object->post_modified ) ) . "</lastmod>\n";
      $output .= "\t\t<changefreq>" . $args['changefreq'] . "</changefreq>\n";
      $output .= "\t\t<priority>" . $args['priority'] . "</priority>\n";

    } else {

      $output .= $indent . "<li>";
      $output .= sprintf( '<a href="%s">%s', get_the_permalink( $object->ID ), $object->post_title );

    }

  }

  /**
   * Ends the element output, if needed.
   *
   * The $args parameter holds additional values that may be used with the child class methods.
   *
   * @since 2.1.0
   * @abstract
   *
   * @param string $output Used to append additional content (passed by reference).
   * @param object $object The data object.
   * @param int    $depth  Depth of the item.
   * @param array  $args   An array of additional arguments.
   */
  public function end_el( &$output, $object, $depth = 0, $args = [] ) {
    
    if( true === $this->_excluded )
      return;

    if( "xml" === $args['markup'] )
      $output .= "\t</url>\n";
    else
      $output .= "</a></li>\n";

  }

}


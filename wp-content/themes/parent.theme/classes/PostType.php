<?php

namespace Theme\Parent;

use Theme\Parent\{ Utility, Field };

class PostType {
  
  use Traits\PostMetaFields;
  use Traits\Reference;

  /**
   * Name
   * The nice name of the post type.
   *
   * @var (string)
   */
  public $name;

  /**
   * Slug
   * The slug of the post type.
   * 
   * @var (string)
   * @see PostType::_registerCPT()
   */
  public $slug;

  /**
   * Args
   * @var (array)
   *
   * @link https://developer.wordpress.org/reference/functions/register_post_type/#parameters
   * @link https://developer.wordpress.org/resource/dashicons/ for dashicon names used on 'menu_icon'
   * @see PostType::_registerCPT()
   * @example key => value pairs
    [
      'description'          => "Create character builds based on games, roles and play styles.",
      'public'               => true,
      'hierarchical'         => true,
      'exclude_from_search'  => false,
      'publicly_queryable'   => true,
      'show_ui'              => true,
      'show_in_menu'         => true,
      'show_in_nav_menus'    => true,
      'show_in_admin_bar'    => true,
      'show_in_rest'         => true,
      'menu_position'        => 4,
      'menu_icon'            => ""
      'capabilities' => [
        'edit_post',
        'read_post',
        'delete_post',
        'edit_posts',
        'edit_others_posts',
        'delete_posts',
        'publish_posts',
        'read_private_posts'
      ],
      'supports' => [
        'author',
        'comments',
        'custom-fields',
        'editor',
        'excerpt',
        'page-attributes',
        'revisions',
        'thumbnail',
        'title'
      ],
      'register_meta_box_cb' => [],
      'labels' => [
        'name'                       => "Builds",
        'singular_name'              => "Build",
        'add_new'                    => "Add Build",
        'add_new_item'               => "Add New Build",
        'edit_item'                  => "Edit Build",
        'new_item'                   => "New Build",
        'view_item'                  => "View Build",
        'view_items'                 => "View Builds",
        'search_items'               => "Search Builds",
        'not_found'                  => "No builds found.",
        'not_found_in_trash'         => "No builds found in trash.",
        'parent_item_colon'          => "Parent Build",
        'all_items'                  => "All Builds",
        'archives'                   => "Build Archives",
        'attributes'                 => "Build Attributes",
        'insert_into_item'           => "Insert into build",
        'uploaded_to_this_item'      => "Uploaded to this build",
        'featured_image'             => "Build image",
        'set_featured_image'         => "Set build image",
        'remove_featured_image'      => "Remove build image",
        'use_featured_image'         => "Use as Build image",
        'menu_name'                  => "Character Builds",
        'filter_items_list'          => "Filter builds list",
        'items_list_navigation'      => "Build list navigation",
        'items_list'                 => "Builds list",
        'item_published'             => "Build published.",
        'item_published_privately'   => "Build published privately",
        'item_reverted_to_draft'     => "Build reverted to draft.",
        'item_scheduled'             => "Build scheduled.",
        'item_updated'               => "Build updated."
      ]
   */
  public $args;

  /**
   * Taxonomies
   * Key of each array should be the slug of taxonomy.
   *
   * @var (array) of arrays
   *
   * @link https://developer.wordpress.org/reference/functions/register_taxonomy/#parameters
   * @see PostType::_registerTaxonomies()
   * @example key => value pairs:
    'roles' => [ 
      'hierarchical'          => true,
      'show_ui'               => true,
      'show_admin_column'     => true,
      'show_in_admin_columns' => true,
      'show_in_rest'          => true,
      'query_var'             => true,
      'rewrite'               => [ 'slug' => 'role' ],
      'labels'                => [
        'name'                    => "Roles",
        'singular_name'           => "Role",
        'search_items'            => "Search Roles",
        'all_items'               => "All Roles",
        'parent_item'             => "Parent Role",
        'parent_item_colon'       => "Parent Role:",
        'edit_item'               => "Edit Role",
        'update_item'             => "Update Role",
        'add_new_item'            => "Add New Role",
        'new_item_name'           => "New Role Name",
        'menu_name'               => "Role",
        'not_found'               => "No roles founds.",
        'no_terms'                => "No roles",
        'items_list_navigation'   => "Roles list navigation",
        'items_list'              => "Roles list",
        'back_to_items'           => "&larr; Back to Roles"
      ]
    ],
    'games' => [ ... ]
   */
  public $taxonomies;

  /**
   * Metaboxes
   *
   * @var (array) of arrays
   *
   * @see Traits\Metaboxes::addMetabox()
   * @example key => value pairs of a single array:
    [
      'id'        => "service_props",
      'context'   => "side",
      'title'     => "Service Properties",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "fa_icon",
          'type'    => "input",
          'options' => [
            'label'     => 'Icon Class<i>Refer to <a href="https://fontawesome.com/icons" target="_new">Font Awesome</a> to find a class name.</i>',
            'render'    => '<input id="%s" name="%s" type="text" value="%s" placeholder="e.g. fa-om" />',
            'container' => 'div'
          ]
        ]
      ],
      'nonce'     => [
        'name'    => "custom_nonce_name",
        'action'  => "custom_nonce_save_action"
      ]
    ]
   */
  public $metaboxes;

  /**
   * Params
   * Store extra parameters when using this class without extension
   * 
   * @var (array)
   */
  public $params;

  /**
   * Use Default Meta
   * Determines whether or not this post type should
   * use the standard metaboxes and custom fields like
   * posts and pages.
   *
   * @var (bool)
   * @see PostType::addDefaultMeta
   */
  public $useDefaultMeta = true;

  function __construct( array $params = [] ) {

    if( property_exists( __CLASS__, "_instance" ) )
      self::$_instance = $this;

    if( ! empty( $params ) ) {

      foreach( $params as $param => $value ) {

        if( property_exists( $this, $param ) )
          $this->$param = $value;
        else
          $this->params[$param] = $value;

      }

    }

    if( isset( $this->args['register_meta_box_cb'] ) )
      $this->args['register_meta_box_cb'] = [ $this, "addMetaboxes" ];

    $this->_setup();
    $this->_themeReference( "postTypes" );

  }

  private function _setup() {

    if( ! empty( $this->slug ) && ! empty( $this->args ) ) {

      // Immediate actions
      $this->_registerCPT();
      $this->_registerTaxonomies();
      $this->_registerSidebars();

      // Filters
      if( method_exists( $this, "addSchema" ) )
        add_filter( 'site_schema', [ $this, "addSchema" ], 10, 4);

      add_filter( 'sitemap_content_types', [ $this, "sitemap" ] );
      add_filter( 'robots_disallowed', [ $this, "robotsDisallowed"] );
      add_filter( 'default_meta_post_types', [ $this, "addDefaultMeta" ], 5 );

      // Actions
      if( is_array( $this->metaboxes ) && ! empty( $this->metaboxes ) )
        add_action( 'save_post', [ $this, "saveCustomFields" ], 10, 3 );

      if( method_exists( $this, "addToReference" ) )
        add_action( 'theme_reference_post_types', [ $this, "addToReference" ] );
      
    } else {

      error_log( __CLASS__ . ": Missing required values for arguments on post type registry." );

    }

  }

  private function _registerCPT() {

    // TO DO (i18n):
    // Use a trigger to run the translate function
    // on the values of each item in $this->args['labels']
    register_post_type( $this->slug, $this->args );

  }

  private function _registerTaxonomies() {

    if( is_array( $this->taxonomies ) ) {

      foreach( $this->taxonomies as $slug => $args ) {

        // TO DO (i18n):
        // Use a trigger to run the translate function
        // on the values of each item in $args['labels']
        register_taxonomy( $slug, [ $this->slug ], $args );

      }

    }

  }

  private function _searchable() {

    $searchable = true;

    if( isset( $this->args['exclude_from_search'] ) && true === $this->args['exclude_from_search'] )
      $searchable = false;

    if( isset( $this->args['publicly_queryable'] ) && false === $this->args['publicly_queryable'] )
      $searchable = false;
    
    return $searchable;

  }

  protected function _registerSidebars() {

    if( is_array( $this->taxonomies ) && ! empty( $this->taxonomies ) && $this->_searchable() ) {

      foreach( $this->taxonomies as $slug => $details ) {

        $args = [
          'name'          => "{$details['labels']['name']} Sidebar",
          'id'            => $slug,
          'description'   => "Sidebar to use on the {$details['labels']['name']} archive pages.",
          'before_widget' => '<div id="%1$s" class="widget %2$s">',
          'after_widget'  => "</div>"
        ];

        register_sidebar($args);

      }

    }

  }

  public function addMetaboxes() {

    if( is_array( $this->metaboxes ) && ! empty( $this->metaboxes ) ) {

      foreach( $this->metaboxes as $box )
        $this->addMetabox( $this->slug, $box );

    }

  }

  public function sitemap( $contentTypes ) {

    $public = isset( $this->args['public'] ) && $this->args['public'];
    
    if( $this->_searchable() && $public ) {

      $contentTypes[] = [
        'name'        => $this->slug,
        'title'       => $this->args['labels']['name'],
        'changefreq'  => "monthly",
        'priority'    => "0.5"
      ];

      if( ! empty( $this->taxonomies ) ) {

        foreach( $this->taxonomies as $slug => $tax ) {

          if( $tax['show_ui'] && $tax['show_in_rest'] ) {

            $contentTypes[] = [
              'name'        => $slug,
              'title'       => $tax['labels']['name'],
              'changefreq'  =>  "monthly",
              'priority'    => "0.5"
            ];

          }

        }

      }
      
    }

    return $contentTypes;

  }

  public function robotsDisallowed( array $disallowed ): array {

    if( isset( $this->args['exclude_from_search'] ) && true === $this->args['exclude_from_search'] )
      $disallowed[] = $this->slug;

    return $disallowed;

  }

  public function addDefaultMeta( $postTypes ) {

    if( isset( $this->useDefaultMeta ) && true === $this->useDefaultMeta )
      $postTypes[$this->slug] = $this->args['labels']['singular_name'];

    return $postTypes;

  }

}

<?php

namespace Theme\Parent;

class Users {

  protected $_users = [
    [
      'role'          => "manager",
      'name'          => "Manager",
      'basis'         => "administrator",
      'capabilities'  => [
        'activate_plugins'  => false,
        'delete_plugins'    => false,
        'delete_site'       => false,
        'delete_themes'     => false,
        'delete_users'      => false,
        'edit_themes'       => false,
        'edit_users'        => false,
        'install_plugins'   => false,
        'install_themes'    => false,
        'promote_users'     => false,
        'remove_users'      => false,
        'switch_themes'     => false,
        'update_core'       => false,
        'update_plugins'    => false,
        'update_themes'     => false,
      ]
    ]
  ];

  function __construct() {

    add_action( 'init', [ $this, "addUserRoles" ] );

  }

  function addUserRoles() {

    foreach( $this->_users as $newRole ) {

      if( isset( $newRole['basis'] ) ) {

        $basis = get_role( $newRole['basis'] )->capabilities;
        $capabilities = array_merge( $basis, $newRole['capabilities'] );

      } else {

        $capabilities = $newRole['capabilities'];

      }

      add_role( $newRole['role'], $newRole['name'], $capabilities );

    }

  }

}

new Users();

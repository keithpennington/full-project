<?php

namespace Theme\Parent\Framework;

use DOMDocument, Theme\Parent\Utility;

class Bulma {

  use \Theme\Parent\Traits\Instance;

  /**
   * Core Classes
   * Regex and replacements to use across all blocks
   *
   * @var
   */
  private $_coreClasses = [
    'wp'    => [
      "/has-([\S]+)-background-color/",
      "/has-((?!text)[\S]+)-color/",
      "/wp-block-column/",
      "/wp-block-image/",
      "/size-full/",
      "/are-vertically-aligned-center/",
      "/are-vertically-aligned-bottom/",
      "/is-vertically-aligned-bottom/",
      "/is-vertically-aligned-top/",
      "/is-vertically-aligned-center/",
      "/no-border-radius/",
      "/is-style-outline/"
    ],
    'bulma' => [
      "has-background-$1",
      "has-text-$1",
      "column",
      "image",
      "is-fullwidth",
      "is-vcentered",
      "is-align-items-flex-end",
      "is-align-self-flex-end",
      "is-align-self-flex-start",
      "is-align-self-center",
      "is-radiusless",
      "is-outlined"
    ]
  ];
  private $_blockRenderOverrides = [
    'core/button'   => "Theme\Parent\Framework\Bulma::renderCoreButton",
    'core/buttons'  => "Theme\Parent\Framework\Bulma::renderCoreButtons"
  ];
  
  private $_blockRenderAdjustments = [
    [
      'name'        => "renderAccordion",
      'conditions'  => [
        'blockName'   => "core/group",
        'attrs'       => [
          'className'   => "accordion"
        ]
      ]
    ]
  ];
  private $_heroSettingsMetabox = [
    'id'        => "hero_settings",
    'context'   => "side",
    'title'     => "Hero Settings",
    'priority'  => "high",
    'fields'    => [
      [
        'id'      => "hero_style",
        'type'    => "select",
        'options' => [
          'label'       => "Color Style",
          'default'     => "",
          'desc'        => "Select a style for the banner at the top of a page.  Setting a Featured Image on this page will replace the styling that you select here.",
          'options_cb'  => "Theme\Parent\Framework\Bulma::styleOptions"
        ]
      ]
    ],
    'nonce'     => [
      'name'   => "loc_theme_hero_settings",
      'action' => "loc_theme_save_hero_settings"
    ]
  ];

  public static $styles = [
    "black",
    "danger",
    "dark",
    "ghost",
    "info",
    "light",
    "link",
    "primary",
    "success",
    "text",
    "secondary",
    "warning",
    "white",
  ];

  function __construct() {

    self::$_instance = $this;

    add_filter( 'get_search_form', [ $this, "searchWidget" ], 5, 2 );
    add_filter( 'register_block_type_args', [ $this, "filterBlockArgs" ], 10, 2 );
    add_filter( 'render_block', [ $this, "bulmaRenderAdjustments" ], 7, 2 );
    add_filter( 'default_theme_metaboxes', [ $this, "bulmaMetaboxDefaults" ] );

  }

  public function searchWidget( $form, $args ) {

    ob_start();
    Utility::getScopedTemplatePart( "template-parts/Bulma/form/form", "search", $args );
    $form = ob_get_clean();

    return $form;

  }

  public function bulmaRenderAdjustments( $content, $block ) {

    $content = $this->coreClassAdjustments( $content );

    foreach( $this->_blockRenderAdjustments as $adjustment ) {

      if( Utility::deepCompare( $adjustment['conditions'], $block ) ) {

        $method   = $adjustment['name'];
        $content  = $this->$method( $content, $block );

      }

    }
    
    return $content;

  }

  public function coreClassAdjustments( $content ) {

    $expressions  = apply_filters( 'block_class_replacements', $this->_coreClasses );
    $content      = preg_replace( $expressions['wp'], $expressions['bulma'], $content );

    return $content;

  }

  public function filterBlockArgs( $args, $name ) {

    $overrides = apply_filters( 'render_block_overrides', $this->_blockRenderOverrides );

    if( array_key_exists( $name, $overrides ) && is_callable( $overrides[$name] ) )
      $args['render_callback'] = $overrides[$name];

    return $args;

  }

  public function bulmaMetaboxDefaults( $metaboxes ) {

    $this->_heroSettingsMetabox['fields'][0]['options']['default'] = self::getDefaultStyle( "hero" );

    if( isset( $_GET['post'] ) && isset( $_GET['action'] ) ) {

      if( "edit" === $_GET['action'] && $_GET['post'] !== get_option( 'page_on_front' ) )
        $metaboxes[] = $this->_heroSettingsMetabox;

    }

    return $metaboxes;

  }

  public static function renderCoreButton( $atts, $content ) {

    $wpSearch    = [
      "/<div[^>]+>(.*)<\/div>/",
      "/wp-block-button__link/"
    ];
    $bulmaReplace = [
      '${1}',
      "button %s"
    ];

    $btnClasses   = isset( $atts['className'] ) ? explode( " ", $atts['className'] ) : [];
    $defaultStyle = self::getDefaultStyle( "button" );
    $userStyles   = self::getStyle( $btnClasses );

    if( isset( $atts['backgroundColor'] ) ) {

      $btnClasses[]   = str_replace( self::getStyle( $defaultStyle ), "is-" . $atts['backgroundColor'], $defaultStyle );
      $wpSearch[]     = "/has-{$atts['backgroundColor']}-background-color/";
      $bulmaReplace[] = "";

    } elseif( ! $userStyles || empty( $userStyles ) )
      $btnClasses[] = $defaultStyle;

    $content = preg_replace( $wpSearch, $bulmaReplace, $content );

    return apply_filters( 'render_block', sprintf( $content, implode( " ", $btnClasses ) ), $atts );

  }

  public static function renderCoreButtons( $atts, $content ) {

    $lines      = array_filter( explode("\n", $content) );
    $extraClass = isset( $atts['className'] ) ? $atts['className'] : "";
    $alignClass = "";

    if( isset( $atts['contentJustification'] ) ) {
      switch( $atts['contentJustification'] ) {

        case "left":
          $alignClass = "is-left";
          break;
        case "right":
          $alignClass = "is-right";
          break;
        default:
          $alignClass = "is-centered";

      }
    } else {

      $alignClass = "is-left";

    }

    array_shift($lines);
    array_pop($lines);

    $class = implode( " ", [ "buttons", $extraClass, $alignClass ] );

    return apply_filters( 'render_block', sprintf( '<p class="%s">%s</p>', $class, implode( "\n", $lines ) ), $atts );

  }

  public static function renderAccordion( $content, $block ) {

    ob_start();

    Utility::getScopedTemplatePart(
      "template-parts/Bulma/blocks/accordion",
      "block",
      [
        'class'   => $block['attrs']['className'],
        'blocks'  => $block['innerBlocks']
      ]
    );

    $content = ob_get_clean();

    return $content;

  }

  public static function styleOptions() {

    $options = [
      ''              => "Choose a style",
      'is-primary'    => "Primary",
      'is-secondary'  => "Secondary",
      'is-info'       => "Info",
      'is-dark'       => "Dark",
      'is-light'      => "Light"
    ];

    return apply_filters( 'bulma_style_options', $options );

  }

  public static function sizeOptions() {

    $options = [
      ''          => "Normal",
      'is-small'  => "Small",
      'is-medium' => "Medium",
      'is-large'  => "Large"
    ];

    return apply_filters( 'bulma_size_options', $options );

  }

  public static function getDefaultStyle( $component = null ) {

    $style    = apply_filters( 'default_bulma_style', "is-primary" );
    $defaults = [
      'hero'          => apply_filters( 'default_hero_style', "is-secondary" ),
      'hero-full'     => apply_filters( 'default_full_hero_style', "is-dark" ),
      'button'        => apply_filters( 'default_button_style', "is-primary" ),
      'nav'           => apply_filters( 'default_nav_style', "is-light" ),
      'nav-footer'    => apply_filters( 'default_footer_nav_style', "is-light" ),
      'notification'  => apply_filters( 'default_notification_style', "is-info" )
    ];
    $styles = apply_filters( 'bulma_component_styles', $defaults, $component );

    if( null !== $component && isset( $styles[$component] ) )
      $style = $defaults[$component];

    return $style;

  }

  /**
   * Get Style
   * Receive a string of classes to check against a full list of Bulma styles
   *
   * @param $classes (str|array) the classes to check
   *
   * @return (bool|array) first match if found, false if not.
   */
  public static function getStyle( $classes ) {

    $exp = "/^(is-)?(" . implode( "|", self::$styles ) . ")$/";

    if( is_string( $classes ) )
      $classes = explode( " ", trim( $classes ) );

    $matches = [];

    foreach( $classes as $class ) {

      if( preg_match( $exp, strtolower( $class ) ) )
        $matches[] = preg_replace( $exp, "is-$2", strtolower($class) );

    }

    if( ! empty( $matches ) )
      $style = $matches[0];
    else
      $style = false;

    return $style;

  }

}

global $Bulma;
$Bulma = new Bulma();

namespace Theme\Parent\Framework\Bulma;

use Walker_Nav_Menu, Theme\Parent\Utility;

/**
 * Walker_Nav_Menu Class for Bulma markup
 * Author: R. Simard
 * Author URL: https://simard.solutions
 * Original Author: Tracy Ridge
 * Version: 2.0
 * Version notes: this version is basically an entire refactor from original concepts;
 *    consolidated lots of moot conditionals, bad string incrementation and redundancies.
 *
 * @see Walker_Nav_Menu & Walker
 * @link https://developer.wordpress.org/reference/classes/walker_nav_menu/
 * @link https://bulma.io/documentation/components/navbar/
 */

class Walker extends Walker_Nav_Menu {

  /**
   * [private stored in start_el and used in start_lvl to pass custom classes on ]
   * @var [array]
   * @see Walker::_parseClasses
   */
  private $_rightClass = false;
  /**
   * Stores parsed item classes that will be used on the navbar-item level.
   * @var array
   * @see Walker::_parseClasses
   */
  private $_itemClasses;
  /**
   * Stores parsed item classes that will be used on Font Awesome icons.
   * @var array
   * @see Walker::_parseClasses
   */
  private $_iconClasses;
  /**
   * Store parsed $args that are used repeatedly from the menu call.
   * @var array
   */
  private $_args;

  /**
   * Get Args
   *
   * If the args have not been set, store them.  If they have already been set, retrieve them.
   *
   * @param WP_Post $item
   * @param array $args
   *
   * @return array
   */
  private function _getArgs( $args ) {

    if( null === $this->_args ) {

      $defaults = [
        'menu'            => '',
        'container'       => false,
        'container_class' => '',
        'container_id'    => '',
        'menu_class'      => 'navbar-start',
        'menu_id'         => 'navbar-nav-items',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<div id="%1$s" class="%2$s">%3$s</div>',
        'item_spacing'    => 'preserve',
        'depth'           => 3,
        'walker'          => '',
        'theme_location'  => '',
      ];

      $this->_args = wp_parse_args( $args, apply_filters( 'navbar_default_args', $defaults ) );

    }

    return $this->_args;

  }

  /**
   * Parse Classes
   * 
   * Parse the item classes from WP_Post menu item: identify which ones are for icons,
   * which are for the navbar item, and ignore some excessive ones.
   * Data is stored on private properties $_rightClass, $_itemClasses and $_iconClasses.
   *
   * @param WP_Post $item
   * @param array $args
   * @param bool $children
   *
   * @return void
   */
  private function _parseClasses( $item, $args, $children ) {

    $this->_itemClasses = [];
    $this->_iconClasses = [];

    $classes      = empty( $item->classes ) ? [] : $item->classes;
    $hasDropdown  = apply_filters( 'navbar_item_has_children_link_classes', [ "dropdown", "navbar-link" ], $item, $args );

    if( ! empty( $classes ) ) {

      $this->_rightClass = false;

      foreach( $item->classes as $class ) {

        if( "current-menu-item" === $class )
          $this->_itemClasses[] = "is-active";
        elseif( 'fa-show-title' == $class )
          $this->_iconClasses[] = "showing-title";
        elseif( "is-right" === $class )
          $this->_rightClass = true;
        elseif( strstr( $class, "object" ) )
          continue;
        elseif( in_array( $class, [ "current_page_parent", "current_page_ancestor" ] ) )
          continue;
        elseif( strstr( $class, "menu-item" ) )
          $this->_itemClasses[] = str_replace( "menu-item", "navbar-item", $class );
        elseif( preg_match( Utility::$faExp, $class ) )
          $this->_iconClasses[] = $class;
        else
          $this->_itemClasses[] = $class;
      }

    }

    $this->_itemClasses[] = 'navbar-item-' . $item->ID;

    if( true === $children )
      $this->_itemClasses = array_merge( $this->_itemClasses, $hasDropdown );
    else
      $this->_itemClasses[] = "no-dropdown";

    if( preg_match( "/(http|www)/", $item->url ) && ! strstr( $item->url, site_url() ) )
      $this->_itemClasses[] = "external";

  }

  private function _get_item_attributes( $item, $args ): string {

    $atts = [];
    if ( empty( $item->attr_title ) )
      $atts['title'] = ! empty( $item->title ) ? strip_tags( $item->title ) : '';

    $atts['target'] = ! empty( $item->target ) ? $item->target : '';
    $atts['rel']    = ! empty( $item->xfn ) ? $item->xfn : '';
    $atts['href']   = ! empty( $item->url ) ? $item->url : '';
    $atts           = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );
    $attributes     = '';

    foreach ( $atts as $attr => $value ) {

      if ( ! empty( $value ) ) {

        $value      = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
        $attributes .= ' ' . $attr . '="' . $value . '"';

      }

    }

    return $attributes;

  }

  /**
   * Start Level.
   *
   * @see Walker::start_lvl()
   * @since 1.0.0
   *
   * @access public
   *
   * @param mixed $output Passed by reference. Used to append additional content.
   * @param int $depth (default: 0) Depth of page. Used for padding.
   * @param array $args (default: array()) Arguments.
   *
   * @return void
   */
  public function start_lvl( &$output, $depth = 0, $args = [] ) {

    $dropdownClasses  = [ "navbar-dropdown" ];
    $indent           = str_repeat( "\t", $depth );
    $output          .= $indent . "<div class=\"" . implode( " ", apply_filters( 'navbar_dropdown_classes', $dropdownClasses ) );

    if( false !== $this->_rightClass )
      $output .= " is-right ";

    $output .= "\">";

  }

  public function end_lvl( &$output, $depth = 0, $args = [] ) {

    $indent   = str_repeat( "\t", $depth );
    $output  .= "$indent</div>\n";

  }

  /**
   * Start El.
   *
   * @see Walker::start_el()
   * @since 1.0.0
   *
   * @access public
   *
   * @param mixed $output Passed by reference. Used to append additional content.
   * @param mixed $item Menu item data object.
   * @param int $depth (default: 0) Depth of menu item. Used for padding.
   * @param array $args (default: array()) Arguments.
   * @param int $id (default: 0) Menu item ID.
   *
   * @return void
   */
  public function start_el( &$output, $item, $depth = 0, $args = [], $id = 0 ) {

    $hasChildren = $args->has_children;
    $args = $this->_getArgs( $args );
    
    if ( ! in_array( 'navbar-divider', $item->classes ) ) {

      $this->_parseClasses( $item, $args, $hasChildren );

      $title        = apply_filters( 'the_title', $item->title, $item->ID );
      $attributes   = $this->_get_item_attributes( $item, $args );
      $indent       = 0 !== $depth ? str_repeat( "\t", $depth ) : '';
      $hasDropdown  = [ "navbar-item", "has-dropdown", "is-hoverable" ];
      $itemClasses  = implode( " ", apply_filters( 'nav_menu_css_class', array_filter( $this->_itemClasses ), $item, $args ) );

      // Begin output
      $item_output  = $args['before'];

      if( true === $hasChildren) 
        $item_output .= $indent . sprintf( '<div class="%s">', implode( " ", apply_filters( 'navbar_item_has_children_classes', $hasDropdown, $item, $args ) ) ) . "\n";

      $item_output .= sprintf( '<a class="%s" %s>', esc_attr( $itemClasses ), $attributes );

      if( ! empty( $this->_iconClasses ) )
        $item_output .= sprintf( '<i class="%s"></i>', implode( " ", $this->_iconClasses ) );

      // If title is hidden, use screen reader text
      if( ! empty( $this->_iconClasses ) && ! in_array( "showing-title", $this->_iconClasses ) )
        $title = sprintf( '<span class="screen-reader-text">%s</span>', $title );

      $item_output .= $args['link_before'] . trim( $title ) . $args['link_after'];
      $item_output .= $args['after'];

      if( true === $hasChildren ) 
        $item_output .= "</a>";

    } else {

      $item_output = '<hr class="navbar-divider" aria-hidden="true">';

    }

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

  }

  public function end_el( &$output, $item, $depth = 0, $args = [] ) {

    if ( ! in_array( "menu-item-has-children", $item->classes ) )
      $item_output = "</a>\n";
    else
      $item_output = "</div>\n";

    $output .= apply_filters( 'walker_nav_menu_end_el', $item_output, $item, $depth, $args );

  }

  /**
   * Traverse elements to create list from elements.
   *
   * Display one element if the element doesn't have any children otherwise,
   * display the element and its children. Will only traverse up to the max
   * depth and no ignore elements under that depth.
   *
   * This method shouldn't be called directly, use the walk() method instead.
   *
   * @see Walker::start_el()
   * @since 1.0.0
   *
   * @access public
   *
   * @param mixed $element Data object.
   * @param mixed $children_elements List of elements to continue traversing.
   * @param mixed $max_depth Max depth to traverse.
   * @param mixed $depth Depth of current element.
   * @param mixed $args Arguments.
   * @param mixed $output Passed by reference. Used to append additional content.
   *
   * @return null empty on failure with no changes to parameters.
   */
  public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {

    if ( ! $element )
      return;

    $id_field = $this->db_fields['id'];
    
    if ( is_object( $args[0] ) )
      $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );

    parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );

  }

  /**
   * Menu Fallback
   * =============
   * If this function is assigned to the wp_nav_menu's fallback_cb variable
   * and a menu has not been assigned to the theme location in the WordPress
   * menu manager the function with display nothing to a non-logged in user,
   * and will add a link to the WordPress menu manager if logged in as an admin.
   *
   * @param array $args passed from the wp_nav_menu function.
   */
  public static function fallback( $args ) {

    $args = $this->_getArgs( $args );

    if ( current_user_can( 'edit_theme_options' ) ) {
      /* Get Arguments. */
      $container       = $args['container'];
      $container_id    = $args['container_id'];
      $container_class = $args['container_class'];
      $menu_class      = $args['menu_class'];
      $menu_id         = $args['menu_id'];

      if ( $container ) {

        echo '<' . esc_attr( $container );

        if ( $container_id )
          echo ' id="' . esc_attr( $container_id ) . '"';

        if ( $container_class )
          echo ' class="' . sanitize_html_class( $container_class ) . '"';

        echo '>';
      }

      echo '<div';

      if ( $menu_id )
        echo ' id="' . esc_attr( $menu_id ) . '"';

      if ( $menu_class )
        echo ' class="' . esc_attr( $menu_class ) . '"';

      echo '>';
      echo '<a class="navbar-item" href="' . esc_url( admin_url( 'nav-menus.php' ) ) . '" title="">' . esc_attr( 'Add a menu', 'stylesheet_theme' ) . '</a>';
      echo '</div>';

      if ( $container )
        echo '</' . esc_attr( $container ) . '>';

    }

  }

}

class MegaWalker extends Walker_Nav_Menu {

  /**
   * [private stored in start_el and used in start_lvl to pass custom classes on ]
   * @var [array]
   * @see Walker::_parseClasses
   */
  private $_rightClass = false;
  /**
   * Stores parsed item classes that will be used on the navbar-item level.
   * @var array
   * @see Walker::_parseClasses
   */
  private $_itemClasses;
  /**
   * Stores parsed item classes that will be used on Font Awesome icons.
   * @var array
   * @see Walker::_parseClasses
   */
  private $_iconClasses;
  /**
   * Store parsed $args that are used repeatedly from the menu call.
   * @var array
   */
  private $_args;

  /**
   * Get Args
   *
   * If the args have not been set, store them.  If they have already been set, retrieve them.
   *
   * @param WP_Post $item
   * @param array $args
   *
   * @return array
   */
  private function _getArgs( $args ) {

    if( null === $this->_args ) {

      $defaults = [
        'menu'            => '',
        'container'       => false,
        'container_class' => '',
        'container_id'    => '',
        'menu_class'      => 'navbar-start',
        'menu_id'         => 'navbar-meganav-items',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<div id="%1$s" class="%2$s">%3$s</div>',
        'item_spacing'    => 'preserve',
        'depth'           => 3,
        'walker'          => '',
        'theme_location'  => '',
      ];

      $this->_args = wp_parse_args( $args, apply_filters( 'navbar_default_args', $defaults ) );

    }

    return $this->_args;

  }

  /**
   * Parse Classes
   * 
   * Parse the item classes from WP_Post menu item: identify which ones are for icons,
   * which are for the navbar item, and ignore some excessive ones.
   * Data is stored on private properties $_rightClass, $_itemClasses and $_iconClasses.
   *
   * @param WP_Post $item
   * @param array $args
   * @param bool $children
   *
   * @return void
   */
  private function _parseClasses( $item, $args, $children ) {

    $this->_itemClasses = [];
    $this->_iconClasses = [];

    $classes      = empty( $item->classes ) ? [] : $item->classes;
    $hasDropdown  = apply_filters( 'navbar_item_has_children_link_classes', [ "dropdown", "navbar-link" ], $item, $args );

    if( ! empty( $classes ) ) {

      $this->_rightClass = false;

      foreach( $item->classes as $class ) {

        if( "current-menu-item" === $class )
          $this->_itemClasses[] = "is-active";
        elseif( 'fa-show-title' == $class )
          $this->_iconClasses[] = "showing-title";
        elseif( "is-right" === $class )
          $this->_rightClass = true;
        elseif( strstr( $class, "object" ) )
          continue;
        elseif( in_array( $class, [ "current_page_parent", "current_page_ancestor" ] ) )
          continue;
        elseif( strstr( $class, "menu-item" ) )
          $this->_itemClasses[] = str_replace( "menu-item", "navbar-item", $class );
        elseif( preg_match( Utility::$faExp, $class ) )
          $this->_iconClasses[] = $class;
        else
          $this->_itemClasses[] = $class;
      }

    }

    $this->_itemClasses[] = 'navbar-item-' . $item->ID;

    if( true === $children )
      $this->_itemClasses = array_merge( $this->_itemClasses, $hasDropdown );
    else
      $this->_itemClasses[] = "no-dropdown";

    if( preg_match( "/(http|www)/", $item->url ) && ! strstr( $item->url, site_url() ) )
      $this->_itemClasses[] = "external";

  }

  private function _get_item_attributes( $item, $args ): string {

    $atts = [];
    if ( empty( $item->attr_title ) )
      $atts['title'] = ! empty( $item->title ) ? strip_tags( $item->title ) : '';

    $atts['target'] = ! empty( $item->target ) ? $item->target : '';
    $atts['rel']    = ! empty( $item->xfn ) ? $item->xfn : '';
    $atts['href']   = ! empty( $item->url ) ? $item->url : '';
    $atts           = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );
    $attributes     = '';

    foreach ( $atts as $attr => $value ) {

      if ( ! empty( $value ) ) {

        $value      = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
        $attributes .= ' ' . $attr . '="' . $value . '"';

      }

    }

    return $attributes;

  }

  /**
   * Start Level.
   *
   * @see Walker::start_lvl()
   * @since 1.0.0
   *
   * @access public
   *
   * @param mixed $output Passed by reference. Used to append additional content.
   * @param int $depth (default: 0) Depth of page. Used for padding.
   * @param array $args (default: array()) Arguments.
   *
   * @return void
   */
  public function start_lvl( &$output, $depth = 0, $args = [] ) {

    $dropdownClasses  = [ "navbar-dropdown" ];
    $indent           = str_repeat( "\t", $depth );
    $output          .= $indent . "<div class=\"" . implode( " ", apply_filters( 'navbar_dropdown_classes', $dropdownClasses ) );

    if( false !== $this->_rightClass )
      $output .= " is-right ";

    $output .= "\">";

  }

  public function end_lvl( &$output, $depth = 0, $args = [] ) {

    $indent   = str_repeat( "\t", $depth );
    $output  .= "$indent</div>\n";

  }

  /**
   * Start El.
   *
   * @see Walker::start_el()
   * @since 1.0.0
   *
   * @access public
   *
   * @param mixed $output Passed by reference. Used to append additional content.
   * @param mixed $item Menu item data object.
   * @param int $depth (default: 0) Depth of menu item. Used for padding.
   * @param array $args (default: array()) Arguments.
   * @param int $id (default: 0) Menu item ID.
   *
   * @return void
   */
  public function start_el( &$output, $item, $depth = 0, $args = [], $id = 0 ) {

    $hasChildren = $args->has_children;
    $args = $this->_getArgs( $args );
    
    if ( ! in_array( 'navbar-divider', $item->classes ) ) {

      $this->_parseClasses( $item, $args, $hasChildren );

      $title        = apply_filters( 'the_title', $item->title, $item->ID );
      $attributes   = $this->_get_item_attributes( $item, $args );
      $indent       = 0 !== $depth ? str_repeat( "\t", $depth ) : '';
      $hasDropdown  = [ "navbar-item", "has-dropdown", "is-hoverable" ];
      $itemClasses  = implode( " ", apply_filters( 'nav_menu_css_class', array_filter( $this->_itemClasses ), $item, $args ) );

      // Begin output
      $item_output  = $args['before'];

      if( true === $hasChildren )
        $item_output .= $indent . sprintf( '<div class="%s">', implode( " ", apply_filters( 'navbar_item_has_children_classes', $hasDropdown, $item, $args ) ) ) . "\n";

      $item_output .= sprintf( '<a class="%s" %s>', esc_attr( $itemClasses ), $attributes );

      if( ! empty( $this->_iconClasses ) )
        $item_output .= sprintf( '<i class="%s"></i>', implode( " ", $this->_iconClasses ) );

      // If title is hidden, use screen reader text
      if( ! empty( $this->_iconClasses ) && ! in_array( "showing-title", $this->_iconClasses ) )
        $title = sprintf( '<span class="screen-reader-text">%s</span>', $title );

      $item_output .= $args['link_before'] . trim( $title ) . $args['link_after'];
      $item_output .= $args['after'];

      if( true === $hasChildren )
        $item_output .= "</a>";

    } else {

      $item_output = '<hr class="navbar-divider" aria-hidden="true">';

    }

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

  }

  public function end_el( &$output, $item, $depth = 0, $args = [] ) {

    if ( ! in_array( "menu-item-has-children", $item->classes ) )
      $item_output = "</a>\n";
    else
      $item_output = "</div>\n";

    $output .= apply_filters( 'walker_nav_menu_end_el', $item_output, $item, $depth, $args );

  }

  /**
   * Traverse elements to create list from elements.
   *
   * Display one element if the element doesn't have any children otherwise,
   * display the element and its children. Will only traverse up to the max
   * depth and no ignore elements under that depth.
   *
   * This method shouldn't be called directly, use the walk() method instead.
   *
   * @see Walker::start_el()
   * @since 1.0.0
   *
   * @access public
   *
   * @param mixed $element Data object.
   * @param mixed $children_elements List of elements to continue traversing.
   * @param mixed $max_depth Max depth to traverse.
   * @param mixed $depth Depth of current element.
   * @param mixed $args Arguments.
   * @param mixed $output Passed by reference. Used to append additional content.
   *
   * @return null empty on failure with no changes to parameters.
   */
  public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {

    if ( ! $element )
      return;

    $id_field = $this->db_fields['id'];
    
    if ( is_object( $args[0] ) )
      $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );

    parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );

  }

  /**
   * Menu Fallback
   * =============
   * If this function is assigned to the wp_nav_menu's fallback_cb variable
   * and a menu has not been assigned to the theme location in the WordPress
   * menu manager the function with display nothing to a non-logged in user,
   * and will add a link to the WordPress menu manager if logged in as an admin.
   *
   * @param array $args passed from the wp_nav_menu function.
   */
  public static function fallback( $args ) {

    $args = $this->_getArgs( $args );

    if ( current_user_can( 'edit_theme_options' ) ) {
      /* Get Arguments. */
      $container       = $args['container'];
      $container_id    = $args['container_id'];
      $container_class = $args['container_class'];
      $menu_class      = $args['menu_class'];
      $menu_id         = $args['menu_id'];

      if ( $container ) {

        echo '<' . esc_attr( $container );

        if ( $container_id )
          echo ' id="' . esc_attr( $container_id ) . '"';

        if ( $container_class )
          echo ' class="' . sanitize_html_class( $container_class ) . '"';

        echo '>';
      }

      echo '<div';

      if ( $menu_id )
        echo ' id="' . esc_attr( $menu_id ) . '"';

      if ( $menu_class )
        echo ' class="' . esc_attr( $menu_class ) . '"';

      echo '>';
      echo '<a class="navbar-item" href="' . esc_url( admin_url( 'nav-menus.php' ) ) . '" title="">' . esc_attr( 'Add a menu', 'stylesheet_theme' ) . '</a>';
      echo '</div>';

      if ( $container )
        echo '</' . esc_attr( $container ) . '>';

    }

  }

}

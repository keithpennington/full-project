<?php

namespace Theme\Parent;

class Defaults {

  use Traits\PostMetaFields, Traits\Instance;

  public $metaboxes = [
    [
      'id'        => "seo_props",
      'context'   => "advanced",
      'title'     => "SEO Settings",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "sitemap_exclusion",
          'type'    => "checkbox",
          'options' => [
            'label'     => "Exclude from Sitemap",
            'desc'      => "Checking this box will prevent search engines from reading and indexing this page."
          ]
        ],
        [
          'id'      =>  "contact_schema",
          'type'    =>  "checkbox",
          'options' =>  [
            'label'      => "Add Contact Page Schema",
            'desc'       => "Checking this box will automatically apply the \"ContactPage\" schema object to this page."
          ]
        ],
        [
          'id'      => "og_title",
          'type'    => "input",
          'options' => [
            'label'   => "Open Graph Title",
            'desc'    => "Write a custom open graph title that will be different from this post's title and used when third parties embed a preview of this page. If left blank, the default page title will be used.",
            'render'  => '<input id="%s" name="%s" type="text" class="form-control" value="%s" />'
          ]
        ],
        [
          'id'      => "meta_title",
          'type'    => "input",
          'options' => [
            'label'   => "Meta Title",
            'desc'    => "Write a custom meta title that will be different from this post's title and used when bots crawl this site. If left blank, the default page title will be used.",
            'render'  => '<input id="%s" name="%s" type="text" class="form-control" value="%s" />'
          ]
        ]
      ],
      'nonce' => [
        'name'    => "loc_theme_seo_properties",
        'action'  => "loc_theme_save_seo_properties"
      ]
    ]
  ];

  public $postTypes = [
    'post' => "Post",
    'page' => "Page"
  ];

  function __construct() {

    self::$_instance = $this;

    // Actions
    add_action( 'init', [ $this, "postTypeSupport" ] );
    add_action( 'add_meta_boxes', [ $this, "addMetaboxes" ] );
    add_action( 'save_post', [ $this, "saveCustomFields" ], 10, 3 );
    add_action( 'save_post', [ $this, "overrideAuthor" ], 10, 3 );
    add_action( 'wp_footer', [ $this, "bannerDisplay" ], 10 );
    add_action( 'activated_plugin', [ $this, "gfActivated" ], 10, 2 );
    add_action( 'add_meta_boxes', [ $this, 'addFaqMetaBox' ]);
    add_action( 'save_post' , [ $this, 'saveFaqMetaFields' ], 10, 3 );



    // Filters
    add_filter( 'render_block', [ $this, "defaultBlockRenderFixes" ], 10, 2 );
    add_filter( 'gform_pre_render_4', [ $this, 'gfPopulateJobPostField' ] );
    add_filter( 'gform_pre_validation_4', [ $this, 'gfPopulateJobPostField' ] );
    add_filter( 'gform_pre_submission_filter_4', [ $this, 'gfPopulateJobPostField' ] );
    add_filter( 'gform_admin_pre_render_4', [ $this, 'gfPopulateJobPostField' ] );
    add_filter( 'site_schema', [ $this, 'pageSchema' ], 10, 4 );
    add_filter( 'site_schema', [ $this, 'postSchema' ], 10, 4 );
    add_filter( 'site_schema', [ $this, 'faqSchema' ], 10, 4 );

  }

  public function postTypeSupport() {

    add_post_type_support( "page", "excerpt" );

  }

  public function getPostTypes() {

    return apply_filters( 'default_meta_post_types', $this->postTypes );

  }

  public function getMetaboxes() {

    return apply_filters( 'default_theme_metaboxes', $this->metaboxes );

  }

  public function addMetaboxes() {

    $metaboxes = $this->getMetaboxes();

    if( is_array( $metaboxes ) && ! empty( $metaboxes ) ) {

      foreach( $metaboxes as $box )
        $this->addMetabox( array_keys( $this->getPostTypes() ), $box );

    }

  }

  public function overrideAuthor( $postID, $post, $update ) {

    $override = Utility::getOption( "business_info", "override_author" );
    $owner    = Utility::getOption( "business_info", "owner" );

    if( "revision" !== $post->post_type && in_array( $override, Utility::trueTerms() ) && $owner !== $post->post_author )
      wp_update_post( [ 'ID' => $postID, 'post_author' => $owner ] );

  }

  public function defaultBlockRenderFixes( $content, $block ) {

    // Remove empty paragrph tags from rendering if they have no contents
    if( isset( $block['blockName'] ) && "core/paragraph" == $block['blockName'] && preg_match( "/<p>[\w\W]{0}<\/p>/", $block['innerHTML'] ) )
      $content = "";

    // Restore missing alt text from images rendered in cover block
    if( isset( $block['blockName'] ) && "core/cover" == $block['blockName'] && preg_match( "/alt=\"\"/", $block['innerHTML'] ) )
      $content = $this->_fixMissingAltAttributes( $content, $block );

    return $content;

  }

  private function _fixMissingAltAttributes( $content, $block ) {
    
    $alt      = get_post_meta( $block['attrs']['id'], '_wp_attachment_image_alt', true );
    $pattern  = "/(wp-image-{$block['attrs']['id']}[^>]+) (alt=\"\")/";
    $replace  = sprintf( "$1 alt=\"%s\"", esc_attr( $alt ) );

    $content = preg_replace( $pattern, $replace, $content );

    return $content;

  }

  public function bannerDisplay() {

    $args = [
      'show'            => Utility::getOption( 'banner_settings', 'show' ),
      'message'         => Utility::getOption( 'banner_settings', 'message' ),
      'modal_content'   => Utility::getOption( 'banner_settings', 'modal_content' ),
      'modal_toggle'    => Utility::getOption( 'banner_settings', 'modal_toggle' )
    ];

    if( in_array( $args['show'], Utility::trueTerms() ) )
      Utility::getScopedTemplatePart( 'template-parts/Vanilla/content/content', 'banner', $args );

  }

  public function gfActivated( $plugin, $network_wide ) {
    if ( $plugin === "gravityforms/gravityforms.php" ) {
      $pubKey = update_option( 'rg_gforms_captcha_public_key', '6LffNwkaAAAAAIUM75mhv7sGzY9IzMAn0nS0o5gP' );
      $privateKey = update_option( 'rg_gforms_captcha_private_key', '6LffNwkaAAAAAEdhS4fskb-6BEdm7u3--mmIQn4k' );
    }
  }

  public function gfPopulateJobPostField( $form ) {
  
      foreach ( $form['fields'] as &$field ) {
  
          if ( $field->type != 'select' || $field->id !== 7 ) {
              continue;
          }
  
          $args = [
            'post_type'   =>    'jobs',
            'numberposts' =>    -1
          ];

          $posts = get_posts( $args );
  
          $choices = array();
  
          foreach ( $posts as $post ) {
              $choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
          }
  
          $field->placeholder = 'Select Position';
          $field->choices = $choices;
  
      }
  
      return $form;
  }

  public function pageSchema( $schema, $info, $og, $hours ) {

    global $post;

    $contactSchema = Utility::getPostMetaValue( 'seo_props', 'contact_schema' );

    if ( $post->post_type === 'page' ) {
      array_push( $schema, [
        "@context"            => "https://schema.org",
        "@type"               => ( in_array( $contactSchema, Utility::trueTerms() ) ) ? "ContactPage" : "WebPage",
        "name"                => $og['title'],
        "description"         => get_the_excerpt(),
        "url"                 => get_the_permalink(),
        "primaryImageOfPage"  => [
          "@type"                 =>  "ImageObject",
          "thumbnailUrl"          =>  get_the_post_thumbnail_url(),
          "representativeOfPage"  =>  true
        ],
        "datePublished"       => get_the_date(),
        "dateModified"        => get_the_modified_date()
      ] );
    }

    return $schema;
  }

  public function postSchema( $schema, $info, $og, $hours ) {

    global $post;

    if ( $post->post_type === 'post' ) {
      array_push( $schema, [
        "@context"            => "https://schema.org",
        "@type"               => 'BlogPosting',
        "mainEntityOfPage"    =>  [
          "@type"  => "WebPage",
          "@id"    => get_the_permalink()
        ],
        "headline"            => $og['title'],
        "description"         => get_the_excerpt(),
        "image"               => get_the_post_thumbnail_url(),
        "author"              => [
          "@type"         =>  "Person",
          "name"          =>  get_the_author_meta( 'display_name' ),
          "url"           =>  get_the_author_meta( 'user_url' )
        ],
        "publisher"           =>  [
          "@type"         =>  "Organization",
          "name"          =>  $info['organization'],
          "logo"          =>  [
            "@type"           =>  "ImageObject",
            "url"             =>  $info['logo_url']
          ]
        ],
        "datePublished"       => get_the_date(),
        "dateModified"        => get_the_modified_date()
      ] );
    }

    return $schema;
  }
  
  public function addFaqMetaBox() {
    $post_types = get_post_types();

    foreach( $post_types as $post_type ) {

      $obj = get_post_type_object( $post_type );

      if( ! $obj->exclude_from_search ) {

        add_meta_box(
          'faq_schema_meta_box', // $id
          'FAQ Schema', // $title 
          [ $this, 'showFaqMetaBox' ], // $callback
           $post_type,
          'normal', // $context
          'high' // $priority
        );
      }

      }
  }

public function showFaqMetaBox() {

  global $post;

  $fields = get_post_meta( $post->ID, '_faq_schema', true );

  ?>
  
  <div id="faq-schema-wrapper">
  <?php 
    if( is_array( $fields ) && ! empty( $fields ) ) :
      foreach( $fields as $field ) : 
        ?>
        <div class="faq-item" style="margin-bottom: 15px">
          <h6>FAQ Item</h6>
          <input type="text" name="faq_schema[]" placeholder="Question..." value="<?php echo $field['question']; ?>" />
          <input type="text" name="faq_schema[]" placeholder="Answer..." value="<?php echo $field['answer']; ?>" />
          <a class="faq-removal" href="#" onclick="removeFaqItem(event)" >Remove</a>
        </div>

  <?php
    $count++;
      endforeach;
    else: 
  ?>
    <div class="faq-item" style="margin-bottom: 15px">
      <h6>FAQ Item</h6>
      <input type="text" name="faq_schema[]" placeholder="Question..." />
      <input type="text" name="faq_schema[]" placeholder="Answer..." />
    </div>
    <?php 
      endif;
    ?>
  </div>
  <div id="faq-schema-bottom">
   <!-- TODO Add button to remove FAQ Item -->
    <input type="button" id="faq_add_button" name="faq_add_button" value="+" onclick="addFaqItem()"/> 
  </div>

  <script>
    function addFaqItem() {
      jQuery('<div class="faq-item" style="margin-bottom: 15px"><h6>FAQ Item</h6><input type="text" name="faq_schema[]" placeholder="Question..." /><input type="text" name="faq_schema[]" placeholder="Answer..." /><a class="faq-removal" href="#" onclick="removeFaqItem(event)" >Remove</a></div>')
      .hide().appendTo('#faq-schema-wrapper').fadeIn('slow');
    }

    function removeFaqItem(e) {
      e.preventDefault();
      jQuery(e.currentTarget).parent().fadeOut('slow', function() {
        jQuery(this).remove();
      });
    }
  </script>
  <?php
  }

  public function saveFaqMetaFields( $post_id, $post, $update ) {

    $faqItemsArr = [];

    if( isset( $_POST['faq_schema'] ) && ! empty( $_POST['faq_schema'] ) ) {
      $count = 0;

      for( $i = 0; $i < count( $_POST['faq_schema'] ); $i += 2 ) {
        if( isset( $_POST['faq_schema'][$i] ) && ! empty( $_POST['faq_schema'][$i] ) && 
            isset( $_POST['faq_schema'][$i + 1] ) && ! empty( $_POST['faq_schema'][$i + 1] ) ) {
              $faqItemsArr[$count]['question'] = $_POST['faq_schema'][$i];
              $faqItemsArr[$count]['answer'] = $_POST['faq_schema'][$i + 1];
        } else {
          unset( $_POST['faq_schema'][$i] );
        }

        $count++;

        if( $i % 2 != 0 || $i === 0 ) {
          continue;
        }
      }
    }

    update_post_meta($post_id, '_faq_schema', $faqItemsArr );
  }

  public function faqSchema( $schema, $info, $og, $hours ) {
    global $post;

    $faqs = get_post_meta( $post->ID , '_faq_schema', true );

    if ( is_array( $faqs ) && ! empty( $faqs ) ) {

      array_push( $schema, 
      [
        "@context"            => "https://schema.org",
        "@type"               => 'FAQPage',
        "mainEntity"    =>  [
          $this->getFaqs( $faqs )
        ]
      ]);
      
    }
 
    return $schema;

  }

  private function getFaqs( $faqs ) {
    global $post;

    $fields = [];

    for( $i = 0; count( $faqs ) > $i; $i++ ) {

      $data = [
        "@type"           => "Question",
        "name"            => $faqs[$i]['question'],
        "acceptedAnswer"  => [
          "@type"           =>  "Answer",
          "text"            =>  $faqs[$i]['answer']
        ]
      ];

        array_push( $fields, $data );
    } 

    return $fields;
  }
  
}

new Defaults();

<?php 

namespace Theme\Parent\Woocommerce;

use DOMDocument;
use Theme\Parent\{ Field, Utility };

class WooProduct extends WooDefaults {
  use \Theme\Parent\Traits\PostMetaFields;

  public $metaboxes = [
    [
      'id'        =>  "schema",
      'context'   =>  "advanced",
      'title'     =>  "Product Schema",
      'priority'  =>  "high",
      'screens'   =>  [
        'product'
      ],
      'fields'    =>  [
        [
          'id'      =>  "name",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The name of this product",
            'label'       =>  "Product Name",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Product Name..." />'
          ]
        ],
        [
          'id'      =>  "description",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "A short description of this product",
            'label'       =>  "Description",
            'render'      =>  '<textarea id="%s" name="%s" class="form-control">%s</textarea>'
          ]
        ],
        [
          'id'      =>  "brand",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The brand associated with this product",
            'label'       =>  "Brand",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Brand..." />'
          ]
        ],
        [
          'id'      =>  "category",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The category that this product belongs to",
            'label'       =>  "Category",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Category..." />'
          ]
        ],
        [
          'id'      =>  "color",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The base color of this product",
            'label'       =>  "Color",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Color..." />'
          ]
        ],
        [
          'id'            => "itemCondition",
          'type'          => "select",
          'options'       =>  [
            'label'         => "Product Condition",
            'options_cb'    => "Theme\Parent\Woocommerce\WooProduct::getConditionSelectOptions"
          ]
        ],
        [
          'id'      =>  "logo",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The logo for the brand",
            'label'       =>  "Logo",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Logo URL..." />'
          ]
        ],
        [
          'id'      =>  "image",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The image of this product",
            'label'       =>  "Product Image",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Product Image URL..." />'
          ]
        ],
        [
          'id'      =>  "slogan",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "A slogan or motto associated with this product",
            'label'       =>  "Slogan",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Slogan..." />'
          ]
        ],
        [
          'id'      =>  "manufacturer",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The manufacturer of this product",
            'label'       =>  "Manufacturer",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Manufacturer Name..." />'
          ]
        ],
        [
          'id'      =>  "material",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The material the product is made of (Ex: leather, wood, plastic, or paper)",
            'label'       =>  "Material",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Material..." />'
          ]
        ],
        [
          'id'      =>  "size",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "A standardized size for a product (Ex: small/SM, medium/Med, or large/LG)",
            'label'       =>  "Size",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Size..." />'
          ]
        ],
        [
          'id'      =>  "height",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The height of the item",
            'label'       =>  "Height",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Height..." />'
          ]
        ],
        [
          'id'      =>  "width",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The width of the item",
            'label'       =>  "Width",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Width..." />'
          ]
        ],
        [
          'id'      =>  "depth",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The depth/length of the item",
            'label'       =>  "Depth",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Depth..." />'
          ]
        ],
        [
          'id'      =>  "weight",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The weight of the product",
            'label'       =>  "Weight",
            'render'      =>  '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="Height..." />'
          ]
        ],
        [
          'id'      =>  "url",
          'type'    =>  "input",
          'options' =>  [
            'desc'        =>  "The URL of the product",
            'label'       =>  "URL",
            'render'      => '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="URL..." />'
          ]
        ]
      ],
      'nonce'     =>  [
        'name'        =>  "loc_product_post_schema",
        'action'      =>  "save_loc_product_post_schema"
      ]
    ]
  ];

  function __construct() {
    parent::__construct();

    /**
      * Filters
      */

    add_filter( 'add_meta_boxes_product', [ $this, 'addmetaboxesProduct' ], 10 );
    add_filter( 'woocommerce_structured_data_product', [ $this, "productSchema" ], 10, 2 );

    /**
      * Actions
      */

    if( is_array( $this->metaboxes ) && ! empty( $this->metaboxes ) )
      add_action( 'save_post', [ $this, "saveProductCustomFields" ], 10, 3 );
  }

  public function addmetaboxesProduct( $post ) {
    foreach ( $this->metaboxes as $box ) {
      add_meta_box( $box['id'], $box['title'], [ $this, 'renderMetabox' ], $box['screens'], $box['context'], $box['priority'], [ 'fields' => $box['fields'], 'nonce' => $box['nonce']] );
    }
  }

  public function saveProductCustomFields( $post_id, $post, $update ) {

    $post       = get_post( $post_id );
    $postTypes  = [ 
      'product' =>  "Product" 
    ];
    $metaboxes  = $this->metaboxes;

    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
      return;

    if( empty( $metaboxes ) )
      return;

    if( ! in_array( $post->post_type, $postTypes ) && ! array_key_exists( $post->post_type, $postTypes ) )
      return;

    foreach( $metaboxes as $metabox ) {

      $nonce = isset( $_POST[$metabox['nonce']['name']] ) ? wp_verify_nonce( $_POST[$metabox['nonce']['name']], $metabox['nonce']['action'] ) : false;

      if( ! $nonce )
        continue;

      $post   = isset( $_POST[$metabox['id']] ) ? $_POST[$metabox['id']] : '';
      $values = Field::checkboxValues( $post, $metabox['fields'] );
      
      update_post_meta( $post_id, '_' . $metabox['id'], $values );

    }

  }

  private function getOffers( $product ) {

    $variations = $product->get_available_variations();

    $prices = array_column( $variations, 'display_regular_price' );

    $highPrice = max( $prices );

    $lowPrice = min( $prices );

     return [
        'itemOffered'     => [
          '@type'           =>  "AggregateOffer",
          'url'             =>  get_the_permalink(),
          'priceCurrency'   =>  get_woocommerce_currency(),
          'lowPrice'        =>  $lowPrice,
          'highPrice'       =>  $highPrice,
          'offerCount'      =>  count($variations)
        ]
      ];
  }

  public function productSchema( $schema, $product ) {

    global $post;

    $productSchema = Utility::getPostMetaValue( "schema" );

    $schema = Utility::getSchemaObject( "Product", $productSchema );

    $schema['sku'] = $product->get_sku();


    $schema['offers'] = [
      '@type'           =>  "Offer",
      'price'           =>  $product->get_price(),
      'priceCurrency'   =>  get_woocommerce_currency(),
      ( $product->is_type( 'variable' ) ) ? $this->getOffers( $product ) : ""
    ];

    switch ($product->get_stock_status()) {
      case 'instock':
        $schema['offers']['availability'] = "InStock";
        break;

      case 'outofstock':
        $schema['offers']['availability'] = "OutOfStock";
        break;

      case 'onbackorder':
        $schema['offers']['availability'] = "BackOrder";
        break;

      default:
        $schema['offers']['availability'] = "InStock";
        break;
    }

    $reviews = $product->get_review_count();

    if ( $reviews > 0 ) {
      $schema['aggregateRating']['ratingValue'] = $product->get_average_rating();
      $schema['aggregateRating']['reviewCount'] = $product->get_review_count();
      $schema['aggregateRating']['bestRating']  = 5;
      $schema['aggregateRating']['worstRating'] = 1;
      $schema['aggregateRating']['name'] = 'Product Rating';
    }

    
    return $schema;

  }

  public static function getConditionSelectOptions( $args = [] ) {

    $args     = wp_parse_args( $args, $defaults );
    $options  = [
      ''                      => "Choose condition...",
      'DamagedCondition'      =>  "Damaged",
      'NewCondition'          =>  "New",
      'RefurbishedCondition'  =>  "Refurbished",
      'UsedCondition'         =>  "Used"
    ];

    return $options;

  }

}

new WooProduct();
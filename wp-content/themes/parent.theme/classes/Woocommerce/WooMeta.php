<?php 

namespace Theme\Parent\Woocommerce;

use DOMDocument, Theme\Parent\Utility;

class WooMeta extends WooDefaults {

  /**
   * Core Woocommerce Pages Meta Fields
   */

  private static $_pageMeta = [
    'title',
    'excerpt',
    'permalink',
    'post_thumbnail'
  ];

  function __construct() {
    self::$_instance = $this;
    $this->_setPageMetaDefaults();

    add_filter( 'open_graph', [ $this, 'wooShopOgMeta' ], 10 );
    add_filter( 'meta_title', [ $this, 'wooShopMetaTitle' ], 10 );
    add_filter( 'meta_description', [ $this, 'wooShopMetaDescription' ], 10 );

  }

  private function _setPageMetaDefaults() {

    foreach ($this->_pages['name'] as $key => $name) {

      if( array_key_exists( $key, Self::$_pageIds ) ) {

        foreach( Self::$_pageMeta as $meta ) {

          $func = 'get_the_' . $meta;

          $this->_pages['meta'][$key][$meta] = $func( Self::$_pageIds[$key] );

        }

      }

    }

  }

  public function getPageMeta() {

    return apply_filters( 'woo_page_meta', $this->_pages['meta'], $this->_pages['name'], Self::$_pageIds );
  }

  public function wooShopOgMeta( $og ) {

    if ( is_shop() ) {
      $og['title'] = ( ! empty(Utility::getPostMetaValue( 'seo_props', 'og_title', Self::$_pageIds['shop'] ) ) ) ? Utility::getPostMetaValue( 'seo_props', 'og_title', Self::$_pageIds['shop'] ) : get_the_title( Self::$_pageIds['shop'] );
      $og['description'] = get_the_excerpt( Self::$_pageIds['shop'] );
      $og['url'] = get_the_permalink( Self::$_pageIds['shop'] );
      
      $og['image'] = ( has_post_thumbnail( Self::$_pageIds['shop'] ) ) ? get_the_post_thumbnail_url( Self::$_pageIds['shop'], 'full' ) : Utility::getOption( 'business_info', 'logo_url' );

    }

    return $og;
  }

  public function wooShopMetaTitle( $title ) {

    if( is_shop() ) {
      $title = ( ! empty( Utility::getPostMetaValue( 'seo_props', 'meta_title', Self::$_pageIds['shop'] ) ) ) ? Utility::getPostMetaValue( 'seo_props', 'meta_title', Self::$_pageIds['shop']  ) : get_the_title( Self::$_pageIds['shop'] );
    }

    return $title;
  }

  public function wooShopMetaDescription( $description ) {

    if( is_shop() ) {
      $description = ( has_excerpt( Self::$_pageIds['shop'] ) ) ? get_the_excerpt( Self::$_pageIds['shop'] ) : get_bloginfo( 'description' );
    }

    return $description;

  }

}

new WooMeta();


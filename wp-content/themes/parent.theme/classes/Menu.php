<?php

namespace Theme\Parent;

class Menu {

  use Traits\Reference;

  public $slug;
  public $name;

  public static $menuID;
  
  function __construct() {

    add_action( 'init', [ $this, "register" ], 8 );
    
    $this->_setup();
    $this->_themeReference( "menus" );

  }

  protected function _setup() {

    add_action( 'init', [ $this, "setMenuID" ] );
    
    if( method_exists( $this, "defaultArgs" ) )
      add_filter( 'wp_nav_menu_args', [ $this, "defaultArgs" ] );

  }

  public function setMenuID() {

    $menus = get_nav_menu_locations();
    if( ! empty( $menus ) && isset( $menus[$this->slug] ) )
      self::$menuID = $menus[$this->slug];

  }

  public function register() {

    register_nav_menus( [
      $this->slug => $this->name
    ] );

  }

}

<?php

/**
 * Search template file used to render search queries
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

$search = get_search_query();
$activeTab  = get_query_var( "type", "post" );

$queryArgs = [
  's'               =>  $search,
  'post_type'       =>  get_query_var('type', 'post'),
  'paged'           =>  get_query_var('page', 1),
  'posts_per_page'  =>  10
];

$the_query = new WP_Query( $queryArgs );

$pagination = [
  'base'    =>  get_home_url() . '%_%',
  'format'  =>  '?page=%#%',
  'prev_next'   => true,
  'prev_text'   => 'Prev',
  'next_text'   => 'Next',
  'show_all'    => false,
  'end_size'    => 1,
  'mid_size'    => 3,
  'total'       => $the_query->max_num_pages,
  'current'     => max( 1, get_query_var('page') )
];

$sidebar    = apply_filters(
  'sidebar',
  false,
  [
    'location'  => "search",
    'object'    => $search
  ]
);
$postTypes  = Theme\Parent\Defaults::instance()->getPostTypes();

get_header();

Utility::getScopedTemplatePart(
  "template-parts/hero/hero",
  "search",
  [
    'title'       => "Search Results",
    'subtitle'    => "Showing results for \"{$search}\"",
    'post_types'  => $postTypes,
    'active_tab'  => $activeTab
  ]
) ?>
<main id="main-content" role="main">
  <section class="section">
    <div class="container content">
      <div class="feed-container category-archives-feed-container list-layout">
      <?php 
        if($the_query->have_posts()) :
          while($the_query->have_posts()) :
            $the_query->the_post();

            $dateFormat = get_option( 'date_format', "M j, Y" );
            $timeFormat = get_option( 'time_format', "h:i A" );
            $datetime   = get_the_date( "l m-d-Y " . $timeFormat, $args['id'] );
            $date       = get_the_date( $dateFormat, $args['id'] );

            ?>
              <article class="media">
        <figure class="media-left">
          <?php if( has_post_thumbnail() ) { ?>
          <p class="image is-64x64">
            <a href="<?php the_permalink() ?>">
              <?php echo get_the_post_thumbnail() ?>
            </a>
          </p>
          <?php } ?>
        </figure>
        <div class="media-content">
          <div class="content">
            <a href="<?php the_permalink() ?>">
              <h4 class="title is-4">
                <?php

                the_title();
                echo sprintf(
                  '<small class="%s" title="%s">%s%s</small>',
                  "ml-2 is-size-6 is-bold has-text-weight-normal",
                  $datetime,
                  '<span class="screen-reader-text">' . __("Published on") . ' </span> ',
                  $date
                );
                
                ?>
              </h4>
            </a>
            <?php the_excerpt() ?>
          </div>
          <nav class="level is-mobile">
            <div class="level-left">
              <a class="ml-1 level-item read-more" href="<?php the_permalink() ?>">
                Read more
                <span class="screen-reader-text"><?php echo __("about", "parent.theme") . " " . get_the_title() ?></span>
              </a>
            </div>
          </nav>
        </div>
        <div class="media-right">
        </div>
      </article>
            <?php

          endwhile;
        else:

      ?>
      <h2>No Results to Display</h2>
      <?php 
        endif;
      ?>
        <div class="pagination">
          <?php 
            echo paginate_links( $pagination );
          ?>
        </div>
      </div>   
    </div>
  </section>
</main>

<script>
    var searchVars = <?php echo json_encode([ 'query' => $search, 'page' ], JSON_HEX_TAG); ?>; // Don't forget the extra semicolon!
</script>

<?php
wp_reset_query();

if( false !== $sidebar ) {

  Utility::getScopedTemplatePart(
    "template-parts/aside/aside",
    null,
    [
      'sidebar' => $sidebar
    ]
  );

}

get_footer();

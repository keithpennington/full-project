<?php

namespace Theme\Parent;

include( TEMPLATEPATH . "/traits/Instance.php" );

class Loader {

  use Traits\Instance;

  protected $_ThemeSupport;
  protected $_Menus;
  protected $_PostTypes;
  protected $_Shortcodes;
  protected $_SettingsPages;
  protected $_Widgets;
  protected $_Breadcrumbs;

  protected $_sidebars;

  protected $_scripts;
  protected $_styles;
  protected $_stylesheetFramework;
  protected $_frontPageMetabox;

  public $imageSizes = [
    [
      'crop'  => 0,
      'slug'  => "mobile",
      'name'  => "Mobile",
      'sizes' => [
        320,
        320
      ]
    ],
    [
      'crop'  => 0,
      'name'  => "Large Mobile",
      'slug'  => "large-mobile",
      'sizes' => [
        480,
        480
      ]
    ],
    [
      'crop'  => 0,
      'name'  => "Tablet",
      'slug'  => "tablet",
      'sizes' => [
        768,
        768
      ]
    ],
    [
      'crop'  => 0,
      'name'  => "Desktop",
      'slug'  => "desktop",
      'sizes' => [
        1024,
        1024
      ]
    ],
    [
      'crop'  => 0,
      'name'  => "High Resolution",
      'slug'  => "hi-res",
      'sizes' => [
        2048,
        2048
      ]
    ]
  ];
  public $features;
  public $postTypes = [];

  public static $availableFrameworks = [
    "Bootstrap",
    "Bulma",
    "Vanilla"
  ];

  function __construct() {

    self::$_instance = $this;

    if( method_exists( $this, "_stylesheetSetup" ) )
      $this->_stylesheetSetup();

    $this->_templateSetup();

    // Actions
    add_action( 'after_setup_theme', [ $this, "themeSupport" ], 3 );
    add_action( 'after_setup_theme', [ $this, "createImageSizes" ], 4 );
    add_action( 'init', [ $this, "registerEntities" ], 1 );
    add_action( 'widgets_init', [ $this, "registerSidebars" ] );
    add_action( 'widgets_init', [ $this, "themeWidgets" ], 55 );
    add_action( 'init', [ $this, "customAccordionBlock" ] );
    add_action( 'wp_nav_menu_item_custom_fields', [ $this, "navMenuItemCustomFields"], 5, 5 );
    add_action( 'wp_update_nav_menu_item', [ $this, "updateNavMenuItemCustomFields" ], 5 , 3 );

    foreach( [ "wp_enqueue_scripts", "admin_enqueue_scripts" ] as $hook ) {

      add_action( $hook, [ $this, "scripts" ] );
      add_action( $hook, [ $this, "styles" ] );
      add_action( $hook, [ $this, "localizedJS" ] );

    }

    add_action( 'wp_footer', [ $this, "siteSchema" ], 2 );
    add_action( 'wp_footer', [ $this, "noScripts" ] );

    // Filters
    if( $this->_frontPageMetabox && ! empty( $this->_frontPageMetabox ) )
      add_filter( 'default_theme_metaboxes', [ $this, "frontPageMetabox"] );

    add_filter( 'auto_update_plugin', '__return_false' );
    add_filter( 'get_site_icon_url', [ $this, "favicon" ], 10, 3 );
    add_filter( 'wp_title', [ $this, "wpTitle" ], 10, 3 );
    add_filter( 'intermediate_image_sizes', [ $this, "reduceImageSizes" ] );
    add_filter( 'image_size_names_choose', [ $this, "imageChoices" ] );
    add_filter( 'language_attributes', [ $this, "ogHtmlAttr" ], 10, 2 );
    add_filter( 'wp_get_attachment_image_attributes', [ $this, "imageAtts" ], 20, 3 );
    add_filter( 'wp_sitemaps_enabled', [ $this, "disableDefaultSitemap" ] );
    add_filter( 'theme_templates', [ $this, "reduceTemplates" ], 10, 4 );
    add_filter( 'wp_mail_from', [ $this, "mailFrom" ] );
    add_filter( 'wp_mail_from_name', [ $this, "mailFromName" ] );
    add_filter( 'upload_mimes', [ $this, "addMimeTypes" ], 4 ) ;
    add_filter( 'walker_nav_menu_end_el', [ $this, 'renderMegaMenus' ], 10, 4 );
    add_filter( 'nav_menu_link_attributes', [ $this, 'megaMenuLinkAtts' ], 10, 4 );
    add_filter( 'site_schema', [ $this, "multiLocationSiteSchema" ], 10, 2 );

    if( defined( "PROD_URL" ) ) {

      add_filter( 'wp_get_attachment_image_src', [ $this, "resetImageBase" ], 10, 4 );
      add_filter( 'upload_dir', [ $this, "resetUploadDir" ] );

    }

  }

  private function _templateSetup() {

    $inClasses = [
      "Utility",
      "Defaults",
      "Field",
      "Menu",
      "PostType",
      "Shortcode",
      "Sitemap",
      "SettingsPage",
      "Users",
      "Woocommerce"
    ];

    $inTraits = [
      "PostMetaFields",
      "Redirect",
      "Reference",
      "TermDependencies",
      "TermMetaFields"
    ];

    foreach( $inTraits as $inT )
      $this->_include( TEMPLATEPATH . "/traits/", $inT );

    foreach( $inClasses as $inC )
      $this->_include( TEMPLATEPATH . "/classes/", $inC );

    if( is_admin() || ! defined( "CONTENTFRAMEWORK" ) )
      define( "CONTENTFRAMEWORK", "Bootstrap" );

    if( isset( $this->_stylesheetFramework ) && ! empty( $this->_stylesheetFramework ) )
      $this->_include( TEMPLATEPATH . "/classes/Frameworks/", $this->_stylesheetFramework );

    $this->_handleFeatures();

    add_editor_style( STYLESHEETPATH . "/css/theme.min.css" );

  }

  private function _handleFeatures() {

    if( ! isset( $this->features['wpemoji'] ) ) {

      remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
      remove_action( 'wp_print_styles', 'print_emoji_styles' );

      remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
      remove_action( 'admin_print_styles', 'print_emoji_styles' );

    }

  }

  private function _includeFromThemePaths( $object, $folder, $construct = true ) {

    $objects = "_" . $object . "s";

    if( ! empty( $this->$objects ) ) {

      foreach( $this->$objects as $name ) {

        if( $this->_include( STYLESHEETPATH . $folder, $name ) ) {

          $class = "Theme\Child\\" . $object . "\\" . $name;

          if( $construct && class_exists( $class ) ) {

            $instance = new $class();
            Utility::objectUpdates( $instance );

          }
          else
            error_log( "Missing class " . $class );

        } elseif( $this->_include( TEMPLATEPATH . $folder, $name ) ) {

          $class = "Theme\Parent\\" . $object . "\\" . $name;

          if( $construct && class_exists( $class ) ) {

            $instance = new $class();
            Utility::objectUpdates( $instance );
          }
          else
            error_log( "Missing class " . $class );

        }

      }

    }

  }

  protected function _include( $path, $name ) {

    $status = false;
    $file = $path . $name . ".php";

    if( file_exists( $file ) ) {

      include_once( $file );
      $status = true;

    }

    return $status;

  }

  public function themeSupport() {

    if( is_array( $this->_ThemeSupport ) && ! empty( $this->_ThemeSupport ) ) {

      foreach( $this->_ThemeSupport as $feature ) {
        if( isset( $feature['args'] ) )
          add_theme_support( $feature['name'], $feature['args'] );
        else
          add_theme_support( $feature['name'] );

      }

    }

  }

  public function createImageSizes() {

    foreach( $this->imageSizes as $imgSize ) {

      add_image_size( $imgSize['slug'], $imgSize['sizes'][0], $imgSize['sizes'][1], $imgSize['crop'] );

    }

  }

  public function registerEntities() {

    $this->_includeFromThemePaths( "PostType", "/classes/PostTypes/" );
    $this->_includeFromThemePaths( "Shortcode", "/classes/Shortcodes/" );
    $this->_includeFromThemePaths( "SettingsPage", "/classes/SettingsPages/" );
    $this->_includeFromThemePaths( "Menu", "/classes/Menus/" );

    // Build the sitemap after all objects are registered.
    new Sitemap();

  }

  public function registerSidebars() {

    if( is_array( $this->_sidebars ) && ! empty( $this->_sidebars ) ) {

    foreach( $this->_sidebars as $s )
      register_sidebar($s);

    }

  }

  public function themeWidgets() {

    global $wp_registered_widgets;

    if( is_array( $this->_Widgets ) && ! empty( $this->_Widgets ) ) {

      foreach( $this->_Widgets['deactivate'] as $widget )
          unregister_widget( $widget );

      foreach( $this->_Widgets['activate'] as $widget ) {
        // TO DO:  Add registration process for widgets.
      }


    }

  }

  public function scripts() {

    if( is_array( $this->_scripts ) && ! empty( $this->_scripts ) ) {

      $scripts = true === is_admin() ? $this->_scripts['admin'] : $this->_scripts['front'];

      if( is_array( $scripts ) && ! empty( $scripts ) ) {

        foreach( $scripts as $s ) {

          if( ! isset( $s['dir'] ) )
            continue;

          $dirFunc = "get_" . $s['dir'] . "_directory_uri";

          if( ! function_exists( $dirFunc ) )
            continue;

          $deps = isset( $s['dependencies'] ) ? $s['dependencies'] : [];
          $ver  = isset( $s['ver'] ) ? $s['ver'] : false;

          if( array_key_exists( 'name', $s) && array_key_exists( 'file', $s ) )
            wp_enqueue_script( $s['name'], $dirFunc() . $s['file'], $deps, $s['ver'], $ver);

        }

      }

    }

  }

  public function styles() {

    if( is_array( $this->_styles ) && ! empty( $this->_styles ) ) {

      $styles = true === is_admin() ? $this->_styles['admin'] : $this->_styles['front'];

      if( is_array( $styles ) && ! empty( $styles ) ) {

        foreach( $styles as $s ) {

          if( ! isset( $s['dir'] ) )
            continue;

          $dirFunc  = "get_" . $s['dir'] . "_directory_uri";

          if( ! function_exists( $dirFunc ) )
            continue;

          $deps = isset( $s['dependencies'] ) ? $s['dependencies'] : [];
          $ver  = isset( $s['ver'] ) ? $s['ver'] : false;

          if( array_key_exists( 'name', $s ) && array_key_exists( 'file', $s ) )
            wp_enqueue_style( $s['name'], $dirFunc() . $s['file'], $deps, $ver );

        }

      }

    }

  }

  public function localizedJS() {

    if( ! is_admin() ) {
      
      $l10n = [
        'hostname'  => get_option( 'siteurl' ),
        'uaIds'     => array_map( 'trim', explode( ",", Utility::getOption( 'google_props', 'ga_id' ) ) ),
        'gtmIds'    => array_map( 'trim', explode( ",", Utility::getOption( 'google_props', 'gtm_id' ) ) ),
        'mapKey'    => Utility::getOption( 'google_props', 'maps_api_key' )
      ];

      $code = "var loadVars = " . json_encode( apply_filters( 'localized_js', $l10n ) ) . ";";
      wp_add_inline_script( "theme", $code, "before" );

    }

  }

  public function favicon( $url, $size, $blog_id ) {

    $url = get_stylesheet_directory_uri() . "/assets/favicon.png";
    return $url;

  }

  public function siteSchema() {

    $schema = json_encode( Utility::getSchema() );

    echo sprintf( "\n<script type=\"application/ld+json\">\n%s\n</script>\n", $schema );

  }

  public static function multiLocationSiteSchema( $schema, $info ) {

    foreach( $schema as $k => $item ) {
      $key = array_search( 'LocalBusiness', $item ); 

      if ( Utility::isMultiLocationSite() && $key !== false ) {
        unset( $schema[$k] );

        $schema = array_values( $schema );
      }
    }

    return $schema;
  }

  public function noScripts() {

    $gtmContainers = array_map( 'trim', explode( ",", Utility::getOption( 'google_props', 'gtm_id' ) ) );

    foreach( $gtmContainers as $container )
      echo sprintf( "<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=%s\" height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n", $container );

  }

  public function frontPageMetabox( $metaboxes ) {

    if( is_admin() && isset( $_GET['post'] ) && isset( $_GET['action'] ) ) {
      
      if( "edit" === $_GET['action'] && $_GET['post'] === get_option( 'page_on_front' ) )
        $metaboxes[] = $this->_frontPageMetabox;

    }

    return $metaboxes;

  }

  public function wpTitle( $title, $sep, $sepLocation ) {

    if( is_front_page() )
      $title = get_the_title() . " $sep " . get_bloginfo( 'blogname' );
    else
      $title = $title . get_bloginfo( 'blogname' );

    return $title;

  }

  public function reduceImageSizes( $defaults ) {

    $themeSizes = array_column( $this->imageSizes, "slug" );
    $keepSizes  = array_merge( [ "thumbnail" ], $themeSizes );

    return array_intersect( $defaults, $keepSizes);

  }

  public function imageChoices( $choices ) {

    $themeSizes = array_combine(
      array_column( $this->imageSizes, "slug" ),
      array_column( $this->imageSizes, "name" )
    );

    $choices = array_merge( $choices, $themeSizes );

    return $choices;

  }

  public function ogHtmlAttr( $output, $doctype ) {

    return $output . ' prefix="og: https://ogp.me/website#"';

  }

  public function imageAtts( $attr, $attachment, $size ) {

    $dimensions   = wp_get_attachment_image_src( $attachment->ID );
    $orientation  = "landscape";

    if( $dimensions[2] > $dimensions[1] )
      $orientation = "portrait";

    $attr['class'] .= " {$orientation}-oriented";

    return $attr;

  }

  public function disableDefaultSitemap( $enabled ) { return false; }

  public function reduceTemplates( $templates, $wpTheme, $post, $postType ) {

    foreach( $templates as $name => $info ) {

      if( ! stristr( $name, $this->_stylesheetFramework ) )
        unset( $templates[$name] );

    }

    return $templates;

  }

  public function mailFrom( $email ) { return "wordpress@lightsoncreative.com"; }

  public function mailFromName( $name ) { return "Lights On Creative [WordPress]"; }

  public function resetImageBase( $image, $attach_id, $size, $icon ) {

    $image = str_replace( WP_SITEURL, PROD_URL, $image );

    return $image;

  }

  public function resetUploadDir( $dir ) {

    $dir['baseurl'] = str_replace( WP_SITEURL, PROD_URL, $dir['baseurl'] );
    $dir['url']     = str_replace( WP_SITEURL, PROD_URL, $dir['url'] );

    return $dir;

  }

  public function addMimeTypes( $types ) {

    if ( current_user_can( 'upload_files' ) ) {

      $types['svg']   =  "image/svg+xml";
      $types['webp']  =  "image/webp";

    }

    return $types;

  }

  public function customAccordionBlock() {

    wp_register_script(
      'accordion-block-item',
      get_template_directory_uri() . '/js/blocks/accordion-item.js'
  );

    wp_register_script(
        'accordion-block',
        get_template_directory_uri() . '/js/blocks/accordion.js'
    );

  register_block_type( 'custom/accordion-item', array(
    'api_version' => 2,
    'editor_script' => 'accordion-block-item',
) );
  
    register_block_type( 'custom/accordion', array(
        'api_version' => 2,
        'editor_script' => 'accordion-block',
    ) );
  }

  /**
   * navMenuItemCustomFields
   * 
   * Hooking into the wp_nav_menu_item_custom_fields action to add Mega Menu checkbox and select fields.
   * Could be used to add additional fields.
   *
   * @param int $item_id The menu item ID
   * @param WP_Post $item Menu item data object
   * @param int $depth Depth of menu item. Used for padding
   * @param stdClass $args An object of Walker arguments
   * @param int $id Nav menu ID
   * @return void
   */
  public function navMenuItemCustomFields( $item_id, $item, $depth, $args, $id ) {

    $menuLocations = get_nav_menu_locations();

    if( array_key_exists( 'main-nav', $menuLocations ) ) {

      $megaMenuMeta = get_post_meta( $item_id, '_mega_menu_meta', true );
      $megaMenuSelection = get_post_meta( $item_id, '_mega_menu_selection', true );
  
      $args = [
        'post_type'   =>    'megamenu',
        'numberposts' =>    -1
      ];
  
      $megaMenus = get_posts( $args );
  
      foreach ( $megaMenus as $menu ) {
        $selections .= '<option value="' . $menu->post_name . '"';
        ( $megaMenuSelection === $menu->post_name ) ? $selections .= 'selected' : false;
        $selections .= '>' . $menu->post_title . '</option>';
      }
  
      wp_nonce_field( 'mega_menu_meta_nonce', '_mega_menu_meta_nonce_name' );
      $field = '<label for="edit-menu-item-mega-menu-' . $item_id . '">';
      $field .= 'Mega Menu <input type="checkbox" id="edit-menu-item-mega-menu-' . $item_id . '"';
      $field .= 'class="widefat edit-menu-item-mega-menu" name="menu-item-mega-menu[' . $item_id . ']" value="' . true . '"';
      $field .= ( $megaMenuMeta ) ? " checked " : "";
      $field .= '/></label>';
  
      if( $megaMenuMeta ) {
        $field .= '<br/><select name="mega-menu-selection[' . $item_id . ']"id="mega-menu-list-' . $item_id . '">' . $selections . '</select>';
      }
  
      echo $field;
  
      ?>
      
      <script>
        jQuery('#edit-menu-item-mega-menu-<?php echo $item_id ?>').change(function(e) {
          if ( jQuery(e.currentTarget).is(':checked') ) {
            parent = jQuery(e.currentTarget).parent();
            jQuery('<br/><select name="mega-menu-selection[<?php echo $item_id ?>]" id="mega-menu-list-<?php echo $item_id ?>"><?php echo $selections ?></select>').insertAfter(parent);
          } else {
            jQuery('#mega-menu-list-<?php echo $item_id ?>').remove();
          }
        });
      </script>
      <?php 

    }
  }

  /**
   * updateNavMenuItemCustomFields
   *
   * Hooking into wp_update_nav_menu_item to update mega menu custom fields when saving menus
   *
   * @param int $menu_id
   * @param int $menu_item_db_id
   * @param array $menu_item_data
   * @return void
   */
  public function updateNavMenuItemCustomFields( $menu_id, $menu_item_db_id, $menu_item_data ) {
    $classes = $menu_item_data['menu-item-classes'];

    if ( ! isset( $_POST['_mega_menu_meta_nonce_name'] ) || ! wp_verify_nonce( $_POST['_mega_menu_meta_nonce_name'], 'mega_menu_meta_nonce' ) ) {
      return $menu_id;
    }

    if ( isset( $_POST['menu-item-mega-menu'][$menu_item_db_id]  ) ) {
      $val = ( $_POST['menu-item-mega-menu'][$menu_item_db_id] ) ? true : false;
      
      if( $classes[0] === "" ) {
        $classes[0] = 'mega-menu-link';
      } else {
        (! in_array( 'mega-menu-link', $classes ) ) ? array_push( $classes, 'mega-menu-link' ) : false;
      }

      update_post_meta( $menu_item_db_id, '_menu_item_classes', $classes );
      update_post_meta( $menu_item_db_id, '_mega_menu_meta', $val);
      update_post_meta( $menu_item_db_id, '_mega_menu_selection', $_POST['mega-menu-selection'][$menu_item_db_id] );
    } else {

      $key = array_search( 'mega-menu-link', $classes );

      if( $key !== false ) {
        unset( $classes[$key] );
      } 

      update_post_meta( $menu_item_db_id, '_menu_item_classes', $classes );
      delete_post_meta( $menu_item_db_id, '_mega_menu_meta' );
      delete_post_meta( $menu_item_db_id, '_mega_menu_selection' );
    }
  }
  
  /**
   * Hooking into walker_nav_menu_end_el to render the mega menu html after the menu item has been created in the Walker
   *
   * @param string $item_output
   * @param WP_Post $item
   * @param int $depth
   * @param stdClass $args
   * @return string The end of the menu item and the filtered output of the mega menu post, if it has a mega menu assigned.
   */
  public function renderMegaMenus( $item_output, $item, $depth, $args ) {

    $hasMegaMenu  = in_array( 'mega-menu-link', $item->classes ); 

    if( $hasMegaMenu && $depth === 0 && $args->theme_location === 'main-nav' ) {

      $megaMenuSlug = get_post_meta( $item->ID, '_mega_menu_selection', true );

      $postArgs = [
        'name'          => $megaMenuSlug,
        'post_type'     =>  'megamenu',
        'post_status'   =>  'Published',
        'numberposts'   => 1
      ];

      $megaMenu  =  get_posts( $postArgs );

      ob_start();

      Utility::getScopedTemplatePart( 'template-parts/nav/nav', 'mega-menu', [ 'item' => $item, 'mega_menu' => $megaMenu[0], 'menu_args' => $args ] );

      $item_output .= ob_get_clean();

    }

    return $item_output;
  }

  /**
   * Hooking into nav_menu_link_attributes to add a data-id attr for JS targeting
   *
   * @param array $atts
   * @param WP_Post $item
   * @param stdClass $args
   * @return arrary $atts and array of the html attributes.
   */
  public function megaMenuLinkAtts( $atts, $item, $args ) {

    $atts['data-id'] = 'mega-menu-' . $item->ID;
    return $atts;
  }

}

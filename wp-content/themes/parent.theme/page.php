<?php
/**
 * The template for displaying a page
 *
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

$sidebar  = apply_filters(
  'sidebar',
  false,
  [
    'location'  => "page",
    'object'    => get_page( get_the_ID() )
  ]
);

get_header();

if( have_posts() ) {

  while( have_posts() ) {

    the_post();

    Utility::getScopedTemplatePart(
      "template-parts/hero/hero",
      null
    );

    Utility::getScopedTemplatePart(
      "template-parts/content/content",
      null,
      [
        'sidebar' => $sidebar
      ]
    );

    if( $sidebar ) {
      Utility::getScopedTemplatePart(
        "template-parts/aside/aside",
        null,
        [
          'sidebar' => $sidebar
        ]
      );
    }

  }

} else {

  Utility::getScopedTemplatePart( "template-parts/hero/hero", null );
  Utility::getScopedTemplatePart( "template-parts/content/content", "none" );

}

get_footer() ?>

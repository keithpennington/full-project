<?php
/**
 * The template for 404 page
 *
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

function page_404_content() {

  ob_start();

  Utility::getScopedTemplatePart(
    "template-parts/hero/hero",
    null,
    [
      'before'  => apply_filters('404_title_icon', "<i class=\"fas fa-fw fa-exclamation\"></i>" ),
      'classes' => [ "is-danger" ],
      'title'   => apply_filters( '404_title', "Whoops" ),
      'subtitle' => apply_filters( '404_subtitle', __( "Looks like you've gotten lost.", "theme" ) )
    ]
  );

  Utility::getScopedTemplatePart(
    "template-parts/content/content",
    null
  );

  return ob_get_clean();

}

$four04 = new WP_Query( [
  'pagename' => 'site-404'
] );

if( ! $four04->have_posts() ) {

  $four04 = new WP_Query( [
    'p' => get_option( 'page_for_posts' )
  ] );

}

while( $four04->have_posts() ) {
  
  $four04->the_post();
  $template = get_page_template();
  get_header();
  echo page_404_content();
  get_footer();

}

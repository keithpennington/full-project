<?php
/**
 * The template for displaying a single post
 *
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

$sidebar = apply_filters(
  'sidebar',
  false,
  [
    'location'  => "single",
    'object'    => get_post( get_the_ID() )
  ]
);

get_header();

if( have_posts() ) {

  while( have_posts() ) {

    the_post();

    Utility::getScopedTemplatePart(
      "template-parts/hero/hero-job",
      null
    );

    Utility::getScopedTemplatePart(
      "template-parts/content/content",
      null,
      [
        'sidebar' => $sidebar
      ]
    );

    if( false !== $sidebar ) {
      Utility::getScopedTemplatePart(
        "template-parts/aside/aside",
        null,
        [
          'sidebar' => $sidebar
        ]
      );
    }

  }

} else {

  Utility::getScopedTemplatePart( "template-parts/hero/hero", null );
  Utility::getScopedTemplatePart( "template-parts/content/content", "none" );

}

get_footer();

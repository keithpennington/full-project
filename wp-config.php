<?php
/**
 * Change the database configuration based on the presence
 * of a local development environment variable.
 */

if( getenv( "LOCDEV" ) ) {

  define( 'DB_NAME', 'local' );
  define( 'DB_USER', 'root' );
  define( 'DB_PASSWORD', 'root' );
  define( 'DB_HOST', 'localhost' );

  define( 'WP_HOME', "http://localhost:8888" );
  define( 'WP_SITEURL', "http://localhost:8888" );

} else {

  define( 'DB_NAME', '{STAGING_DB_NAME}' );
  define( 'DB_USER', '{STAGING_DB_USER}' );
  define( 'DB_PASSWORD', '{STAGING_DB_PASSWORD}' );
  define( 'DB_HOST', '127.0.0.1' ); 

  define( 'WP_HOME', "{STAGING_URL}" );
  define( 'WP_SITEURL', "{STAGING_URL}" );

}

define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );
define( 'AUTOMATIC_UPDATER_DISABLED', true );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         'WQaX#;NQy);6(.+qai++!;_?6-59iW-N-.4VqI@CS*p*`U.<y.<7RO5o_+hO8irj');
define('SECURE_AUTH_KEY',  'zp.G1jcS5/#S^^fek!9ted|--beAJ`sfvZ%?]8;GCs?H$3ay<.Hh2FI*<Uy$7}-@');
define('LOGGED_IN_KEY',    '-4]N.})cph.m[</$MUTo`T|(9qbK7#`K$F5+?wyOs<Hmc+T9PNQ,j>3aD4dNB+j>');
define('NONCE_KEY',        'G`>dGhmtE^J5DA~4$;+ky2%OX->zZ8SS8iE!acC4NS$Tep.fM{Ru&pWSk.VbMUE|');
define('AUTH_SALT',        'SZLY6u+.1S8|rs7?76<[+0B%nM4V,HYbP0/Ah^DnVR?|byL$9}KusjWUH)YNZV|1');
define('SECURE_AUTH_SALT', 'mjdmeRR^gy]{}:Oue.cp3~%r/a?2-_Q54iy U+b0w#/3;66K_r{UBM7[w|Cq$HF@');
define('LOGGED_IN_SALT',   '*Sch;_;*`R?iD<(s=+gGS|9z,|p,VpqRy^d_R$_BLKu+0?G4ZuQ6gz-^`9yh2-zt');
define('NONCE_SALT',       '[8m$0X,-9K|QH5ZM1^=ZU?x)Dvp$TRYz?K>AO<)+|1X5?VYm-+em$Z:nuU(.vOaG');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';
/* That's all, stop editing! Happy publishing. */

if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
define( 'WP_ENVIRONMENT_TYPE', 'staging' ); // Added by SiteGround WordPress Staging system
require_once ABSPATH . 'wp-settings.php';
@require_once ABSPATH . 'disable_wp_cron.php'; // Added by SiteGround WordPress Staging system
@include_once('/var/lib/sec/wp-settings.php'); // Added by SiteGround WordPress management system
